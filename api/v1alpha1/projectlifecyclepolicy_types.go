/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type AppDeletionPolicyType string

const (
	// AppDeletionPolicyDeleteNamespace deletes parent namespace when an ApplicationRegistration's status.provisioningStatus becomes DeletedFromAPI
	AppDeletionPolicyDeleteNamespace AppDeletionPolicyType = "DeleteNamespace"
	// AppDeletionPolicyIgnoreAndPreserveNamespace does nothing when an ApplicationRegistration's status.provisioningStatus becomes DeletedFromAPI
	AppDeletionPolicyIgnoreAndPreserveNamespace AppDeletionPolicyType = "IgnoreAndPreserveNamespace"
	// AppDeletionPolicyBlockAndDeleteAfterGracePeriod will mark the namespace as "soft deleted", having a behavior similar to blocked, when an ApplicationRegistration's status.provisioningStatus becomes DeletedFromAPI
	// Operator should block website similar to normal block procedure: https://okd-internal.docs.cern.ch/operations/project-blocking/
	// More info: https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/1115
	AppDeletionPolicyBlockAndDeleteAfterGracePeriod AppDeletionPolicyType = "BlockAndDeleteAfterGracePeriod"
)

// strings for the conditions in status
const (
	// Type of the Condition in ProjectLifecyclePolicy status that indicates if policy was successfully applied
	ConditionTypeAppliedProjectLifecyclePolicy string = "AppliedProjectLifecyclePolicy"
	// Reason for the Condition in ProjectLifecyclePolicy status when policy was successfully applied
	ConditionReasonSuccessful string = "Successful"
	// Reason for the Condition in ProjectLifecyclePolicy status when policy was NOT successfully applied
	// because the conditions are not met for us to be able to do something.
	ConditionReasonCannotApply string = "CannotApply"
	// Reason for the Condition in ProjectLifecyclePolicy status when policy was NOT successfully applied
	// because we tried but someting went wrong.
	// NB: we could have a separate value for each failure case, but not worth the effort for the projectLifecyclePolicy.
	ConditionReasonFailed string = "Failed"
)

// ProjectLifecyclePolicySpec defines the desired state of ProjectLifecyclePolicy
type ProjectLifecyclePolicySpec struct {

	// The ClusterRole that should be granted to the Application's owner and administrator group
	// in the RoleBinding identified by ApplicationOwnerRoleBindingName.
	// The authz-operator serviceaccount MUST itself have this cluster role so it can grant it to other users!
	// If not specified, then no RoleBinding is created.
	// +optional
	ApplicationOwnerClusterRole string `json:"applicationOwnerClusterRole,omitempty"`

	// Name of a RoleBinding whose members should be set to the value of ApplicationRegistration's status.CurrentOwnerUsername
	// and (if present) status.CurrentAdminGroup.
	// Any other member will be removed from the RoleBinding.
	// +kubebuilder:default:="application-owner"
	ApplicationOwnerRoleBindingName string `json:"applicationOwnerRoleBindingName"`

	// Policy when the ApplicationRegistration's status.provisioningStatus becomes DeletedFromAPI,
	// i.e. the application was deleted from the Application Portal.
	// If DeleteNamespace, the parent namespace/project containing the ApplicationRegistration is deleted.
	// +kubebuilder:validation:Enum="IgnoreAndPreserveNamespace";"DeleteNamespace";"BlockAndDeleteAfterGracePeriod"
	// +kubebuilder:default:="IgnoreAndPreserveNamespace"
	ApplicationDeletedFromAuthApiPolicy AppDeletionPolicyType `json:"applicationDeletedFromAuthApiPolicy"`

	// Generate a link to the application's management page in the application portal.
	// This is created as a ConsoleLink in the NamespaceDashboard (the only type of link
	// that can be specified per namespace).
	// +optional
	ApplicationPortalManagementLink bool `json:"applicationPortalConsoleLink,omitempty"`

	// Generate a link showing current application's category in the app portal
	// with link to the application's management page to update category.
	// This is created as a ConsoleLink in the NamespaceDashboard (the only type of link
	// that can be specified per namespace).
	// +optional
	ApplicationCategoryLink bool `json:"applicationCategoryLink,omitempty"`

	// Sync the parent Openshift project's metadata (annotations and labels) with the information from the Application Portal.
	// Description goes to the standard Openshift annotation for project description. Owner, Admin Group and category are
	// exposed with custom labels.
	// +optional
	// +kubebuilder:default:=true
	SyncProjectMetadata bool `json:"syncProjectMetadata,omitempty"`
}

// ProjectLifecyclePolicyStatus defines the observed state of ProjectLifecyclePolicy
type ProjectLifecyclePolicyStatus struct {
	// Conditions represent the latest available observations of an object's state
	Conditions []metav1.Condition `json:"conditions"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// ProjectLifecyclePolicy controls how the authz-operator applies changes to lifecycle-related properties of the application in the AuthzAPI to the OKD project/namespace containing an `ApplicationRegistration`.
// More info: https://gitlab.cern.ch/paas-tools/operators/authz-operator#projectlifecyclepolicy
type ProjectLifecyclePolicy struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ProjectLifecyclePolicySpec   `json:"spec,omitempty"`
	Status ProjectLifecyclePolicyStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// ProjectLifecyclePolicyList contains a list of ProjectLifecyclePolicy
type ProjectLifecyclePolicyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ProjectLifecyclePolicy `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ProjectLifecyclePolicy{}, &ProjectLifecyclePolicyList{})
}
