package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/go-logr/logr"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/controller"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/kubernetes/scheme"
)

var failedDeleteNamespace = false

func deleteNamespacesIfDeleteTimestampIsOld(ctx context.Context, dryRunFlag bool, maxProjectDeletionsPerRun int, log logr.Logger) error {
	// Get Kubeconfig
	config := ctrl.GetConfigOrDie()

	// Create a new custom resource client using the Kubernetes client
	c, err := client.New(config, client.Options{
		Scheme: scheme.Scheme,
	})
	if err != nil {
		return fmt.Errorf("failed to create kubernetes client: %v", err)
	}

	// Define the label selector to use for filtering
	labelSelector := labels.SelectorFromSet(labels.Set(map[string]string{controller.LabelBlockedNamespace: "true"}))
	listOptions := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	// Retrieve a list of all namespaces with the given label
	namespaceList := &corev1.NamespaceList{}
	err = c.List(ctx, namespaceList, listOptions)
	if err != nil {
		return fmt.Errorf("failed to list namespaces: %v", err)
	}
	// Check if we found any eligible namespaces to process
	if len(namespaceList.Items) == 0 {
		log.Info("No Application Registration found for deletion.")
		return nil
	}
	numberOfProjectDeletions := 0
	for _, namespace := range namespaceList.Items {
		if shouldDeleteNamespace(log, namespace) {
			if numberOfProjectDeletions >= maxProjectDeletionsPerRun {
				return fmt.Errorf("reached the limit of deletions per run (%d), stopped processing further", maxProjectDeletionsPerRun)
			}
			deleteOpts := client.DeleteOptions{}
			if dryRunFlag {
				log.Info("Running operation in dry-run mode")
				deleteOpts.DryRun = []string{metav1.DryRunAll}
			}
			err = c.Delete(ctx, &namespace, &deleteOpts)
			if err != nil {
				log.Info("Failed to delete namespace", "error", err, "namespace", namespace.Name, "dry-run", dryRunFlag)
				failedDeleteNamespace = true
			} else {
				log.Info("Deleted namespace", "namespace", namespace.Name, "dry-run", dryRunFlag)
				numberOfProjectDeletions++
			}
		}
	}
	// Nothing else to do
	return nil
}

// Process namespace based on the presence of the annotation
func shouldDeleteNamespace(log logr.Logger, namespace corev1.Namespace) bool {
	// Confirm that expected annotations are in place
	if namespace.Annotations[controller.AnnotationBlockedNamespaceReason] == controller.AnnotationLifecycleBlockedReason && namespace.Annotations[controller.AnnotationDeleteNamespaceTimestamp] != "" {
		timestampToDelete, err := time.Parse(time.RFC3339, namespace.Annotations[controller.AnnotationDeleteNamespaceTimestamp])
		if err != nil {
			log.Error(err, fmt.Sprintf("Failed to retrieve delete Timestamp from namespace '%s': %s", namespace.Name, err))
			return false
		}
		// Confirm it has expired grace period
		if time.Now().After(timestampToDelete) {
			// Delete the namespace
			return true
		}
	}
	return false
}

func main() {
	dryRunFlag := flag.Bool("dry-run", false, "Do not execute deletions, just print the names of projects.")
	// We limit the number of deletions per run to avoid any unintentional mass deletion of projects
	maxProjectDeletions := flag.Int("max-project-deletions", 10, "Maximum number of project deletions per run.")

	flag.Parse()
	ctx := context.Background()

	// Set up Logger
	softdeleteLog := zap.New(zap.UseDevMode(true))
	ctrl.SetLogger(softdeleteLog)

	if err := deleteNamespacesIfDeleteTimestampIsOld(ctx, *dryRunFlag, *maxProjectDeletions, softdeleteLog); err != nil || failedDeleteNamespace {
		softdeleteLog.Error(err, "Namespace deletion failed")
		os.Exit(1)
	}
	softdeleteLog.Info("Process has finalized")
}
