# authz-operator

An operator to register k8s apps with the CERN Authorization Service, including
Application registrations, OIDC registrations and basic lifecycle ops.

# Setup & Deployment

## Configuration

The authz-operator is configured with a set of environment variables:

 env var | example | description
 --- | --- | ---
`CLUSTER_NAME`  | `okd4-prod1`           | Name of the k8s cluster where the operator is deployed. Used in the `ApplicationRegistration` naming convention.
`KC_ISSUER_URL` | `https://auth.cern.ch/auth/realms/cern` | Identity provider (keycloak) issuer URL to fetch API access tokens from
`AUTHZAPI_URL`  | `https://authorization-service-api.web.cern.ch` | API base URL for interacting with the Authorization service
`KC_CLIENT_ID`  | `authz-operator-okd4-prod` | For OAuth client credentials flow to get API access token
`KC_CLIENT_SECRET`  | `0789a3b8-fc2c-49d4-bfc9-eb1943f5977b` | For OAuth client credentials flow to get API access token

For Internal tests to work, two extra environment variables need to be set:

env var | description
--- | ---
`SVC_ACCOUNT_ID` | This is the `OwnerID` provided by the Auth API, should be found in the CI vars, to retrieve it, go to the [AuthAPI Documentation](https://authorization-service-api-dev.web.cern.ch/) (This will not work for production credentials) and get an Application owned by the Service account by ID, and the value should be returned as `OwnerID` 
`MANAGER_ID` | The `ID` of our Manager Application, should be found in the CI vars and not change 


In Helm deployments, these values correspond to parameters explained in [deploy/values.yaml](deploy/values.yaml)

## Deployment

Standard deployment is with the [Helm chart](deploy).

For a new deployment we need to [create new keycloack credentials](#keycloak-cred)

### <a name="keycloak-cred"></a> Create Keycloak credentials

The authz-operator's deployment needs to be known to the CERN Authorization service (AuthzSvc) as an Application.
The AuthzSvc supports the concept of a "manager" for each Application.
That's the role this operator plays for the resources it creates/manages from the AuthzSvc perspective.

The cluster admin needs to register an Application at the CERN Application portal and setup the OAuth client credentials flow:
1. [Create a new application](https://application-portal.web.cern.ch/)
    - An appropriate admin group should be specified
2. Create an OIDC registration with client credentials
    - Edit the Application -> SSO Registration
    - New OIDC registration
    - Set a random redirectURI eg "https://example.cern.ch"
    - Advanced Options -> check "My application will need to get tokens using its own client ID and secret"
3. Request group memberships:
    - `authorization-service-identity-readers`
    - `authorization-service-applications-managers`

### Images

Gitlab CI is set up to automatically tag images in this repo's registry whenever any branch is pushed.


# Development

## Design

### Discussions with Authorization service

[Support applications managed by another service](https://its.cern.ch/jira/browse/MALTIAM-646)
- Add `application.managerId`
- Define which properties can be modified on the authz / operator side

[Support blocking applications](https://its.cern.ch/jira/browse/MALTIAM-723) (security)
- Resources lifecycle

### OIDC: where is the SoT?

We discussed if OIDC-related information (esp `redirectURIs`) should be a new CRD or part of AppReg: [discussion](#53)
Especially, if `redirectURIs` should be read from this operator's CRDs at all, or directly from an external source.

### Supporting only a core OIDC flow type

This operator automates the most often-encountered cases, as a convenience, without wresting control from the end user.
Therefore, marginal use cases don't need to be automated.
With this reasoning, we don't support defining the OIDC flow type, because the only flow type supported by this operator
will be the authentication code.

Similar for `UserConsentRequired`: this is only relevant for content hosted outside of CERN, for which we don't care now.

#### Other OIDC flows

The user can always create a separate Application in the AuthzAPI from the UI and have fine-grained control over the OIDC details.

### Why `redirectURIs` -> AppReg.spec?

This choice requires other components to calculate the `redirectURIs` and put them in the AppReg.Spec
(likely the Site Operators).

We prefer this because where we pick the `redirectURIs` up from depends on the use case:

#### webeos

single redirectURI that can be generated at creation of the ApplicationRegistration
(just include it in the webeos project template) mysite
=> mysite.web.cern.ch/oidcsso/whatever (decided by the webeos-config-operator)
=> set ApplicationRegistration.spec.RedirectURI at project creation

#### PaaS / general use case

multiple hostnames, default `/*` (or user-specified) return path
=> Route + annotation; we'll need a separate operator (PaasSite operator?)
to update ApplicationRegistration.spec.RedirectURI base on changes to routes

### SAML

It is possible to create a SAML registration after the OIDC registration for the same application;
therefore we're not complicating the Drupal use case with this decision.

## Running tests locally

```bash
# Export secrets as env vars
export OPERATOR_NAME="authz-operator"
export CLUSTER_NAME="test1"
export KC_ISSUER_URL="https://keycloak-dev.cern.ch/auth/realms/cern"
export AUTHZAPI_URL="https://authorization-service-api-dev.web.cern.ch"
export KUBECONFIG=
# Secrets
export KC_CLIENT_ID=
export KC_CLIENT_SECRET=
./run-tests.sh
```
