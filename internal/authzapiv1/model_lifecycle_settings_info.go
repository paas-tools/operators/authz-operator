/*
Authorization Service API v. 1.0

<a href='https://cern.service-now.com/service-portal?id=privacy_policy&se=SSO-Service&notice=resources'>Privacy notice</a>

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package authzapiv1

import (
	"encoding/json"
)

// checks if the LifecycleSettingsInfo type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &LifecycleSettingsInfo{}

// LifecycleSettingsInfo struct for LifecycleSettingsInfo
type LifecycleSettingsInfo struct {
	Name                  NullableString        `json:"name,omitempty"`
	EligibleRole          *RoleInfo             `json:"eligibleRole,omitempty"`
	SubscribedRole        *RoleInfo             `json:"subscribedRole,omitempty"`
	SelfSubscriptionGroup *GroupInfo            `json:"selfSubscriptionGroup,omitempty"`
	DeniedRole            *RoleInfo             `json:"deniedRole,omitempty"`
	AdminRole             *RoleInfo             `json:"adminRole,omitempty"`
	HasResources          *bool                 `json:"hasResources,omitempty"`
	ManagedResourceTypes  []ManagedResourceType `json:"managedResourceTypes,omitempty"`
}

// NewLifecycleSettingsInfo instantiates a new LifecycleSettingsInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLifecycleSettingsInfo() *LifecycleSettingsInfo {
	this := LifecycleSettingsInfo{}
	return &this
}

// NewLifecycleSettingsInfoWithDefaults instantiates a new LifecycleSettingsInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLifecycleSettingsInfoWithDefaults() *LifecycleSettingsInfo {
	this := LifecycleSettingsInfo{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *LifecycleSettingsInfo) GetName() string {
	if o == nil || IsNil(o.Name.Get()) {
		var ret string
		return ret
	}
	return *o.Name.Get()
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *LifecycleSettingsInfo) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Name.Get(), o.Name.IsSet()
}

// HasName returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasName() bool {
	if o != nil && o.Name.IsSet() {
		return true
	}

	return false
}

// SetName gets a reference to the given NullableString and assigns it to the Name field.
func (o *LifecycleSettingsInfo) SetName(v string) {
	o.Name.Set(&v)
}

// SetNameNil sets the value for Name to be an explicit nil
func (o *LifecycleSettingsInfo) SetNameNil() {
	o.Name.Set(nil)
}

// UnsetName ensures that no value is present for Name, not even an explicit nil
func (o *LifecycleSettingsInfo) UnsetName() {
	o.Name.Unset()
}

// GetEligibleRole returns the EligibleRole field value if set, zero value otherwise.
func (o *LifecycleSettingsInfo) GetEligibleRole() RoleInfo {
	if o == nil || IsNil(o.EligibleRole) {
		var ret RoleInfo
		return ret
	}
	return *o.EligibleRole
}

// GetEligibleRoleOk returns a tuple with the EligibleRole field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LifecycleSettingsInfo) GetEligibleRoleOk() (*RoleInfo, bool) {
	if o == nil || IsNil(o.EligibleRole) {
		return nil, false
	}
	return o.EligibleRole, true
}

// HasEligibleRole returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasEligibleRole() bool {
	if o != nil && !IsNil(o.EligibleRole) {
		return true
	}

	return false
}

// SetEligibleRole gets a reference to the given RoleInfo and assigns it to the EligibleRole field.
func (o *LifecycleSettingsInfo) SetEligibleRole(v RoleInfo) {
	o.EligibleRole = &v
}

// GetSubscribedRole returns the SubscribedRole field value if set, zero value otherwise.
func (o *LifecycleSettingsInfo) GetSubscribedRole() RoleInfo {
	if o == nil || IsNil(o.SubscribedRole) {
		var ret RoleInfo
		return ret
	}
	return *o.SubscribedRole
}

// GetSubscribedRoleOk returns a tuple with the SubscribedRole field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LifecycleSettingsInfo) GetSubscribedRoleOk() (*RoleInfo, bool) {
	if o == nil || IsNil(o.SubscribedRole) {
		return nil, false
	}
	return o.SubscribedRole, true
}

// HasSubscribedRole returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasSubscribedRole() bool {
	if o != nil && !IsNil(o.SubscribedRole) {
		return true
	}

	return false
}

// SetSubscribedRole gets a reference to the given RoleInfo and assigns it to the SubscribedRole field.
func (o *LifecycleSettingsInfo) SetSubscribedRole(v RoleInfo) {
	o.SubscribedRole = &v
}

// GetSelfSubscriptionGroup returns the SelfSubscriptionGroup field value if set, zero value otherwise.
func (o *LifecycleSettingsInfo) GetSelfSubscriptionGroup() GroupInfo {
	if o == nil || IsNil(o.SelfSubscriptionGroup) {
		var ret GroupInfo
		return ret
	}
	return *o.SelfSubscriptionGroup
}

// GetSelfSubscriptionGroupOk returns a tuple with the SelfSubscriptionGroup field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LifecycleSettingsInfo) GetSelfSubscriptionGroupOk() (*GroupInfo, bool) {
	if o == nil || IsNil(o.SelfSubscriptionGroup) {
		return nil, false
	}
	return o.SelfSubscriptionGroup, true
}

// HasSelfSubscriptionGroup returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasSelfSubscriptionGroup() bool {
	if o != nil && !IsNil(o.SelfSubscriptionGroup) {
		return true
	}

	return false
}

// SetSelfSubscriptionGroup gets a reference to the given GroupInfo and assigns it to the SelfSubscriptionGroup field.
func (o *LifecycleSettingsInfo) SetSelfSubscriptionGroup(v GroupInfo) {
	o.SelfSubscriptionGroup = &v
}

// GetDeniedRole returns the DeniedRole field value if set, zero value otherwise.
func (o *LifecycleSettingsInfo) GetDeniedRole() RoleInfo {
	if o == nil || IsNil(o.DeniedRole) {
		var ret RoleInfo
		return ret
	}
	return *o.DeniedRole
}

// GetDeniedRoleOk returns a tuple with the DeniedRole field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LifecycleSettingsInfo) GetDeniedRoleOk() (*RoleInfo, bool) {
	if o == nil || IsNil(o.DeniedRole) {
		return nil, false
	}
	return o.DeniedRole, true
}

// HasDeniedRole returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasDeniedRole() bool {
	if o != nil && !IsNil(o.DeniedRole) {
		return true
	}

	return false
}

// SetDeniedRole gets a reference to the given RoleInfo and assigns it to the DeniedRole field.
func (o *LifecycleSettingsInfo) SetDeniedRole(v RoleInfo) {
	o.DeniedRole = &v
}

// GetAdminRole returns the AdminRole field value if set, zero value otherwise.
func (o *LifecycleSettingsInfo) GetAdminRole() RoleInfo {
	if o == nil || IsNil(o.AdminRole) {
		var ret RoleInfo
		return ret
	}
	return *o.AdminRole
}

// GetAdminRoleOk returns a tuple with the AdminRole field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LifecycleSettingsInfo) GetAdminRoleOk() (*RoleInfo, bool) {
	if o == nil || IsNil(o.AdminRole) {
		return nil, false
	}
	return o.AdminRole, true
}

// HasAdminRole returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasAdminRole() bool {
	if o != nil && !IsNil(o.AdminRole) {
		return true
	}

	return false
}

// SetAdminRole gets a reference to the given RoleInfo and assigns it to the AdminRole field.
func (o *LifecycleSettingsInfo) SetAdminRole(v RoleInfo) {
	o.AdminRole = &v
}

// GetHasResources returns the HasResources field value if set, zero value otherwise.
func (o *LifecycleSettingsInfo) GetHasResources() bool {
	if o == nil || IsNil(o.HasResources) {
		var ret bool
		return ret
	}
	return *o.HasResources
}

// GetHasResourcesOk returns a tuple with the HasResources field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LifecycleSettingsInfo) GetHasResourcesOk() (*bool, bool) {
	if o == nil || IsNil(o.HasResources) {
		return nil, false
	}
	return o.HasResources, true
}

// HasHasResources returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasHasResources() bool {
	if o != nil && !IsNil(o.HasResources) {
		return true
	}

	return false
}

// SetHasResources gets a reference to the given bool and assigns it to the HasResources field.
func (o *LifecycleSettingsInfo) SetHasResources(v bool) {
	o.HasResources = &v
}

// GetManagedResourceTypes returns the ManagedResourceTypes field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *LifecycleSettingsInfo) GetManagedResourceTypes() []ManagedResourceType {
	if o == nil {
		var ret []ManagedResourceType
		return ret
	}
	return o.ManagedResourceTypes
}

// GetManagedResourceTypesOk returns a tuple with the ManagedResourceTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *LifecycleSettingsInfo) GetManagedResourceTypesOk() ([]ManagedResourceType, bool) {
	if o == nil || IsNil(o.ManagedResourceTypes) {
		return nil, false
	}
	return o.ManagedResourceTypes, true
}

// HasManagedResourceTypes returns a boolean if a field has been set.
func (o *LifecycleSettingsInfo) HasManagedResourceTypes() bool {
	if o != nil && !IsNil(o.ManagedResourceTypes) {
		return true
	}

	return false
}

// SetManagedResourceTypes gets a reference to the given []ManagedResourceType and assigns it to the ManagedResourceTypes field.
func (o *LifecycleSettingsInfo) SetManagedResourceTypes(v []ManagedResourceType) {
	o.ManagedResourceTypes = v
}

func (o LifecycleSettingsInfo) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o LifecycleSettingsInfo) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.Name.IsSet() {
		toSerialize["name"] = o.Name.Get()
	}
	if !IsNil(o.EligibleRole) {
		toSerialize["eligibleRole"] = o.EligibleRole
	}
	if !IsNil(o.SubscribedRole) {
		toSerialize["subscribedRole"] = o.SubscribedRole
	}
	if !IsNil(o.SelfSubscriptionGroup) {
		toSerialize["selfSubscriptionGroup"] = o.SelfSubscriptionGroup
	}
	if !IsNil(o.DeniedRole) {
		toSerialize["deniedRole"] = o.DeniedRole
	}
	if !IsNil(o.AdminRole) {
		toSerialize["adminRole"] = o.AdminRole
	}
	if !IsNil(o.HasResources) {
		toSerialize["hasResources"] = o.HasResources
	}
	if o.ManagedResourceTypes != nil {
		toSerialize["managedResourceTypes"] = o.ManagedResourceTypes
	}
	return toSerialize, nil
}

type NullableLifecycleSettingsInfo struct {
	value *LifecycleSettingsInfo
	isSet bool
}

func (v NullableLifecycleSettingsInfo) Get() *LifecycleSettingsInfo {
	return v.value
}

func (v *NullableLifecycleSettingsInfo) Set(val *LifecycleSettingsInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableLifecycleSettingsInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableLifecycleSettingsInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLifecycleSettingsInfo(val *LifecycleSettingsInfo) *NullableLifecycleSettingsInfo {
	return &NullableLifecycleSettingsInfo{value: val, isSet: true}
}

func (v NullableLifecycleSettingsInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLifecycleSettingsInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
