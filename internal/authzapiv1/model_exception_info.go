/*
Authorization Service API v. 1.0

<a href='https://cern.service-now.com/service-portal?id=privacy_policy&se=SSO-Service&notice=resources'>Privacy notice</a>

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package authzapiv1

import (
	"encoding/json"
)

// checks if the ExceptionInfo type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ExceptionInfo{}

// ExceptionInfo struct for ExceptionInfo
type ExceptionInfo struct {
	Message         NullableString  `json:"message,omitempty"`
	StackTrace      []string        `json:"stackTrace,omitempty"`
	InnerExceptions []ExceptionInfo `json:"innerExceptions,omitempty"`
}

// NewExceptionInfo instantiates a new ExceptionInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewExceptionInfo() *ExceptionInfo {
	this := ExceptionInfo{}
	return &this
}

// NewExceptionInfoWithDefaults instantiates a new ExceptionInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewExceptionInfoWithDefaults() *ExceptionInfo {
	this := ExceptionInfo{}
	return &this
}

// GetMessage returns the Message field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *ExceptionInfo) GetMessage() string {
	if o == nil || IsNil(o.Message.Get()) {
		var ret string
		return ret
	}
	return *o.Message.Get()
}

// GetMessageOk returns a tuple with the Message field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *ExceptionInfo) GetMessageOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Message.Get(), o.Message.IsSet()
}

// HasMessage returns a boolean if a field has been set.
func (o *ExceptionInfo) HasMessage() bool {
	if o != nil && o.Message.IsSet() {
		return true
	}

	return false
}

// SetMessage gets a reference to the given NullableString and assigns it to the Message field.
func (o *ExceptionInfo) SetMessage(v string) {
	o.Message.Set(&v)
}

// SetMessageNil sets the value for Message to be an explicit nil
func (o *ExceptionInfo) SetMessageNil() {
	o.Message.Set(nil)
}

// UnsetMessage ensures that no value is present for Message, not even an explicit nil
func (o *ExceptionInfo) UnsetMessage() {
	o.Message.Unset()
}

// GetStackTrace returns the StackTrace field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *ExceptionInfo) GetStackTrace() []string {
	if o == nil {
		var ret []string
		return ret
	}
	return o.StackTrace
}

// GetStackTraceOk returns a tuple with the StackTrace field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *ExceptionInfo) GetStackTraceOk() ([]string, bool) {
	if o == nil || IsNil(o.StackTrace) {
		return nil, false
	}
	return o.StackTrace, true
}

// HasStackTrace returns a boolean if a field has been set.
func (o *ExceptionInfo) HasStackTrace() bool {
	if o != nil && !IsNil(o.StackTrace) {
		return true
	}

	return false
}

// SetStackTrace gets a reference to the given []string and assigns it to the StackTrace field.
func (o *ExceptionInfo) SetStackTrace(v []string) {
	o.StackTrace = v
}

// GetInnerExceptions returns the InnerExceptions field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *ExceptionInfo) GetInnerExceptions() []ExceptionInfo {
	if o == nil {
		var ret []ExceptionInfo
		return ret
	}
	return o.InnerExceptions
}

// GetInnerExceptionsOk returns a tuple with the InnerExceptions field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *ExceptionInfo) GetInnerExceptionsOk() ([]ExceptionInfo, bool) {
	if o == nil || IsNil(o.InnerExceptions) {
		return nil, false
	}
	return o.InnerExceptions, true
}

// HasInnerExceptions returns a boolean if a field has been set.
func (o *ExceptionInfo) HasInnerExceptions() bool {
	if o != nil && !IsNil(o.InnerExceptions) {
		return true
	}

	return false
}

// SetInnerExceptions gets a reference to the given []ExceptionInfo and assigns it to the InnerExceptions field.
func (o *ExceptionInfo) SetInnerExceptions(v []ExceptionInfo) {
	o.InnerExceptions = v
}

func (o ExceptionInfo) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ExceptionInfo) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.Message.IsSet() {
		toSerialize["message"] = o.Message.Get()
	}
	if o.StackTrace != nil {
		toSerialize["stackTrace"] = o.StackTrace
	}
	if o.InnerExceptions != nil {
		toSerialize["innerExceptions"] = o.InnerExceptions
	}
	return toSerialize, nil
}

type NullableExceptionInfo struct {
	value *ExceptionInfo
	isSet bool
}

func (v NullableExceptionInfo) Get() *ExceptionInfo {
	return v.value
}

func (v *NullableExceptionInfo) Set(val *ExceptionInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableExceptionInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableExceptionInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableExceptionInfo(val *ExceptionInfo) *NullableExceptionInfo {
	return &NullableExceptionInfo{value: val, isSet: true}
}

func (v NullableExceptionInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableExceptionInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
