/*
Authorization Service API v. 1.0

<a href='https://cern.service-now.com/service-portal?id=privacy_policy&se=SSO-Service&notice=resources'>Privacy notice</a>

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package authzapiv1

import (
	"encoding/json"
	"fmt"
)

// PolicyOperationType the model 'PolicyOperationType'
type PolicyOperationType string

// List of PolicyOperationType
const (
	POLICYOPERATIONTYPE_CREATE        PolicyOperationType = "Create"
	POLICYOPERATIONTYPE_READ          PolicyOperationType = "Read"
	POLICYOPERATIONTYPE_READ_ALL      PolicyOperationType = "ReadAll"
	POLICYOPERATIONTYPE_UPDATE        PolicyOperationType = "Update"
	POLICYOPERATIONTYPE_DELETE        PolicyOperationType = "Delete"
	POLICYOPERATIONTYPE_ANY_OPERATION PolicyOperationType = "AnyOperation"
)

// All allowed values of PolicyOperationType enum
var AllowedPolicyOperationTypeEnumValues = []PolicyOperationType{
	"Create",
	"Read",
	"ReadAll",
	"Update",
	"Delete",
	"AnyOperation",
}

func (v *PolicyOperationType) UnmarshalJSON(src []byte) error {
	var value string
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := PolicyOperationType(value)
	for _, existing := range AllowedPolicyOperationTypeEnumValues {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid PolicyOperationType", value)
}

// NewPolicyOperationTypeFromValue returns a pointer to a valid PolicyOperationType
// for the value passed as argument, or an error if the value passed is not allowed by the enum
func NewPolicyOperationTypeFromValue(v string) (*PolicyOperationType, error) {
	ev := PolicyOperationType(v)
	if ev.IsValid() {
		return &ev, nil
	} else {
		return nil, fmt.Errorf("invalid value '%v' for PolicyOperationType: valid values are %v", v, AllowedPolicyOperationTypeEnumValues)
	}
}

// IsValid return true if the value is valid for the enum, false otherwise
func (v PolicyOperationType) IsValid() bool {
	for _, existing := range AllowedPolicyOperationTypeEnumValues {
		if existing == v {
			return true
		}
	}
	return false
}

// Ptr returns reference to PolicyOperationType value
func (v PolicyOperationType) Ptr() *PolicyOperationType {
	return &v
}

type NullablePolicyOperationType struct {
	value *PolicyOperationType
	isSet bool
}

func (v NullablePolicyOperationType) Get() *PolicyOperationType {
	return v.value
}

func (v *NullablePolicyOperationType) Set(val *PolicyOperationType) {
	v.value = val
	v.isSet = true
}

func (v NullablePolicyOperationType) IsSet() bool {
	return v.isSet
}

func (v *NullablePolicyOperationType) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePolicyOperationType(val *PolicyOperationType) *NullablePolicyOperationType {
	return &NullablePolicyOperationType{value: val, isSet: true}
}

func (v NullablePolicyOperationType) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePolicyOperationType) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
