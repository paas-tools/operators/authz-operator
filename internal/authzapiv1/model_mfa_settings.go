/*
Authorization Service API v. 1.0

<a href='https://cern.service-now.com/service-portal?id=privacy_policy&se=SSO-Service&notice=resources'>Privacy notice</a>

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package authzapiv1

import (
	"encoding/json"
)

// checks if the MfaSettings type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MfaSettings{}

// MfaSettings struct for MfaSettings
type MfaSettings struct {
	Otp      *MfaMethodSettings `json:"otp,omitempty"`
	Webauthn *MfaMethodSettings `json:"webauthn,omitempty"`
}

// NewMfaSettings instantiates a new MfaSettings object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMfaSettings() *MfaSettings {
	this := MfaSettings{}
	return &this
}

// NewMfaSettingsWithDefaults instantiates a new MfaSettings object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMfaSettingsWithDefaults() *MfaSettings {
	this := MfaSettings{}
	return &this
}

// GetOtp returns the Otp field value if set, zero value otherwise.
func (o *MfaSettings) GetOtp() MfaMethodSettings {
	if o == nil || IsNil(o.Otp) {
		var ret MfaMethodSettings
		return ret
	}
	return *o.Otp
}

// GetOtpOk returns a tuple with the Otp field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MfaSettings) GetOtpOk() (*MfaMethodSettings, bool) {
	if o == nil || IsNil(o.Otp) {
		return nil, false
	}
	return o.Otp, true
}

// HasOtp returns a boolean if a field has been set.
func (o *MfaSettings) HasOtp() bool {
	if o != nil && !IsNil(o.Otp) {
		return true
	}

	return false
}

// SetOtp gets a reference to the given MfaMethodSettings and assigns it to the Otp field.
func (o *MfaSettings) SetOtp(v MfaMethodSettings) {
	o.Otp = &v
}

// GetWebauthn returns the Webauthn field value if set, zero value otherwise.
func (o *MfaSettings) GetWebauthn() MfaMethodSettings {
	if o == nil || IsNil(o.Webauthn) {
		var ret MfaMethodSettings
		return ret
	}
	return *o.Webauthn
}

// GetWebauthnOk returns a tuple with the Webauthn field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MfaSettings) GetWebauthnOk() (*MfaMethodSettings, bool) {
	if o == nil || IsNil(o.Webauthn) {
		return nil, false
	}
	return o.Webauthn, true
}

// HasWebauthn returns a boolean if a field has been set.
func (o *MfaSettings) HasWebauthn() bool {
	if o != nil && !IsNil(o.Webauthn) {
		return true
	}

	return false
}

// SetWebauthn gets a reference to the given MfaMethodSettings and assigns it to the Webauthn field.
func (o *MfaSettings) SetWebauthn(v MfaMethodSettings) {
	o.Webauthn = &v
}

func (o MfaSettings) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MfaSettings) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Otp) {
		toSerialize["otp"] = o.Otp
	}
	if !IsNil(o.Webauthn) {
		toSerialize["webauthn"] = o.Webauthn
	}
	return toSerialize, nil
}

type NullableMfaSettings struct {
	value *MfaSettings
	isSet bool
}

func (v NullableMfaSettings) Get() *MfaSettings {
	return v.value
}

func (v *NullableMfaSettings) Set(val *MfaSettings) {
	v.value = val
	v.isSet = true
}

func (v NullableMfaSettings) IsSet() bool {
	return v.isSet
}

func (v *NullableMfaSettings) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMfaSettings(val *MfaSettings) *NullableMfaSettings {
	return &NullableMfaSettings{value: val, isSet: true}
}

func (v NullableMfaSettings) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMfaSettings) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
