/*
Authorization Service API v. 1.0

<a href='https://cern.service-now.com/service-portal?id=privacy_policy&se=SSO-Service&notice=resources'>Privacy notice</a>

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package authzapiv1

import (
	"encoding/json"
)

// checks if the AccountProviderEnumerateEnvelope type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AccountProviderEnumerateEnvelope{}

// AccountProviderEnumerateEnvelope struct for AccountProviderEnumerateEnvelope
type AccountProviderEnumerateEnvelope struct {
	Pagination *Pagination       `json:"pagination,omitempty"`
	Delta      *Delta            `json:"delta,omitempty"`
	RequestId  NullableString    `json:"request_id,omitempty"`
	Message    NullableString    `json:"message,omitempty"`
	Data       []AccountProvider `json:"data,omitempty"`
}

// NewAccountProviderEnumerateEnvelope instantiates a new AccountProviderEnumerateEnvelope object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAccountProviderEnumerateEnvelope() *AccountProviderEnumerateEnvelope {
	this := AccountProviderEnumerateEnvelope{}
	return &this
}

// NewAccountProviderEnumerateEnvelopeWithDefaults instantiates a new AccountProviderEnumerateEnvelope object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAccountProviderEnumerateEnvelopeWithDefaults() *AccountProviderEnumerateEnvelope {
	this := AccountProviderEnumerateEnvelope{}
	return &this
}

// GetPagination returns the Pagination field value if set, zero value otherwise.
func (o *AccountProviderEnumerateEnvelope) GetPagination() Pagination {
	if o == nil || IsNil(o.Pagination) {
		var ret Pagination
		return ret
	}
	return *o.Pagination
}

// GetPaginationOk returns a tuple with the Pagination field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AccountProviderEnumerateEnvelope) GetPaginationOk() (*Pagination, bool) {
	if o == nil || IsNil(o.Pagination) {
		return nil, false
	}
	return o.Pagination, true
}

// HasPagination returns a boolean if a field has been set.
func (o *AccountProviderEnumerateEnvelope) HasPagination() bool {
	if o != nil && !IsNil(o.Pagination) {
		return true
	}

	return false
}

// SetPagination gets a reference to the given Pagination and assigns it to the Pagination field.
func (o *AccountProviderEnumerateEnvelope) SetPagination(v Pagination) {
	o.Pagination = &v
}

// GetDelta returns the Delta field value if set, zero value otherwise.
func (o *AccountProviderEnumerateEnvelope) GetDelta() Delta {
	if o == nil || IsNil(o.Delta) {
		var ret Delta
		return ret
	}
	return *o.Delta
}

// GetDeltaOk returns a tuple with the Delta field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AccountProviderEnumerateEnvelope) GetDeltaOk() (*Delta, bool) {
	if o == nil || IsNil(o.Delta) {
		return nil, false
	}
	return o.Delta, true
}

// HasDelta returns a boolean if a field has been set.
func (o *AccountProviderEnumerateEnvelope) HasDelta() bool {
	if o != nil && !IsNil(o.Delta) {
		return true
	}

	return false
}

// SetDelta gets a reference to the given Delta and assigns it to the Delta field.
func (o *AccountProviderEnumerateEnvelope) SetDelta(v Delta) {
	o.Delta = &v
}

// GetRequestId returns the RequestId field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *AccountProviderEnumerateEnvelope) GetRequestId() string {
	if o == nil || IsNil(o.RequestId.Get()) {
		var ret string
		return ret
	}
	return *o.RequestId.Get()
}

// GetRequestIdOk returns a tuple with the RequestId field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *AccountProviderEnumerateEnvelope) GetRequestIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.RequestId.Get(), o.RequestId.IsSet()
}

// HasRequestId returns a boolean if a field has been set.
func (o *AccountProviderEnumerateEnvelope) HasRequestId() bool {
	if o != nil && o.RequestId.IsSet() {
		return true
	}

	return false
}

// SetRequestId gets a reference to the given NullableString and assigns it to the RequestId field.
func (o *AccountProviderEnumerateEnvelope) SetRequestId(v string) {
	o.RequestId.Set(&v)
}

// SetRequestIdNil sets the value for RequestId to be an explicit nil
func (o *AccountProviderEnumerateEnvelope) SetRequestIdNil() {
	o.RequestId.Set(nil)
}

// UnsetRequestId ensures that no value is present for RequestId, not even an explicit nil
func (o *AccountProviderEnumerateEnvelope) UnsetRequestId() {
	o.RequestId.Unset()
}

// GetMessage returns the Message field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *AccountProviderEnumerateEnvelope) GetMessage() string {
	if o == nil || IsNil(o.Message.Get()) {
		var ret string
		return ret
	}
	return *o.Message.Get()
}

// GetMessageOk returns a tuple with the Message field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *AccountProviderEnumerateEnvelope) GetMessageOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Message.Get(), o.Message.IsSet()
}

// HasMessage returns a boolean if a field has been set.
func (o *AccountProviderEnumerateEnvelope) HasMessage() bool {
	if o != nil && o.Message.IsSet() {
		return true
	}

	return false
}

// SetMessage gets a reference to the given NullableString and assigns it to the Message field.
func (o *AccountProviderEnumerateEnvelope) SetMessage(v string) {
	o.Message.Set(&v)
}

// SetMessageNil sets the value for Message to be an explicit nil
func (o *AccountProviderEnumerateEnvelope) SetMessageNil() {
	o.Message.Set(nil)
}

// UnsetMessage ensures that no value is present for Message, not even an explicit nil
func (o *AccountProviderEnumerateEnvelope) UnsetMessage() {
	o.Message.Unset()
}

// GetData returns the Data field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *AccountProviderEnumerateEnvelope) GetData() []AccountProvider {
	if o == nil {
		var ret []AccountProvider
		return ret
	}
	return o.Data
}

// GetDataOk returns a tuple with the Data field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *AccountProviderEnumerateEnvelope) GetDataOk() ([]AccountProvider, bool) {
	if o == nil || IsNil(o.Data) {
		return nil, false
	}
	return o.Data, true
}

// HasData returns a boolean if a field has been set.
func (o *AccountProviderEnumerateEnvelope) HasData() bool {
	if o != nil && !IsNil(o.Data) {
		return true
	}

	return false
}

// SetData gets a reference to the given []AccountProvider and assigns it to the Data field.
func (o *AccountProviderEnumerateEnvelope) SetData(v []AccountProvider) {
	o.Data = v
}

func (o AccountProviderEnumerateEnvelope) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AccountProviderEnumerateEnvelope) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Pagination) {
		toSerialize["pagination"] = o.Pagination
	}
	if !IsNil(o.Delta) {
		toSerialize["delta"] = o.Delta
	}
	if o.RequestId.IsSet() {
		toSerialize["request_id"] = o.RequestId.Get()
	}
	if o.Message.IsSet() {
		toSerialize["message"] = o.Message.Get()
	}
	if o.Data != nil {
		toSerialize["data"] = o.Data
	}
	return toSerialize, nil
}

type NullableAccountProviderEnumerateEnvelope struct {
	value *AccountProviderEnumerateEnvelope
	isSet bool
}

func (v NullableAccountProviderEnumerateEnvelope) Get() *AccountProviderEnumerateEnvelope {
	return v.value
}

func (v *NullableAccountProviderEnumerateEnvelope) Set(val *AccountProviderEnumerateEnvelope) {
	v.value = val
	v.isSet = true
}

func (v NullableAccountProviderEnumerateEnvelope) IsSet() bool {
	return v.isSet
}

func (v *NullableAccountProviderEnumerateEnvelope) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAccountProviderEnumerateEnvelope(val *AccountProviderEnumerateEnvelope) *NullableAccountProviderEnumerateEnvelope {
	return &NullableAccountProviderEnumerateEnvelope{value: val, isSet: true}
}

func (v NullableAccountProviderEnumerateEnvelope) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAccountProviderEnumerateEnvelope) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
