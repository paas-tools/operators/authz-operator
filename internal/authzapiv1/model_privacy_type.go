/*
Authorization Service API v. 1.0

<a href='https://cern.service-now.com/service-portal?id=privacy_policy&se=SSO-Service&notice=resources'>Privacy notice</a>

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package authzapiv1

import (
	"encoding/json"
	"fmt"
)

// PrivacyType the model 'PrivacyType'
type PrivacyType string

// List of PrivacyType
const (
	PRIVACYTYPE_OPEN    PrivacyType = "Open"
	PRIVACYTYPE_MEMBERS PrivacyType = "Members"
	PRIVACYTYPE_ADMINS  PrivacyType = "Admins"
)

// All allowed values of PrivacyType enum
var AllowedPrivacyTypeEnumValues = []PrivacyType{
	"Open",
	"Members",
	"Admins",
}

func (v *PrivacyType) UnmarshalJSON(src []byte) error {
	var value string
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := PrivacyType(value)
	for _, existing := range AllowedPrivacyTypeEnumValues {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid PrivacyType", value)
}

// NewPrivacyTypeFromValue returns a pointer to a valid PrivacyType
// for the value passed as argument, or an error if the value passed is not allowed by the enum
func NewPrivacyTypeFromValue(v string) (*PrivacyType, error) {
	ev := PrivacyType(v)
	if ev.IsValid() {
		return &ev, nil
	} else {
		return nil, fmt.Errorf("invalid value '%v' for PrivacyType: valid values are %v", v, AllowedPrivacyTypeEnumValues)
	}
}

// IsValid return true if the value is valid for the enum, false otherwise
func (v PrivacyType) IsValid() bool {
	for _, existing := range AllowedPrivacyTypeEnumValues {
		if existing == v {
			return true
		}
	}
	return false
}

// Ptr returns reference to PrivacyType value
func (v PrivacyType) Ptr() *PrivacyType {
	return &v
}

type NullablePrivacyType struct {
	value *PrivacyType
	isSet bool
}

func (v NullablePrivacyType) Get() *PrivacyType {
	return v.value
}

func (v *NullablePrivacyType) Set(val *PrivacyType) {
	v.value = val
	v.isSet = true
}

func (v NullablePrivacyType) IsSet() bool {
	return v.isSet
}

func (v *NullablePrivacyType) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePrivacyType(val *PrivacyType) *NullablePrivacyType {
	return &NullablePrivacyType{value: val, isSet: true}
}

func (v NullablePrivacyType) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePrivacyType) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
