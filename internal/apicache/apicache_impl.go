package apicache

import (
	cache "github.com/pmylund/go-cache"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapireq"
)

func (c *AuthzCache) LookupUserId(username string) (string, error) {
	var user authzapireq.APIIdentity
	obj, exists := c.IdentityCache.Get(username)
	if !exists {
		var err error // do not use `:=` to avoid shadowing the top-scoped `user` variable!
		user, err = c.AuthzClient.GetIdentity(username)
		if err != nil {
			return "", err
		}
		// store it in the cache. Collisions between user ID and Name excluded as Authz API accepts either name or ID in API URL paths.
		c.IdentityCache.Set(user.ID, user, cache.DefaultExpiration)
		c.IdentityCache.Set(user.Upn, user, cache.DefaultExpiration)
	} else {
		user = obj.(authzapireq.APIIdentity)
	}
	return user.ID, nil
}

func (c *AuthzCache) GetIdentity(id string) (authzapireq.APIIdentity, error) {
	var user authzapireq.APIIdentity
	obj, exists := c.IdentityCache.Get(id)
	if !exists {
		// does not exist in cache, ask Authz API
		var err error // do not use `:=` to avoid shadowing the top-scoped `user` variable!
		user, err = c.AuthzClient.GetIdentity(id)
		if err != nil {
			return user, err
		}

		// store it in the cache. Collisions between user ID and Name excluded as Authz API accepts either name or ID in API URL paths.
		c.IdentityCache.Set(user.ID, user, cache.DefaultExpiration)
		c.IdentityCache.Set(user.Upn, user, cache.DefaultExpiration)
	} else {
		user = obj.(authzapireq.APIIdentity)
	}
	return user, nil
}

func (c *AuthzCache) LookupGroupId(groupname string) (string, error) {
	var group authzapireq.APIGroup
	obj, exists := c.GroupCache.Get(groupname)
	if !exists {
		var err error // do not use `:=` to avoid shadowing the top-scoped `group` variable!
		group, err = c.AuthzClient.GetGroup(groupname)
		if err != nil {
			return "", err
		}
		// store it in the cache. Collisions between group ID and Name excluded as Authz API accepts either name or ID in API URL paths.
		c.GroupCache.Set(group.ID, group, cache.DefaultExpiration)
		c.GroupCache.Set(group.GroupIdentifier, group, cache.DefaultExpiration)
	} else {
		group = obj.(authzapireq.APIGroup)
	}
	return group.ID, nil
}

func (c *AuthzCache) GetGroup(id string) (authzapireq.APIGroup, error) {
	var group authzapireq.APIGroup
	obj, exists := c.GroupCache.Get(id)
	if !exists {
		// does not exist in cache, ask Authz API
		var err error // do not use `:=` to avoid shadowing the top-scoped `group` variable!
		group, err = c.AuthzClient.GetGroup(id)
		if err != nil {
			return group, err
		}

		// store it in the cache. Collisions between group ID and Name excluded as Authz API accepts either name or ID in API URL paths.
		c.GroupCache.Set(group.ID, group, cache.DefaultExpiration)
		c.GroupCache.Set(group.GroupIdentifier, group, cache.DefaultExpiration)
	} else {
		group = obj.(authzapireq.APIGroup)
	}
	return group, nil
}

func (c *AuthzCache) GetLoA(id string) (string, error) {
	var loa string
	obj, exists := c.LoACache.Get(id)
	if !exists {
		var err error // do not use `:=` to avoid shadowing the top-scoped `loa` variable!
		// does not exist in cache, ask Authz API
		loa, err = c.AuthzClient.GetLoA(id)
		if err != nil {
			return loa, err
		}

		// store it in the cache
		c.LoACache.Set(id, loa, cache.DefaultExpiration)
	} else {
		loa = obj.(string)
	}
	return loa, nil
}
