package apicache

import (
	"time"

	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapireq"

	"github.com/go-logr/logr"

	cache "github.com/pmylund/go-cache"
)

// ApiCacheClient will allow to access any of the required API caches
type AuthzAPICache interface {
	GetIdentity(id string) (authzapireq.APIIdentity, error)
	GetGroup(id string) (authzapireq.APIGroup, error)

	LookupUserId(name string) (string, error)
	LookupGroupId(name string) (string, error)

	// TODO: expose GetLoaByValue(int) and GetLoaById(string)

	// Applications and Roles are never cached since they should be reconciled ASAP
}

// AuthzCache is an meta cache object containing all required caches
type AuthzCache struct {
	logr.Logger
	// AuthzClient will be used to communicate with Authz API
	AuthzClient authzapireq.AuthzClient
	// GroupCache is the Cache of Groups in Authz
	GroupCache cache.Cache
	// IdentityCache is the Cache of Identities in Authz
	IdentityCache cache.Cache
	// LoACache is the Cache of Assurances Levels in Authz
	LoACache cache.Cache
}

// NewAuthzCacheClient will return a custom cache containing all the required caches
func NewApiCacheClient(log logr.Logger, authz authzapireq.AuthzClient) AuthzCache {
	// user and group metadata can be cached for up to one week
	// since this information changes infrequently
	IdentityCache := cache.New(7*24*time.Hour, 1*time.Hour)
	GroupCache := cache.New(7*24*time.Hour, 1*time.Hour)
	// LoA levels can be cached forever, we don't expect them to change
	LoACache := cache.New(cache.NoExpiration, cache.NoExpiration)

	return AuthzCache{
		Logger:        log,
		AuthzClient:   authz,
		GroupCache:    *GroupCache,
		IdentityCache: *IdentityCache,
		LoACache:      *LoACache,
	}
}
