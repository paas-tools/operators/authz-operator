/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"sort"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/equality"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapiv1"
)

// ApplicationRegistrationStatusReconciler reconciles *some* fields of the status of an ApplicationRegistration object
// It sets the following fields:
// - status.exportedDetails.roles
type ApplicationRegistrationExportReconciler struct {
	client.Client
	Log            logr.Logger
	Scheme         *runtime.Scheme
	AuthzApiClient *authzapiv1.APIClient

	// ExportAllAppsEvents is used to trigger regular exports of all objects
	ExportAllAppsEvents chan event.GenericEvent
}

func (r *ApplicationRegistrationExportReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		Named("applicationregistration_exporter"). // Must be compatible with a Prometheus metric name i.e. alphanum + underscore

		// whenever we get an event on this channel, we trigger an export ("reconciliation") for all applications
		WatchesRawSource(
			&source.Channel{Source: r.ExportAllAppsEvents},
			handler.EnqueueRequestsFromMapFunc(
				func(ctx context.Context, a client.Object) []reconcile.Request {
					// note: we ignore the input object "a", it comes from the channel and is just a dummy
					r.Log.V(6).Info("Got virtual event to reconcile all ApplicationRegistrations")

					// get all applicationregistrations across all namespaces
					applicationList := &webservicesv1alpha1.ApplicationRegistrationList{}
					err := r.Client.List(ctx, applicationList)
					if err != nil {
						r.Log.Error(err, "Couldn't fetch list of ApplicationRegistrations")
						return []reconcile.Request{}
					}

					// convert them to reconciliation requests
					requests := make([]reconcile.Request, len(applicationList.Items))
					for i, app := range applicationList.Items {
						requests[i].Name = app.GetName()
						requests[i].Namespace = app.GetNamespace()
					}

					r.Log.V(6).Info(fmt.Sprintf("Will add %d items to the queue", len(requests)))
					// enqueue all of them
					return requests
				},
			),
		).Complete(r)
}

// +kubebuilder:rbac:groups=webservices.cern.ch,resources=applicationregistrations,verbs=get;list;watch
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=applicationregistrations/status,verbs=get;update;patch;create;update;patch;delete

func (r *ApplicationRegistrationExportReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("applicationregistration", req.NamespacedName)
	log.V(3).Info("Exporting details about ApplicationRegistration")

	// Fetch the Application from Kubernetes API
	application := &webservicesv1alpha1.ApplicationRegistration{}
	if err := r.Get(ctx, req.NamespacedName, application); err != nil {
		if apierrors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}
	// Do nothing for applications that are being deleted
	if application.GetDeletionTimestamp() != nil {
		return ctrl.Result{}, nil
	}

	// Ensure application has been fully initialized by the controller
	if application.Status.ID == "" {
		log.V(6).Info("Application registration has not been initialized yet")
		return reconcile.Result{Requeue: true}, nil
	}

	log = log.WithValues("appID", application.Status.ID)

	// Fetch roles from AuthZ API
	roles, err := GetExportedRoles(ctx, r.AuthzApiClient, application.Status.ID)
	if err != nil {
		// retry later
		log.Error(err, "Failed to get roles for application")
		return ctrl.Result{Requeue: true}, nil
	}
	log.V(6).Info("Got", "roles", roles) // debug

	// Make a copy of the original object
	// this allows us to check if anything changed
	existing := application.DeepCopyObject()

	// Set new Status fields
	application.Status.ExportedDetails.Roles = roles

	// Check if there are differences
	if equality.Semantic.DeepEqual(existing, application) {
		// nothing changed, no need to call API
		log.V(6).Info("No updates detected, skipping update call to API")
		return ctrl.Result{}, nil
	}

	// set last export timestamp, which allows us to understand when the data was last exported
	// note that this will trigger the other controllers that have a watch on this resource type
	application.Status.ExportedDetails.ExportTimestamp = metav1.Now()

	// Call API to update status of resource
	if err := r.Status().Update(ctx, application); err != nil {
		log.Error(err, fmt.Sprintf("%v failed to update the application", ErrClientK8s))
		return reconcile.Result{Requeue: true}, nil
	}

	// all good
	return ctrl.Result{}, nil
}

func GetExportedRoles(ctx context.Context, authzApiClient *authzapiv1.APIClient, appId string) ([]webservicesv1alpha1.ExportedRole, error) {
	var exportedRoles []webservicesv1alpha1.ExportedRole

	// /api/v1.0/Application/{id}/roles
	rolesEnvelope, _, err := authzApiClient.ApplicationAPI.ApplicationIdRolesGet(ctx, appId).Execute()
	if err != nil {
		return exportedRoles, err
	}
	if rolesEnvelope.GetPagination().Next.Get() != nil {
		return exportedRoles, fmt.Errorf("Pagination not implemented for roles endpoint")
	}

	for _, role := range rolesEnvelope.GetData() {
		roleId := getString(role.Id)
		// look up groups mapped to this role
		// /api/v1.0/Application/{id}/roles/{roleid}/groups
		groupsEnvelope, _, err := authzApiClient.ApplicationAPI.ApplicationIdRolesRoleidGroupsGet(ctx, appId, roleId).Execute()
		if err != nil {
			return exportedRoles, err
		}
		if groupsEnvelope.GetPagination().Next.Get() != nil {
			return exportedRoles, fmt.Errorf("Pagination not implemented for roles/groups endpoint")
		}

		var mappedGroups []webservicesv1alpha1.ExportedRoleGroup
		for _, group := range groupsEnvelope.GetData() {
			mappedGroups = append(mappedGroups, webservicesv1alpha1.ExportedRoleGroup{
				Name: getString(group.GroupIdentifier),
				ID:   getString(group.Id),
			})
		}

		// TODO: use cached AuthzApiClient after refactoring with new Api client
		loaEnvelope, _, err := authzApiClient.LevelOfAssuranceAPI.
			LevelOfAssuranceGet(ctx).
			Filter([]string{"id:" + getString(role.MinimumLoaId)}).
			Execute()
		if err != nil {
			return exportedRoles, fmt.Errorf("failed to lookup Level of Assurance for role %s: %w", roleId, err)
		}
		if loaEnvelope.GetPagination().Next.Get() != nil {
			return exportedRoles, fmt.Errorf("Pagination not implemented for LOA endpoint")
		}
		if len(loaEnvelope.Data) != 1 {
			return exportedRoles, fmt.Errorf("failed to lookup LoA %s: expected one return value, got %d", roleId, len(loaEnvelope.Data))
		}
		loaValue := fmt.Sprintf("%d", loaEnvelope.GetData()[0].Value)

		exportedRoles = append(exportedRoles,
			webservicesv1alpha1.ExportedRole{
				ID:              roleId,
				Name:            role.Name,
				DisplayName:     role.DisplayName,
				Description:     role.Description,
				Required:        *role.Required,
				ApplyToAllUsers: *role.ApplyToAllUsers,
				MinimumLoA:      loaValue,
				Multifactor:     *role.Multifactor,
				MappedGroups:    mappedGroups,
			},
		)
	}

	// Ensure that the returned list has a deterministic order, no matter in which order the items were returned by the API.
	// We use the ID for sorting.
	sort.SliceStable(exportedRoles, func(i, j int) bool {
		return exportedRoles[i].ID < exportedRoles[j].ID
	})

	return exportedRoles, nil
}

// getString is a helper function that takes a nullable string,
// and returns its value (if there is one) or returns an empty string.
func getString(v authzapiv1.NullableString) string {
	if !v.IsSet() || v.Get() == nil {
		return ""
	}
	return *v.Get()
}
