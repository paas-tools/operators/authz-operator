/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"

	"github.com/go-logr/logr"
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/apicache"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapireq"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// LifecycleReconciler reconciles a Lifecycle object
type LifecycleReconciler struct {
	client.Client
	Log           logr.Logger
	Scheme        *runtime.Scheme
	Authz         authzapireq.AuthzClient
	AuthzApiCache apicache.AuthzCache
	// LifecycleEvents channel is to send reconcile requests to the ApplicationRegistration controller
	LifecycleEvents chan event.GenericEvent
	// SyncAllAppsEvents channel is used to trigger a sync of all applications (from a timer)
	SyncAllAppsEvents chan event.GenericEvent
}

// the Reconcile method syncs all apps with AuthZ API
func (r *LifecycleReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	r.Log.V(3).Info("Starting sync of all apps with Authz API")
	// Get all managed applications
	apiAppList, err := r.Authz.GetMyApplications()
	if err != nil {
		r.Log.V(3).Error(err, "Error fetching applications from Authz API")
		return ctrl.Result{}, err
	}
	if len(apiAppList) == 0 {
		r.Log.V(3).Info("Got a empty list of applications from Authz API. This is suspicious, skipping sync of all apps to protect user applications from possible glitch in Authz API.")
		return ctrl.Result{}, nil
	}
	// Get all Apps on Cluster
	currentAppList, err := r.fetchApplicationsOnCluster(ctx)
	if err != nil {
		r.Log.V(3).Error(err, "Error fetching applicationregistrations from cluster")
		return ctrl.Result{}, err
	}
	// Index List for fast retrieval
	indexedCurrentAppList := indexAppListByAppId(currentAppList)
	// See difference between lists and update currentAppList based on apiApplist
	r.reconcileAppsOutOfSync(apiAppList, indexedCurrentAppList)
	r.Log.V(3).Info("Completed sync of all apps with Authz API")
	return ctrl.Result{}, nil
}

func (r *LifecycleReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		Named("lifecycle_syncallapps"). // Must be compatible with a Prometheus metric name i.e. alphanum + underscore
		WatchesRawSource(
			&source.Channel{Source: r.SyncAllAppsEvents},
			&handler.EnqueueRequestForObject{},
		).
		Complete(r)
}

func (r *LifecycleReconciler) fetchApplicationsOnCluster(ctx context.Context) (webservicesv1alpha1.ApplicationRegistrationList, error) {
	applicationList := &webservicesv1alpha1.ApplicationRegistrationList{}
	err := r.Client.List(ctx, applicationList)
	if err != nil {
		return webservicesv1alpha1.ApplicationRegistrationList{}, err
	}
	return *applicationList, nil
}

// reconcileAppsOutOfSync receives a list of Application from the API(apiAppList) and ensures that the correspondent applications(currentAppList)
// in the cluster has the same information. If there's a mismatch, ask the ApplicationRegistration controller to reconcile that application.
func (r *LifecycleReconciler) reconcileAppsOutOfSync(apiAppList []authzapireq.APIApplication, currentAppList map[string]webservicesv1alpha1.ApplicationRegistration) {
	// This Loop will go through each Application in the API:
	// If Application from API exists in the cluster, it will updated it if necessary, if they don't exist on the cluster it will log
	// If it doesn't exist in the API but in the clsuter, it will updated or deleted them from the cluster if it's status is different from "Creating"
	for _, application := range apiAppList {
		appReg, exists := currentAppList[application.ID]
		log := r.Log.WithValues("applicationID", application.ID)
		if exists {
			delete(currentAppList, application.ID)
			application, err := resolveOwnerAndAdministratorIDs(r.AuthzApiCache, application)
			if err != nil {
				log.V(6).WithValues("groupID", application.AdministratorsID).WithValues("ownerID", application.OwnerID).Error(err, "Failed to resolve Owner or Admin Group Name")
				continue // cannot properly determine if app is in sync without this information
			}
			owner, err := r.AuthzApiCache.GetIdentity(application.OwnerID)
			if err != nil {
				log.V(6).WithValues("ownerID", application.OwnerID).Error(err, "Failed to look up owner information")
				continue // cannot properly determine if app is in sync without this information
			}
			// this lets us know if the status needs to be updated. We will NOT actually
			// update the ApplicationRegistration status here, instead we request a full reconcile
			// of the ApplicationRegistration if a status update is needed.
			updated := updateAppStatus(&appReg.Status, application, owner, log)
			if updated {
				log.WithValues("applicationregistration", namespacedName(&appReg).String()).V(3).Info("Requesting reconciliation because app status is not in sync with latest data from Authz API")
				// ask ApplicationRegistration controller to reconcile this application registration
				r.LifecycleEvents <- event.GenericEvent{
					Object: &appReg,
				}
			}
		} else {
			// Application exists on API but probably does not belong to this Cluster, we don't have
			// a matching ApplicationRegistration
			log.V(6).Info("No ApplicationRegistration found in cluster for this applicationID, skipping")
		}
	}
	// Applications that were not found in apiAppList.
	// If they do not already have `DeletedFromAPI` status, request a reconcile to update the status
	for _, elem := range currentAppList {
		if elem.Status.ProvisioningStatus != webservicesv1alpha1.ProvisioningStatusDeletedFromAPI {
			r.Log.WithValues("applicationregistration", namespacedName(&elem).String()).V(3).Info("Requesting reconciliation because app has been deleted from Authz API")
			// ask ApplicationRegistration controller to reconcile this application registration
			r.LifecycleEvents <- event.GenericEvent{
				Object: &elem,
			}
		}
	}
}

// Converts ApplicationRegistrationList into Map using App.ID as index
func indexAppListByAppId(list webservicesv1alpha1.ApplicationRegistrationList) map[string]webservicesv1alpha1.ApplicationRegistration {
	data := make(map[string]webservicesv1alpha1.ApplicationRegistration)
	for _, app := range list.Items {
		data[app.Status.ID] = app
	}
	return data
}
