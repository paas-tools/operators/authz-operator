/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"sync"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/apicache"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapireq"
)

type AppStatusRefreshMode string

const (
	// With mode AppStatusRefreshModeSynchronous, each Reconcile talks to the Authz API, compares and
	// updates the ApplicationRegistration state in Kubernetes. Reconcile() is slow in this mode.
	AppStatusRefreshModeSynchronous AppStatusRefreshMode = "RefreshSynchronously"
	// With mode AppStatusRefreshModeAsync, Reconcile() only talks talks to the Authz API if
	// the state in Kubernetes requires performing some change in the API (e.g. register a new application,
	// update OIDC return URIs). If the state in Kubernetes is consistent, then Reconcile won't make any
	// call to the Authz API. Instead, it will request an asynchronous refresh via FullResyncRequests.
	// This allows Reconcile() to return very fast in most cases, while still propagating changes to the Authz API
	// quickly when something has changed in Kubernetes.
	AppStatusRefreshModeAsync AppStatusRefreshMode = "DelegateRefreshToBackgroundReconciler"
)

// ApplicationRegistrationReconciler reconciles a ApplicationRegistration object
type ApplicationRegistrationReconciler struct {
	client.Client
	Log           logr.Logger
	Scheme        *runtime.Scheme
	Authz         authzapireq.AuthzClient
	AuthzApiCache apicache.AuthzCache

	// see the AppStatusRefreshMode constants for explanations
	appStatusRefreshBehavior AppStatusRefreshMode
	// AppStatusRefreshRequests channel is used to request an asynchronous full (slow) reconciliation with authz API.
	// It receives sync requests from the foreground instance of the reconciler (in AppStatusRefreshModeAsync mode),
	// as well as sync requests from the Lifecycle controller (when syncing all apps with Authz API and finding some apps that need update).
	// The background instance of the reconciler uses mode AppStatusRefreshModeSynchronous and performs the full sync
	// on ApplicationRegistration received through this channel.
	AppStatusRefreshRequests chan event.GenericEvent
	// Because we'll run concurrent instances of the reconciler with different modes,
	// we need to synchronize them so they do not work on the same ApplicationRegistration resource
	// at the same time. This synchronization is required because the Reconcile() implementation is not re-entrant
	// when it has to make changes to the Authz API.
	ReconcilerProcessingInProgress map[string]bool
	ReconcilerSyncMutex            *sync.Mutex
}

func (r *ApplicationRegistrationReconciler) SetupWithManagerForForeground(mgr ctrl.Manager) error {
	r.appStatusRefreshBehavior = AppStatusRefreshModeAsync
	return ctrl.NewControllerManagedBy(mgr).
		Named("applicationregistration_foreground"). // Must be compatible with a Prometheus metric name i.e. alphanum + underscore
		For(&webservicesv1alpha1.ApplicationRegistration{}).
		Watches(&webservicesv1alpha1.OidcReturnURI{}, handler.EnqueueRequestsFromMapFunc(
			func(context context.Context, a client.Object) []reconcile.Request {
				log := r.Log.WithValues("Source", "OIDCReturnURI event handler", "Namespace", a.GetNamespace())
				// Fetch the Applications in the same namespace
				applications := &webservicesv1alpha1.ApplicationRegistrationList{}
				if err := mgr.GetClient().List(context, applications, &client.ListOptions{Namespace: a.GetNamespace()}); err != nil {
					if apierrors.IsNotFound(err) {
						log.Info("Application not found in namespace")
					} else {
						log.Error(err, "Couldn't list Applications in namespace")
					}
					return []reconcile.Request{}
				}
				requests := make([]reconcile.Request, len(applications.Items))
				for i, a := range applications.Items {
					requests[i].Name = a.GetName()
					requests[i].Namespace = a.GetNamespace()
				}
				return requests
			}),
		).
		Complete(r)
}

func (r *ApplicationRegistrationReconciler) SetupWithManagerForBackground(mgr ctrl.Manager) error {
	r.appStatusRefreshBehavior = AppStatusRefreshModeSynchronous
	return ctrl.NewControllerManagedBy(mgr).
		Named("applicationregistration_background"). // Must be compatible with a Prometheus metric name i.e. alphanum + underscore
		WatchesRawSource(
			// this source enables the foreground reconciler and the lifecycle controller to send reconcile requests for
			// ApplicationRegistration resources that should be synced with Authz API in the background.
			&source.Channel{Source: r.AppStatusRefreshRequests},
			&handler.EnqueueRequestForObject{},
		).
		Complete(r)
}

// +kubebuilder:rbac:groups=webservices.cern.ch,resources=applicationregistrations,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=applicationregistrations/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=oidcreturnuris,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=oidcreturnuris/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=secrets,verbs=create;delete;get;list;watch

func (r *ApplicationRegistrationReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("reconcilerMode", r.appStatusRefreshBehavior, "applicationregistration", req.NamespacedName)
	log.V(3).Info("Reconciling ApplicationRegistration")

	// Because we run multiple instances of the reconciler, we must avoid that the same resource is processed
	// by both controllers!
	if !r.acquire(req) {
		// simply requeue, we'll try again later
		log.V(8).Info("Another controller is already processing this resource")
		return ctrl.Result{Requeue: true}, nil
	}
	defer r.release(req)

	// Fetch the Application
	application := &webservicesv1alpha1.ApplicationRegistration{}
	if err := r.Get(ctx, req.NamespacedName, application); err != nil {
		if apierrors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}
	// Deletions: On resource deletion, it deletes from Authzsvc API. If it doesn't exist there, it removes the finalizer
	if application.GetDeletionTimestamp() != nil {
		if controllerutil.ContainsFinalizer(application, deleteFromApiFinalizer) {
			return r.cleanupApplicationRegistration(ctx, log, application)
		}
		return ctrl.Result{}, nil
	}

	handleRetryableErr := func(transientErr reconcileError, logstrFmt string) (ctrl.Result, error) {
		if transientErr.IsRetryable() {
			log.Error(transientErr, fmt.Sprintf(logstrFmt, transientErr.Unwrap()))
			r.setProvisioningStatus(log, &application.Status, transientErr)
			_, err := r.updateCRStatusorFailReconcile(ctx, log, application)
			// Error is returned to the controller to record it in the metrics, despite the extra log message generated
			return ctrl.Result{}, err
		}
		log.Error(transientErr, "Sanity check failed: Permanent error marked as transient!"+fmt.Sprintf(logstrFmt, transientErr.Unwrap()))
		return ctrl.Result{}, nil
	}

	// # Init

	if update := ensureSpecHasAppIdAndName(log, application); update {
		return r.updateCRorFailReconcile(ctx, log, application)
	}
	if err := validateSpec(application.Spec); err != nil {
		appErr := newApplicationError(err, ErrInvalidSpec)
		log.Error(err, fmt.Sprintf("%v failed to validate Application spec", appErr.Unwrap()))
		r.setProvisioningStatus(log, &application.Status, appErr)
		return r.updateCRStatusorFailReconcile(ctx, log, application)
	}
	if update := ensureStatusInit(application); update {
		log.Info("Initializing AppReg Status")
		return r.updateCRStatusorFailReconcile(ctx, log, application)
	}

	// no API application associated yet?
	if application.Status.ID == "" {
		log.Info("Ensuring a matching Application in the AuthzAPI")
		if transientErr := r.ensureAPIAppAndSyncStatus(log, application); transientErr != nil {
			return handleRetryableErr(transientErr, "%v retrying to ensure Application in the AuthzAPI")
		}
		return r.updateCRStatusorFailReconcile(ctx, log, application)
	}

	log = log.WithValues("appID", application.Status.ID)

	// Checking OIDC credentials
	if application.Status.RegistrationID == "" {
		log.Info("Ensuring OIDC registration in the AuthzAPI")
		if transientErr := r.ensureAPIOIDCandSyncStatus(log, application); transientErr != nil {
			return handleRetryableErr(transientErr, "%v retrying to ensure OIDC registration in the AuthzAPI")
		}
		return r.updateCRStatusorFailReconcile(ctx, log, application)
	}

	syncStatusInForeground := false

	// Check if OIDC return URIs in applicationregistration status are in sync with OIDCReturnURI resources
	desiredRedirectURIs, err := r.fetchOIDCRedirectURIs(application.Namespace)
	if err != nil {
		return ctrl.Result{Requeue: true}, err
	}
	// sync immediately (in foreground) to update OIDC return URIs when needed
	if application.Status.ProvisioningStatus == webservicesv1alpha1.ProvisioningStatusCreated &&
		!webservicesv1alpha1.SameSet(desiredRedirectURIs, application.Status.RedirectURIs) {
		syncStatusInForeground = true
	}

	// Sync immediately (in foreground) if this is a newly registered ApplicationRegistration and we haven't synced data yet
	if application.Status.CurrentOwnerUsername == "" {
		syncStatusInForeground = true
	}

	// we have an API application, and we need to reflect the API application's data (owner, admin group etc.) in the
	// ApplicationRegistration's Status. This is an expensive operation.

	// Depending on the reconciler mode, we may skip this part and delegate to background reconciler.
	if r.appStatusRefreshBehavior == AppStatusRefreshModeAsync && !syncStatusInForeground {
		log.V(8).Info("Skipping refresh of ApplicationRegistration status and delegating to background reconciler")
		r.AppStatusRefreshRequests <- event.GenericEvent{
			Object: application,
		}
		// and nothing else to do
		return ctrl.Result{}, nil
	}

	// else proceed with synchronous refresh of application status
	update, authzUpdate, transientErr := r.syncAPIAppStatus(log, application)
	switch {
	case transientErr != nil:
		return handleRetryableErr(transientErr, "%v retrying to sync Application with the AuthzAPI")
	case authzUpdate:
		_, err := r.updateCRStatusorFailReconcile(ctx, log, application)
		return ctrl.Result{Requeue: true}, err
	case update:
		log.Info("Synced AppReg with AuthzAPI Application")
		return r.updateCRStatusorFailReconcile(ctx, log, application)
	}

	update, authzUpdate, transientErr = r.syncAPIOIDCStatus(log, application, desiredRedirectURIs)
	switch {
	case transientErr != nil:
		return handleRetryableErr(transientErr, "%v retrying to sync OIDC with the AuthzAPI")
	case authzUpdate:
		_, err := r.updateCRStatusorFailReconcile(ctx, log, application)
		return ctrl.Result{Requeue: true}, err
	case update:
		log.Info("Synced AppReg with AuthzAPI OIDC Registration")
		return r.updateCRStatusorFailReconcile(ctx, log, application)
	}

	// Signal steady state
	if update := r.setProvisioningStatus(log, &application.Status, nil); update {
		return r.updateCRStatusorFailReconcile(ctx, log, application)
	}
	return ctrl.Result{}, nil
}

// cleanupApplicationRegistration deletes all associated resources (in particular AuthzAPI objects), and if they're gone it clears the finalizer
func (r *ApplicationRegistrationReconciler) cleanupApplicationRegistration(ctx context.Context, log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration) (
	reconcile.Result, error) {
	// 1. Get the associated Application from the API
	ownedApp, err := r.fetchOwnedApp(app.Status.ID)
	if err != nil {
		log.Error(err, fmt.Sprintf("%v unable to check for the owned Application", ErrAuthzAPITemp))
		return reconcile.Result{Requeue: true}, r.deleteApplicationFromAuthz(app.Status.ID)
	}
	// 2. If there isn't any, remove finalizer
	if ownedApp.ID == "" {
		log.Info("Deleting ApplicationRegistration")
		controllerutil.RemoveFinalizer(app, deleteFromApiFinalizer)
		return r.updateCRorFailReconcile(ctx, log, app)
	}
	// 3. Delete the associated Application and requeue
	log.Info("Deleting the associated Application from the AuthzAPI")
	if err := r.deleteApplicationFromAuthz(ownedApp.ID); err != nil {
		log.Error(err, fmt.Sprintf("%v can't delete owned Application from the AuthzAPI", err.Unwrap()))
	}
	// NOTE: Requeue because we need to confirm deletion from the AuthzAPI
	return reconcile.Result{Requeue: true}, nil
}

// updateCRorFailReconcile tries to update the Custom Resource and logs any error
func (r *ApplicationRegistrationReconciler) updateCRorFailReconcile(ctx context.Context, log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration) (
	reconcile.Result, error) {
	if err := r.Update(ctx, app); err != nil {
		log.Error(err, fmt.Sprintf("%v failed to update the application", ErrClientK8s))
		return reconcile.Result{Requeue: true}, nil
	}
	return reconcile.Result{}, nil
}

// updateCRStatusorFailReconcile tries to update the Custom Resource Status and logs any error
func (r *ApplicationRegistrationReconciler) updateCRStatusorFailReconcile(ctx context.Context, log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration) (
	reconcile.Result, error) {
	if err := r.Status().Update(ctx, app); err != nil {
		log.Error(err, fmt.Sprintf("%v failed to update the application status", ErrClientK8s))
		return reconcile.Result{Requeue: true}, nil
	}
	return reconcile.Result{}, nil
}

// Acquire checks if a Request is already being processed and marks it as such
func (r *ApplicationRegistrationReconciler) acquire(req ctrl.Request) bool {
	// Lock the mutex to access the processed map
	mutex := r.ReconcilerSyncMutex
	processingInProgress := r.ReconcilerProcessingInProgress
	mutex.Lock()
	defer mutex.Unlock()
	key := req.NamespacedName.String()
	// Check if the Request is already being processed
	if processingInProgress[key] {
		return false
	}
	// Mark the Request as being processed
	processingInProgress[key] = true
	return true
}

// Release removes a Request key from the processingInProgress map
func (r *ApplicationRegistrationReconciler) release(req ctrl.Request) {
	mutex := r.ReconcilerSyncMutex
	processingInProgress := r.ReconcilerProcessingInProgress
	// Lock the mutex to access the processed map
	mutex.Lock()
	defer mutex.Unlock()
	// Remove the key from the processed map
	delete(processingInProgress, req.NamespacedName.String())
}
