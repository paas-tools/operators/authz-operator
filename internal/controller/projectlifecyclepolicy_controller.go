/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"

	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	apimachineryvalidation "k8s.io/apimachinery/pkg/util/validation"

	// for ConsoleLink. See https://sdk.operatorframework.io/docs/building-operators/golang/advanced-topics/#register-with-the-managers-scheme
	// Use go get github.com/openshift/api@release-4.6
	"github.com/openshift/api/annotations"
	consolev1 "github.com/openshift/api/console/v1"
)

// Variables used in function `ensureProjectMetadata`
const (
	labelOwner            string = "lifecycle.webservices.cern.ch/owner"
	labelResourceCategory string = "lifecycle.webservices.cern.ch/resourceCategory"
	labelAdminGroup       string = "lifecycle.webservices.cern.ch/adminGroup"
	labelDepartment       string = "lifecycle.webservices.cern.ch/cernDepartment"
	labelGroup            string = "lifecycle.webservices.cern.ch/cernGroup"
	// Blocked label and annotations as described here: https://okd-internal.docs.cern.ch/operations/project-blocking/
	LabelBlockedNamespace string = "okd.cern.ch/project-blocked"
	// Annotation that defines when the project should be deleted
	AnnotationDeleteNamespaceTimestamp  string = "lifecycle.webservices.cern.ch/delete-namespace-after"
	AnnotationBlockedNamespaceReason    string = "okd.cern.ch/blocked-reason"
	AnnotationBlockedNamespaceTimestamp string = "okd.cern.ch/blocked-timestamp"
	AnnotationLifecycleBlockedReason    string = "PendingDeletionAfterLifecycleDeletedApplication"
	// deleteNamespaceAfter is the time that the project will be blocked before deletion, deletion date set on annotation BlockAndDeleteAfterGracePeriod
	// This value is used to set the annotationDeleteNamespaceTimestamp, currently we set to 720h (24h x 30days)
	deleteNamespaceAfter time.Duration = time.Hour * 720
)

// ProjectLifecyclePolicyReconciler reconciles a ProjectLifecyclePolicy object
type ProjectLifecyclePolicyReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	// Base URL to generate a link to manage an application in the Application Portal.
	// The application ID will be appended to the base URL to generate to full URL.
	ApplicationPortalBaseUrl string
	// The text to show in the NamespaceDashboard ConsoleLink providing the general-purpose link from the OKD console
	// to the application's management page in the Application Portal.
	ApplicationPortalLinkText string
	// The text to show in the NamespaceDashboard ConsoleLink providing info about the current application category
	// in the Application Portal when category is Undefined
	ApplicationCategoryUndefinedLinkText string
	// The text to show in the NamespaceDashboard ConsoleLink providing info about the current application category
	// in the Application Portal when category is Test
	ApplicationCategoryTestLinkText string
	// The text to show in the NamespaceDashboard ConsoleLink providing info about the current application category
	// in the Application Portal when category is Personal
	ApplicationCategoryPersonalLinkText string
	// The text to show in the NamespaceDashboard ConsoleLink providing info about the current application category
	// in the Application Portal when category is Official
	ApplicationCategoryOfficialLinkText string
}

// +kubebuilder:rbac:groups=webservices.cern.ch,resources=projectlifecyclepolicies,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=projectlifecyclepolicies/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=,resources=namespaces,verbs=get;list;watch;create;update;patch;delete

func (r *ProjectLifecyclePolicyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("projectlifecyclepolicy", req.NamespacedName)

	log.V(3).Info("Reconciling ProjectLifecyclePolicy")

	// Fetch the ProjectLifecyclePolicy instance
	policy := &webservicesv1alpha1.ProjectLifecyclePolicy{}
	err := r.Get(ctx, req.NamespacedName, policy)
	if err != nil {
		if apierrors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			log.V(3).Info("ProjectLifecyclePolicy resource not found. Ignoring since object must be deleted.")
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		log.Error(err, "Failed to get ProjectLifecyclePolicy.")
		return ctrl.Result{}, err
	}

	// we don't need a finalizer. Do nothing if resource is being deleted.
	if policy.GetDeletionTimestamp() != nil {
		log.V(5).Info("ProjectLifecyclePolicy is being deleted, nothing to do")
		return ctrl.Result{}, nil
	}

	// retrieve ApplicationRegistration in same namespace
	applications := &webservicesv1alpha1.ApplicationRegistrationList{}
	if err := r.List(ctx, applications, &client.ListOptions{Namespace: req.Namespace}); err != nil {
		if apierrors.IsNotFound(err) {
			// cannot do anything if there is no ApplicationRegistration.
			err := r.logInfoAndSetCannotApplyStatus(ctx, log, policy, "No ApplicationRegistration found in namespace")
			return ctrl.Result{}, err
		} else {
			r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Couldn't list Applications in namespace")
			return ctrl.Result{}, err
		}
	}

	if len(applications.Items) == 0 {
		// cannot do anything if there is no ApplicationRegistration.
		err := r.logInfoAndSetCannotApplyStatus(ctx, log, policy, "No ApplicationRegistration found in namespace")
		return ctrl.Result{}, err
	}

	// it only makes sense that we have exactly one ApplicationRegistration in the namespace.
	// Behavior is undefined is there are more than one.
	// Just take the first one.
	appreg := applications.Items[0]

	// SAFEGUARD: do nothing if ApplicationRegistration is being deleted.
	// This is very important because of the project deletion policy: without this safeguard
	// deleting an ApplicationRegistration would automatically delete the parent namespace and this is
	// not the intended behavior.
	// We want to react to removal of applications from the Application Portal _initiated by the portal_,
	// not ones initiated by deleting the ApplicationRegistration resource.
	if appreg.GetDeletionTimestamp() != nil {
		err := r.logInfoAndSetCannotApplyStatus(ctx, log, policy, "ApplicationRegistration is being deleted")
		return ctrl.Result{}, err
	}

	if appreg.Status.ProvisioningStatus == webservicesv1alpha1.ProvisioningStatusDeletedFromAPI {
		switch policy.Spec.ApplicationDeletedFromAuthApiPolicy {
		case webservicesv1alpha1.AppDeletionPolicyBlockAndDeleteAfterGracePeriod:
			// soft-delete ApplicationRegistration's parent namespace if project lifecycle policy says so
			// soft-deletion means the namespace will be set as blocked and another component will delete the namespace on a later time (30d as of Jan/2023)
			// We never automatically undo the blocking, it can only be done manually following these steps: https://okd-internal.docs.cern.ch/operations/project-blocking/
			namespace := &corev1.Namespace{}
			err = r.Get(ctx, types.NamespacedName{Name: req.Namespace}, namespace)
			if err != nil {
				r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Failed to retrieve parent namespace following deletion of the application from API")
				return ctrl.Result{}, err
			}
			softDeleteProject(log, namespace)
			if err := r.Update(ctx, namespace); err != nil {
				r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Failed to update parent namespace following deletion of the application from API")
				return ctrl.Result{}, err
			}
		case webservicesv1alpha1.AppDeletionPolicyDeleteNamespace:
			// delete the ApplicationRegistration's parent namespace if project lifecycle policy says so.
			namespace := &corev1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name: appreg.Namespace,
				},
			}
			log.Info("Deleting parent namespace because project lifecycle policy is DeleteNamespace and ApplicationRegistration status was DeletedFromAPI")
			if err := r.Delete(ctx, namespace); err != nil {
				r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Failed to delete parent namespace following deletiong of the application")
				return ctrl.Result{}, err
			}
			// no point in continuing reconciliation since namespace is being deleted
			return ctrl.Result{}, nil
		case webservicesv1alpha1.AppDeletionPolicyIgnoreAndPreserveNamespace:
			// leave namespace as-is, nothing else to do if DeletedFromAPI
			log.V(5).Info("Ignoring parent namespace because project lifecycle policy is IgnoreAndPreserveNamespace and ApplicationRegistration status was DeletedFromAPI")
		default:
			log.Error(nil, fmt.Sprintf("ApplicationDeletedFromAuthApiPolicy \"%s\" not expected.", policy.Spec.ApplicationDeletedFromAuthApiPolicy))
		}
		err = r.logSuccessfulInfoOnStatus(ctx, log, policy, "ApplicationRegistration deleted from the API")
		return ctrl.Result{}, err
	}

	// if the ApplicationRegistration is not in "Created" state, then nothing useful for us to do
	if appreg.Status.ProvisioningStatus != webservicesv1alpha1.ProvisioningStatusCreated {
		err := r.logInfoAndSetCannotApplyStatus(ctx, log, policy, "ApplicationRegistration status is not Created")
		return ctrl.Result{}, err
	}

	// if user updated application's Description in app portal,
	// or changed ownership, adminGroup or Resoruce category,
	// propagate this change to the Openshift project metadata
	if err := r.ensureProjectMetadata(*policy, appreg); err != nil {
		r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Failed to sync project description")
		return ctrl.Result{}, err
	}

	// Create/update rolebinding that will grant permissions in the OKD project to the owner and administrator group
	// defined in the Application Portal.
	// If the rolebinding exists already, we take ownership of it. It is typically necessary for the project
	// template that creates the ProjectLifecyclePolicy to _also_ pre-create the rolebinding, so the creator
	// of the project immediately has access without having to wait for the ProjectLifecyclePolicy to reconcile.
	if err := r.ensureOwnerRoleBinding(*policy, appreg); err != nil {
		r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Failed to apply owner RoleBinding")
		return ctrl.Result{}, err
	}

	// create/update consolelinks
	appManagementUrl := r.ApplicationPortalBaseUrl + appreg.Status.ID
	// Link1: General-purpose link to application management
	// controlled by policy.Spec.ApplicationPortalManagementLink
	if err := r.ensureConsoleLink(*policy, policy.Namespace+"-application-management",
		r.ApplicationPortalLinkText, appManagementUrl, policy.Spec.ApplicationPortalManagementLink); err != nil {
		r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Failed to apply ConsoleLink for application management")
		return ctrl.Result{}, err
	}
	// Link2: explanation about current application category and link to application management to change it.
	// controlled by policy.Spec.ApplicationCategoryLink
	var categoryLinkText string
	switch category := appreg.Status.CurrentResourceCategory; category {
	case webservicesv1alpha1.ResourceCategoryTest:
		categoryLinkText = r.ApplicationCategoryTestLinkText
	case webservicesv1alpha1.ResourceCategoryOfficial:
		categoryLinkText = r.ApplicationCategoryOfficialLinkText
	case webservicesv1alpha1.ResourceCategoryPersonal:
		categoryLinkText = r.ApplicationCategoryPersonalLinkText
	default:
		// Undefined or empty string
		categoryLinkText = r.ApplicationCategoryUndefinedLinkText
	}
	if err := r.ensureConsoleLink(*policy, policy.Namespace+"-category-management",
		categoryLinkText, appManagementUrl, policy.Spec.ApplicationCategoryLink); err != nil {
		r.logErrorAndSetFailedStatus(ctx, log, policy, err, "Failed to set ConsoleLink for category management")
		return ctrl.Result{}, err
	}

	// Update the Status of the ProjectLifecyclePolicy to indicate successful sync
	if err := r.logSuccessfulInfoOnStatus(ctx, log, policy, "Awaiting next reconciliation"); err != nil {
		log.Error(err, "Failed to update ProjectLifecyclePolicy Status")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *ProjectLifecyclePolicyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webservicesv1alpha1.ProjectLifecyclePolicy{}).
		// With Owns RoleBinding, this will watch for any event to any RoleBinding in the cluster.
		// When the rolebinding is owned by a ProjectLifecyclePolicy, it will create a reconcile Request to the
		// owner ProjectLifecyclePolicy
		Owns(&rbacv1.RoleBinding{}).
		// Since the project lifecycle policy is driven by the ApplicationRegistration's
		// status, we want to reconcile whenever there's a change in an
		// ApplicationRegistration in the same namespace (we expect only one such ApplicationRegistration)
		Watches(&webservicesv1alpha1.ApplicationRegistration{}, handler.EnqueueRequestsFromMapFunc(
			func(context context.Context, a client.Object) []reconcile.Request {
				namespace := a.GetNamespace()
				log := r.Log.WithValues("Source", "ApplicationRegistration watch", "Namespace", namespace)
				// Fetch the ProjectLifecyclePolicies in the same namespace
				policies := &webservicesv1alpha1.ProjectLifecyclePolicyList{}
				if err := mgr.GetClient().List(context, policies, &client.ListOptions{Namespace: namespace}); err != nil {
					if apierrors.IsNotFound(err) {
						log.V(5).Info("No ProjectLifecyclePolicy found in namespace")
					} else {
						log.Error(err, "Couldn't list ProjectLifecyclePolicies in namespace")
					}
					return []reconcile.Request{}
				}
				// reconcile all ProjectLifecyclePolicies found in the namespace of the ApplicationRegistration,
				// but it only makes sense that we have exactly one
				requests := make([]reconcile.Request, len(policies.Items))
				for i, p := range policies.Items {
					requests[i].Name = p.GetName()
					requests[i].Namespace = p.GetNamespace()
				}
				return requests
			}),
		).
		Complete(r)
}

// updates the CR Status to indicate a failure.
func (r *ProjectLifecyclePolicyReconciler) logErrorAndSetFailedStatus(ctx context.Context, log logr.Logger, policy *webservicesv1alpha1.ProjectLifecyclePolicy, err error, message string) {
	log.Error(err, message)
	meta.SetStatusCondition(&policy.Status.Conditions, metav1.Condition{
		Type:    webservicesv1alpha1.ConditionTypeAppliedProjectLifecyclePolicy,
		Status:  metav1.ConditionFalse,
		Reason:  webservicesv1alpha1.ConditionReasonFailed,
		Message: message,
	})
	// try to update status in API, but ignore error if we couldn't since we have an error already
	r.Status().Update(ctx, policy)
}

// updates the CR Status to indicate a situation that prevents applying the ProjectLifecyclePolicy.
func (r *ProjectLifecyclePolicyReconciler) logInfoAndSetCannotApplyStatus(ctx context.Context, log logr.Logger, policy *webservicesv1alpha1.ProjectLifecyclePolicy, message string) error {
	// use V(5) since it can be a permanent condition, so not very interesting to log again and again
	log.V(5).Info(message)
	meta.SetStatusCondition(&policy.Status.Conditions, metav1.Condition{
		Type:    webservicesv1alpha1.ConditionTypeAppliedProjectLifecyclePolicy,
		Status:  metav1.ConditionFalse,
		Reason:  webservicesv1alpha1.ConditionReasonCannotApply,
		Message: message,
	})
	// try to update status in API, return error if we couldn't
	return r.Status().Update(ctx, policy)
}

// updates the CR Status to indicate a situation where ApplicationRegistration has been deleted from API
func (r *ProjectLifecyclePolicyReconciler) logSuccessfulInfoOnStatus(ctx context.Context, log logr.Logger, policy *webservicesv1alpha1.ProjectLifecyclePolicy, message string) error {
	// use V(5) since it can be a permanent condition, so not very interesting to log again and again
	log.V(5).Info(message)
	meta.SetStatusCondition(&policy.Status.Conditions, metav1.Condition{
		Type:    webservicesv1alpha1.ConditionTypeAppliedProjectLifecyclePolicy,
		Status:  metav1.ConditionTrue,
		Reason:  webservicesv1alpha1.ConditionReasonSuccessful,
		Message: message,
	})
	// try to update status in API, return error if we couldn't
	return r.Status().Update(ctx, policy)
}

func (r *ProjectLifecyclePolicyReconciler) ensureProjectMetadata(policy webservicesv1alpha1.ProjectLifecyclePolicy, appreg webservicesv1alpha1.ApplicationRegistration) error {
	if !policy.Spec.SyncProjectMetadata {
		// nothing to do
		return nil
	}

	namespace := corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: appreg.Namespace,
		},
	}

	// This function is an auxiliary function from controller runtime that it will either
	// create or update an object but before doing it, it will always run MutateFn, the function specified below.
	// Does nothing if there's no actual change to the resource (here, the namespace)
	_, err := controllerutil.CreateOrUpdate(context.TODO(), r.Client, &namespace, func() error {
		// Mutate function that sets the desired state of the RoleBinding resource.
		ModifyProjectMetadata(policy, appreg, &namespace)
		return nil
	})
	return err
}

// ModifyProjectMetadata sets the desired value for the namespace annotation and adds custom labels, CurrentOwner, CurrentAdminGroup and ResourceCategory, to the Openshift project
func ModifyProjectMetadata(policy webservicesv1alpha1.ProjectLifecyclePolicy, appreg webservicesv1alpha1.ApplicationRegistration, namespace *corev1.Namespace) {
	//Set Annotations
	a := namespace.Annotations
	if a == nil {
		a = make(map[string]string)
	}
	a[annotations.OpenShiftDescription] = appreg.Status.CurrentDescription
	namespace.SetAnnotations(a)
	//Set Labels
	labels := namespace.Labels
	if labels == nil {
		labels = make(map[string]string)
	}
	labels[labelOwner] = validLabelOrEmptyString(appreg.Status.CurrentOwnerUsername)
	labels[labelResourceCategory] = string(appreg.Status.CurrentResourceCategory)
	labels[labelAdminGroup] = validLabelOrEmptyString(appreg.Status.CurrentAdminGroup)
	labels[labelGroup] = validLabelOrEmptyString(appreg.Status.CurrentGroup)
	labels[labelDepartment] = validLabelOrEmptyString(appreg.Status.CurrentDepartment)
	namespace.SetLabels(labels)
}

// Check value is a valid label with IsValidLabelValue from controller runtime
// If not then use empty string instead of the invalid value.
func validLabelOrEmptyString(s string) string {
	// IsValidLabelValue returns a list of error, if there's any then return empty string
	for range apimachineryvalidation.IsValidLabelValue(s) {
		return ""
	}
	return s
}

func (r *ProjectLifecyclePolicyReconciler) ensureOwnerRoleBinding(policy webservicesv1alpha1.ProjectLifecyclePolicy, appreg webservicesv1alpha1.ApplicationRegistration) error {
	if policy.Spec.ApplicationOwnerClusterRole == "" || policy.Spec.ApplicationOwnerRoleBindingName == "" {
		// nothing to do
		return nil
	}

	var ownerRolebinding rbacv1.RoleBinding
	// We have to do this part here as MutateFn cannot mutate object
	// name and/or object namespace
	ownerRolebinding.Namespace = policy.Namespace
	ownerRolebinding.Name = policy.Spec.ApplicationOwnerRoleBindingName

	// This function is an auxiliary function from controller runtime that it will either
	// create or update an object but before doing it, it will always run MutateFn, the function specified below
	_, err := controllerutil.CreateOrUpdate(context.TODO(), r.Client, &ownerRolebinding, func() error {
		// Mutate function that sets the desired state of the RoleBinding resource.
		ModifyOwnerRoleBinding(policy, appreg, &ownerRolebinding)

		// The ProjectLifecyclePolicy should be the owner-controller of that RoleBinding for
		// 1. automatic garbage collection of the roleBinding
		// 2. automatic reconciliation of the owner if the roleBinding is modified (see `Owns`
		//    in SetupWithManager)
		return controllerutil.SetControllerReference(&policy, &ownerRolebinding, r.Scheme)
	})
	return err
}

// Function that sets the desired state of the RoleBinding resource.
func ModifyOwnerRoleBinding(policy webservicesv1alpha1.ProjectLifecyclePolicy, appreg webservicesv1alpha1.ApplicationRegistration, ownerRolebinding *rbacv1.RoleBinding) {
	ownerRolebinding.RoleRef = rbacv1.RoleRef{
		APIGroup: rbacv1.SchemeGroupVersion.Group,
		Kind:     "ClusterRole",
		Name:     policy.Spec.ApplicationOwnerClusterRole,
	}
	// reset any existing member
	ownerRolebinding.Subjects = []rbacv1.Subject{}
	// there's always a owner
	ownerRolebinding.Subjects = append(ownerRolebinding.Subjects, rbacv1.Subject{
		APIGroup: rbacv1.SchemeGroupVersion.Group,
		Kind:     rbacv1.UserKind,
		Name:     appreg.Status.CurrentOwnerUsername,
	})
	// specifying an e-group as a subject works thanks to https://gitlab.cern.ch/paas-tools/okd4-deployment/egroup-sync,
	// which autodetects groups used in RoleBindings and syncs them with e-groups
	if appreg.Status.CurrentAdminGroup != "" {
		ownerRolebinding.Subjects = append(ownerRolebinding.Subjects, rbacv1.Subject{
			APIGroup: rbacv1.SchemeGroupVersion.Group,
			Kind:     rbacv1.GroupKind,
			Name:     appreg.Status.CurrentAdminGroup,
		})
	}
}

func (r *ProjectLifecyclePolicyReconciler) ensureConsoleLink(
	policy webservicesv1alpha1.ProjectLifecyclePolicy,
	consoleLinkName string,
	consoleLinkText string,
	consoleLinkHref string,
	present bool) error {

	var consoleLink consolev1.ConsoleLink
	consoleLink.Name = consoleLinkName

	if !present {
		// delete existing link if it exists
		if err := r.Get(context.TODO(), types.NamespacedName{Name: consoleLink.Name}, &consoleLink); err != nil {
			if apierrors.IsNotFound(err) {
				// it isn't there, we're all good then
				return nil
			} else {
				return err
			}
		}
		if err := r.Delete(context.TODO(), &consoleLink); err != nil {
			return err
		}
		// if we deleted successfully
		return nil
	}

	// case present == true

	// get the policy's parent namespace (for ownership of the ConsoleLink)
	var namespace corev1.Namespace
	if err := r.Get(context.TODO(), types.NamespacedName{Name: policy.Namespace}, &namespace); err != nil {
		return err
	}

	// This function is an auxiliary function from controller runtime that it will either
	// create or update an object but before doing it, it will always run MutateFn, the function specified below
	_, err := controllerutil.CreateOrUpdate(context.TODO(), r.Client, &consoleLink, func() error {
		// Mutate function that sets the desired state of the RoleBinding resource.
		modifyConsoleLink(policy, consoleLinkText, consoleLinkHref, &consoleLink)

		// Set the policy's namespace as the owner of the ConsoleLink so
		// it gets garbage-collected automatically when namespace is deleted.
		return controllerutil.SetOwnerReference(&namespace, &consoleLink, r.Scheme)
	})
	return err
}

func modifyConsoleLink(policy webservicesv1alpha1.ProjectLifecyclePolicy,
	consoleLinkText string,
	consoleLinkHref string,
	consoleLink *consolev1.ConsoleLink) {
	// this is the only location we can configure per namespace
	consoleLink.Spec.Location = consolev1.NamespaceDashboard
	consoleLink.Spec.NamespaceDashboard = &consolev1.NamespaceDashboardSpec{
		Namespaces: []string{policy.Namespace},
	}
	consoleLink.Spec.Href = consoleLinkHref
	consoleLink.Spec.Text = consoleLinkText
}

func softDeleteProject(log logr.Logger, namespace *corev1.Namespace) {
	// Initialization of labels and annotations to confirm they are not nil
	labels := namespace.Labels
	if labels == nil {
		labels = make(map[string]string)
	}
	annotations := namespace.Annotations
	if annotations == nil {
		annotations = make(map[string]string)
	}
	// If the label and annotation are set, there's nothing to do
	if labels[LabelBlockedNamespace] == "true" && annotations[AnnotationBlockedNamespaceReason] == AnnotationLifecycleBlockedReason {
		log.V(5).Info("Project already blocked and set to be deleted after grace period")
		return
	}
	log.Info("Blocking parent namespace because project lifecycle policy is BlockAndDeleteAfterGracePeriod and ApplicationRegistration status was DeletedFromAPI")
	// Retrieve time for the annotations below
	currentTime := time.Now()
	// Set Blocked Annotation timestamp unless namespace was already blocked, in which case we keep the timestamp of the earlier blocking.
	if namespace.Labels[LabelBlockedNamespace] != "true" || annotations[AnnotationBlockedNamespaceTimestamp] == "" {
		// RFC3339 is the same as ISO8601
		annotations[AnnotationBlockedNamespaceTimestamp] = currentTime.Format(time.RFC3339)
	}
	// Dedicated annotation for cleanup mechanism
	deletionTimestamp := currentTime.Add(deleteNamespaceAfter)
	// RFC3339 is the same as ISO8601
	annotations[AnnotationDeleteNamespaceTimestamp] = deletionTimestamp.Format(time.RFC3339)
	// The PendingDeletionAfterLifecycleDeletedApplication reason supersedes any other reason for namespace blocking.
	// This exact reason must be present together with the annotationDeleteNamespaceTimestamp for projects to be deleted at the end of the grace period.
	annotations[AnnotationBlockedNamespaceReason] = AnnotationLifecycleBlockedReason
	namespace.SetAnnotations(annotations)

	// Set blocked label to make applications/websites unavailable
	// https://okd-internal.docs.cern.ch/operations/project-blocking/
	// The logic is only valid when the labels are set only after the annotations
	labels[LabelBlockedNamespace] = "true"
	namespace.SetLabels(labels)
}
