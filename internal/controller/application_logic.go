package controller

// API user flows go here

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/asaskevich/govalidator"
	"github.com/go-logr/logr"
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/apicache"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapireq"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

const (
	// deleteFromApiFinalizer string that indicates we need to delete application from authz API if the ApplicationRegistration is deleted
	deleteFromApiFinalizer    = "webservices.cern.ch/delete-app-from-authz-api"
	k8sSecretOIDCClientSecret = "oidc-client-secret"
)

// ensureAPIAppAndSyncStatus first either creates a new Application in the AuthzAPI or we adopt ownership of an existing one with the same ApplicationIdentifier
// then it updates the Status with information from the AuthzAPI
func (r *ApplicationRegistrationReconciler) ensureAPIAppAndSyncStatus(log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration) (transientErr reconcileError) {
	createInAuthzOrAdopt := func() (authzapireq.APIApplication, reconcileError) {
		apiApp, err := r.createAppInAuthz(app.Spec)
		if err != nil {
			switch err.Unwrap() {
			case ErrApplicationAlreadyExists:
				log.Info("Adopt ownership: Application with the same applicationIdentifier already exists in the AuthzAPI, adopting it")
			case ErrApplicationConflict:
				log.Error(err, "Application not managed by us")
				return authzapireq.APIApplication{}, err
			default:
				return authzapireq.APIApplication{}, err
			}
		}
		return apiApp, nil
	}
	apiApp, err := createInAuthzOrAdopt()
	if err != nil {
		r.setProvisioningStatus(log, &app.Status, err)
		if err.IsRetryable() {
			return err
		}
		// In case of non-retriable error we've already updated the resource status to reflect a permanent failure.
		// There is no point in retrying later, so we now want the caller to just commit the
		// changes to the CR in the Kubernetes API and successfully complete reconciliation.
		return nil
	}

	owner, err2 := r.AuthzApiCache.GetIdentity(apiApp.OwnerID)
	if err2 != nil {
		log.Error(err, fmt.Sprintf("Failed to retrieve metadata of app owner '%s'", apiApp.OwnerID))
		return newApplicationError(err2, ErrInvalidOwner)
	}
	updateAppStatus(&app.Status, apiApp, owner, log)
	return nil
}

// ensureAPIOIDCandSyncStatus creates a new OIDC registration in the AuthzAPI for the owned Application or adopts the existing one,
// then updates the Status with info from the AuthzAPI
func (r *ApplicationRegistrationReconciler) ensureAPIOIDCandSyncStatus(log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration) (transientErr reconcileError) {
	createInAuthzOrAdopt := func() (authzapireq.APIRegistration, reconcileError) {
		apiReg, err := r.fetchOIDCReg(app.Status.ID, false)
		switch {
		case err != nil && err.IsRetryable():
			return authzapireq.APIRegistration{}, err
		case apiReg.RegistrationID != "":
			log.Info("Adopting OIDC Registration that already exists in the AuthzAPI")
			// TODO (alossent 31-May-23): this code path is not working for some reason, in status the state remains Creating and the registrationId is missing.
			return apiReg, err
		}
		apiReg, err = r.createOIDCinAuthz(*app)
		return apiReg, err
	}
	apiReg, err := createInAuthzOrAdopt()
	_, transientErr = r.ensureK8sClientSecret(context.TODO(), app.Namespace, apiReg)
	updateAppStatusOIDC(&app.Status, apiReg)
	if err != nil {
		r.setProvisioningStatus(log, &app.Status, err)
		if err.IsRetryable() {
			transientErr = err
			return
		}
	}
	return
}

// syncAPIAppStatus retrieves the Application from the API and syncs the information in the Status. It checks if the API version needs changes and performs them.
func (r *ApplicationRegistrationReconciler) syncAPIAppStatus(log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration) (update bool, authzUpdate bool, transientErr reconcileError) {
	retrieveFromAuthzHandle := func(appID string) (authzapireq.APIApplication, reconcileError) {
		apiApp, err := r.fetchOwnedApp(appID)
		if err != nil {
			log.Error(err, fmt.Sprintf("%v Failed to fetch owned Application", err.Unwrap()))
			return authzapireq.APIApplication{}, err
		}
		if apiApp == (authzapireq.APIApplication{}) {
			log.Info("Associated Application not found in Authz API!")
			return authzapireq.APIApplication{}, newApplicationError(nil, ErrAssociatedAppNotFound)
		}
		return apiApp, err
	}
	apiApp, err := retrieveFromAuthzHandle(app.Status.ID)
	if err != nil {
		r.setProvisioningStatus(log, &app.Status, err)
		update = true
		if err.IsRetryable() {
			return
		}
		err = nil
		return
	}
	owner, err2 := r.AuthzApiCache.GetIdentity(apiApp.OwnerID)
	if err2 != nil {
		log.Error(err, fmt.Sprintf("Failed to retrieve metadata of app owner '%s'", apiApp.OwnerID))
		return
	}
	update = updateAppStatus(&app.Status, apiApp, owner, log)
	// TODO: the following has several issues as of April 2021 and was disabled.
	// See https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/367 for follow-up work.
	// This tries to use the AppReg's spec as source of truth for
	// a number of attributes like display name and description.
	// However there are several issues:
	// 1. the way it's done, enforceSpec will reset other values users may have set in the portal like admin group!
	//   It should _merge_ what it wants to set with the other existing attributes retrieved from
	//   the app in the portal.
	// 2. enforceSpec incorrectly pushes to authz API some attributes that we explicitly do NOT want OKD to be the SoT,
	//   like ownerID. This makes it impossible to change the owner from the app portal.
	// 3. it's not clear what the SoT for description should be. There is no way for users to update
	//   description in the appReg, if we want OKD to be the SoT then we need a mechanism to propagate
	//   changes from the OKD project description to the appReg spec, which doesn't exist.
	// 4. the same logic is not currently applied by the lifecycle controller syncing changes from AuthZ API.
	// By commenting this out, we effectively use the Authz API as the SoT for everything.
	// Follow-up required to clarify the SoT for attributes where it's not clear, like description, display name etc.
	// and properly document them.
	/*
		if !appspecMatchesAuthz(app.Spec, apiApp) {
			authzUpdate = true
			if err := r.enforceSpec(app.Spec, app.Status.ID); err != nil {
				log.Error(err, "Couldn't enforce the Application's Spec on the AuthzAPI")
				r.ensureErrorMsg(log, &app.Status, err)
				update = true
				if err.Temporary() {
					return
				}
				err = nil
				return
			}
		}
	*/
	return
}

// syncAPIOIDCStatus retrieves the OIDC from the API and syncs the information in the Status.
// It checks the redirectURIs and updates the API if they're not equal.
func (r *ApplicationRegistrationReconciler) syncAPIOIDCStatus(log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration, desiredOidcUris []string) (update bool, authzUpdate bool, transientErr reconcileError) {
	apiReg, err := r.fetchOIDCReg(app.Status.ID, true)
	if err != nil {
		update = r.setProvisioningStatus(log, &app.Status, err)
		if err.IsRetryable() {
			update = false
			transientErr = err
		}
		return
	}
	update, transientErr = r.ensureK8sClientSecret(context.TODO(), app.Namespace, apiReg)
	if update {
		log.Info("OIDC client secret is updated")
	}
	update = updateAppStatusOIDC(&app.Status, apiReg) || update

	// Check redirectURIs and update on API if needed
	if err := func() reconcileError {
		if !webservicesv1alpha1.SameSet(desiredOidcUris, apiReg.RedirectURIs) {
			log.Info("Updating OIDC redirect URIs in Authzsvc")
			authzUpdate = true
			return r.enforceOIDCRedirectURIs(app)
		}
		return nil
	}(); err != nil {
		log.Error(err, "Couldn't enforce the redirectURIs on the AuthzAPI")
		r.setProvisioningStatus(log, &app.Status, err)
		update = true
		if err.IsRetryable() {
			return
		}
		err = nil
		return
	}
	return
}

// ensureK8sClientSecret creates the k8s secret holding the OIDC client secret and updates it if necessary
func (r *ApplicationRegistrationReconciler) ensureK8sClientSecret(ctx context.Context, namespace string, apiReg authzapireq.APIRegistration) (update bool, transientErr reconcileError) {
	k8sSecret := &v1.Secret{ObjectMeta: metav1.ObjectMeta{Name: k8sSecretOIDCClientSecret, Namespace: namespace}}

	op, err := controllerutil.CreateOrUpdate(ctx, r.Client, k8sSecret, func() error {
		if k8sSecret.Data == nil {
			k8sSecret.Data = make(map[string][]byte)
		}

		k8sSecret.Data["clientID"] = []byte(apiReg.ClientID)
		k8sSecret.Data["clientSecret"] = []byte(apiReg.ClientSecret)
		k8sSecret.Data["issuerURL"] = []byte(r.Authz.IssuerURL())

		// Most Oauth clients (httpd module, oauth2 proxy...) expect a cookie secret to encrypt cookies.
		// It is thus useful that we generate such a secret in order to provide a complete configuration
		// for oauth clients to use.
		// Generating a random secret would not be deterministic; re-using the OIDC application
		// secret as the cookie encryption secret sounds interesting
		// but is not of the appropriate length for the oauth2 proxy image.
		// So we will hash the clientSecret to generate
		// a 24 character string that can be used
		// for the OIDC cookie secret
		hasher := sha256.New()
		hasher.Write([]byte(apiReg.ClientSecret))
		sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
		k8sSecret.Data["suggestedCookieSecret"] = []byte(sha[:24])

		return nil
	})

	if err != nil {
		transientErr = newApplicationError(err, ErrClientK8s)
	}
	update = (op != controllerutil.OperationResultNone)
	return
}

// fetchOwnedApp tries to fetch any associated Application from the AuthzAPI and returns an empty Application if it doesn't find it
func (r *ApplicationRegistrationReconciler) fetchOwnedApp(appID string) (authzapireq.APIApplication, reconcileError) {
	if appID != "" {
		apiAppHTTP, err := r.Authz.Get(authzapireq.Application.Join(appID), nil)
		if err != nil {
			return authzapireq.APIApplication{}, newApplicationError(err, ErrClientAuthz)
		}
		respBody, err := ioutil.ReadAll(apiAppHTTP.Body)
		if err != nil {
			return authzapireq.APIApplication{}, newApplicationError(err, ErrAuthzAPIPermanent)
		}
		switch {
		// Unauthorized, we can treat this as if the application didn't exist
		case apiAppHTTP.StatusCode == 401:
			return authzapireq.APIApplication{}, newApplicationError(err, ErrAuthzAPIUnauthorized)
		// Not Found, this isnt considered an error, we just return an empty application
		case apiAppHTTP.StatusCode == 404:
			return authzapireq.APIApplication{}, nil
		case apiAppHTTP.StatusCode > 201:
			return authzapireq.APIApplication{}, newApplicationError(
				authzapireq.StatusCodeErr(apiAppHTTP, respBody, nil), ErrAuthzAPITemp)
		}
		apiApp, err := authzapireq.APIApplicationHTTP(respBody)
		if err != nil {
			return authzapireq.APIApplication{}, newApplicationError(err, ErrAuthzInvalidResponse)
		}
		// case where Application is in the new Deleted state introduced by authz API v5
		// cf. https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/1459
		// and https://auth.docs.cern.ch/resources/resource-states/
		if apiApp.State == "Deleted" {
			// behave like a 404. We only see this application because we fetched by ID
			// (fetching by ApplicationIdentifier or other filter would return a 404)
			return authzapireq.APIApplication{}, nil
		}
		apiApp, err = resolveOwnerAndAdministratorIDs(r.AuthzApiCache, apiApp)
		if err != nil {
			if errors.Is(err, authzapireq.ErrNotFound) {
				return authzapireq.APIApplication{}, newApplicationError(nil, ErrInvalidOwner)
			}
			return authzapireq.APIApplication{}, newApplicationError(err, ErrTemporary)
		}
		return apiApp, nil
	}
	return authzapireq.APIApplication{}, nil
}

// resolveOwnerAndAdministratorIDs is a helper function that populates the human-readable
// OwnerUPN and AdministratorsDisplayName fields.
func resolveOwnerAndAdministratorIDs(authzApiCache apicache.AuthzCache, app authzapireq.APIApplication) (authzapireq.APIApplication, error) {
	// Get Person Name by ID
	owner, err := authzApiCache.GetIdentity(app.OwnerID)
	if err != nil {
		return app, err
	}
	app.OwnerUPN = owner.Upn

	// Get Group Name by ID
	groupName := ""
	if app.AdministratorsID != nil && *app.AdministratorsID != "" {
		group, err := authzApiCache.GetGroup(*app.AdministratorsID)
		if err != nil {
			return app, err
		}
		groupName = group.GroupIdentifier
	}
	app.AdministratorsDisplayName = groupName

	return app, nil
}

// fetchOIDCReg tries to fetch any associated OIDC registration from the AuthzAPI and returns an empty Registration if it doesn't find it
func (r *ApplicationRegistrationReconciler) fetchOIDCReg(appID string, fetchSecret bool) (authzapireq.APIRegistration, reconcileError) {
	apiOIDCHTTP, err := r.Authz.Get(authzapireq.Registration.Join(appID), nil)
	if err != nil {
		return authzapireq.APIRegistration{}, newApplicationError(err, ErrClientAuthz)
	}
	respBody, err := ioutil.ReadAll(apiOIDCHTTP.Body)
	if err != nil {
		return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzInvalidResponse)
	}
	switch {
	case apiOIDCHTTP.StatusCode == 404:
		return authzapireq.APIRegistration{}, nil
	case apiOIDCHTTP.StatusCode > 201:
		return authzapireq.APIRegistration{}, newApplicationError(authzapireq.StatusCodeErr(apiOIDCHTTP, respBody, nil), ErrAuthzAPITemp)
	}
	apiReg, err := authzapireq.APIRegistrationHTTP(respBody)
	if err != nil {
		return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzInvalidResponse)
	}

	if fetchSecret && apiReg.RegistrationID != "" {
		apiSecretHTTP, err := r.Authz.Get(authzapireq.Registration.Join(apiReg.RegistrationID).Join("secret"), nil)
		switch {
		case err != nil:
			return authzapireq.APIRegistration{}, newApplicationError(err, ErrClientAuthz)
		case apiSecretHTTP.StatusCode > 201:
			return authzapireq.APIRegistration{}, newApplicationError(authzapireq.StatusCodeErr(apiSecretHTTP, respBody, nil), ErrAuthzAPITemp)
		}
		respBody, err := ioutil.ReadAll(apiSecretHTTP.Body)
		if err != nil {
			return authzapireq.APIRegistration{}, newApplicationError(err, ErrClientAuthz) // TODO: Confirm Type of Error
		}
		apiSecret, err := authzapireq.APIApplicationHTTP(respBody)
		if err != nil {
			return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzInvalidResponse)
		}
		apiReg.ClientSecret = apiSecret.ClientSecret
	}
	return apiReg, nil
}

// updateAppStatus updates Application-related status values and reports if anything was changed
// Note: this defines the mapping between Status and Authzsvc API
func updateAppStatus(app *webservicesv1alpha1.ApplicationRegistrationStatus, apiApp authzapireq.APIApplication, owner authzapireq.APIIdentity, log logr.Logger) bool {
	updated := false

	if app.ID != apiApp.ID {
		log.WithValues("app.Id", app.ID, "apiApp.ID", apiApp.ID).V(8).Info("app.ID does not match apiApp.ID")
		app.ID = apiApp.ID
		updated = true
	}
	if app.CurrentOwnerUsername != apiApp.OwnerUPN {
		log.WithValues("app.CurrentOwnerUsername", app.CurrentOwnerUsername, "apiApp.OwnerUPN", apiApp.OwnerUPN).V(8).Info("app.CurrentOwnerUsername does not match apiApp.OwnerUPN")
		app.CurrentOwnerUsername = apiApp.OwnerUPN
		updated = true
	}
	if app.CurrentAdminGroup != apiApp.AdministratorsDisplayName {
		log.WithValues("app.CurrentAdminGroup", app.CurrentAdminGroup, "apiApp.AdministratorsDisplayName", apiApp.AdministratorsDisplayName).V(8).Info("app.CurrentAdminGroup does not match apiApp.Admin")
		app.CurrentAdminGroup = apiApp.AdministratorsDisplayName
		updated = true
	}
	if !(app.CurrentEnabledStatus == !apiApp.Blocked) {
		log.WithValues("app.CurrentEnabledStatus", app.CurrentEnabledStatus, "!apiApp.Blocked", !apiApp.Blocked).V(8).Info("app.CurrentEnabledStatus does not match !apiApp.Blocked")
		app.CurrentEnabledStatus = !apiApp.Blocked
		updated = true
	}
	if app.CurrentDescription != apiApp.Description {
		log.WithValues("app.CurrentDescription", app.CurrentDescription, "apiApp.Description", apiApp.Description).V(8).Info("app.CurrentDescription does not match apiApp.Description")
		app.CurrentDescription = apiApp.Description
		updated = true
	}
	if string(app.CurrentResourceCategory) != apiApp.Category {
		log.WithValues("app.CurrentResourceCategory", string(app.CurrentResourceCategory), "apiApp.Category", apiApp.Category).V(8).Info("app.CurrentResourceCategory does not match apiApp.Category")
		app.CurrentResourceCategory = webservicesv1alpha1.ResourceCategoryType(apiApp.Category)
		updated = true
	}
	if app.CurrentDepartment != owner.CernDepartment {
		log.WithValues("app.CurrentDepartment", string(app.CurrentDepartment), "owner.CernDepartment", owner.CernDepartment).V(8).Info("app.CurrentDepartment does not match")
		app.CurrentDepartment = owner.CernDepartment
		updated = true
	}
	if app.CurrentGroup != owner.CernGroup {
		log.WithValues("app.CurrentGroup", string(app.CurrentGroup), "owner.CernGroup", owner.CernGroup).V(8).Info("app.CurrentGroup does not match")
		app.CurrentGroup = owner.CernGroup
		updated = true
	}
	return updated
}

// updateAppStatusOIDC updates OIDC Registration- related status values (those that can be modified) and reports if anything was changed
func updateAppStatusOIDC(appStatus *webservicesv1alpha1.ApplicationRegistrationStatus, apiReg authzapireq.APIRegistration) (update bool) {
	if !(appStatus.OIDCEnabled == apiReg.Enabled &&
		appStatus.ClientCredentialsSecret == k8sSecretOIDCClientSecret &&
		webservicesv1alpha1.SameSet(appStatus.RedirectURIs, apiReg.RedirectURIs)) {
		appStatus.RegistrationID = apiReg.RegistrationID
		appStatus.OIDCEnabled = apiReg.Enabled
		appStatus.ClientCredentialsSecret = k8sSecretOIDCClientSecret
		appStatus.RedirectURIs = apiReg.RedirectURIs
		return true
	}
	return false
}

// createAppInAuthz: creates the ApplicationRegistration at the Authzsvc API
// Return POST response (equivalent to GET Application/{appID})
func (r *ApplicationRegistrationReconciler) createAppInAuthz(appSpec webservicesv1alpha1.ApplicationRegistrationSpec) (authzapireq.APIApplication, reconcileError) {
	signalAdoptApp := func(appCreationErrMsg string) (authzapireq.APIApplication, reconcileError) {
		// If creation returned a 400 error, it may be because an app already exists with that name,
		// in which case we try to adopt it. But it may also have failed for other reasons.
		// Verify if it already exists
		existingAPIApp, err := r.Authz.GetApplicationByAppID(appSpec.ApplicationIdentifier)
		if err != nil {
			switch {
			case errors.Is(err, authzapireq.ErrNotFound):
				// NOTE: if POST returns 400, but the condition to adopt isn't met, does this mean the Application is invalid?
				// If yes, it should be a permanent error
				return authzapireq.APIApplication{}, newApplicationError(errors.New("Failed to create the Application due to invalid input,"+
					" and it does not look like this is due to a name conflict as we could not find an existing application with the same identifier."+
					" The error returned by the Authz API was: "+appCreationErrMsg), ErrTemporary)
			case errors.Is(err, authzapireq.ErrUnauthorized):
				return authzapireq.APIApplication{}, newApplicationError(nil, ErrApplicationConflict)
			}
			return authzapireq.APIApplication{}, newApplicationError(err, ErrTemporary)
		}
		if existingAPIApp.AppID != appSpec.ApplicationIdentifier {
			return authzapireq.APIApplication{}, newApplicationError(errors.New("application with conflicting identifiers already exists,"+
				" but when trying to adopt it, the applicationIdentifiers don't match"), ErrTemporary)
		}
		// Obtain names of Owner and Administrator group
		existingAPIApp, err = resolveOwnerAndAdministratorIDs(r.AuthzApiCache, existingAPIApp)
		switch {
		case errors.Is(err, authzapireq.ErrNotFound):
			return existingAPIApp, newApplicationError(nil, ErrInvalidOwner)
		case err != nil:
			return existingAPIApp, newApplicationError(err, ErrTemporary)
		}
		return existingAPIApp, newApplicationError(nil, ErrApplicationAlreadyExists)
	}
	appJSON, sortedErr := r._createJSONBasedOnSpec(appSpec)
	if sortedErr != nil {
		return authzapireq.APIApplication{}, sortedErr
	}
	apiAppHTTP, err := r.Authz.Post(authzapireq.Application, bytes.NewBuffer(appJSON))
	if err != nil {
		return authzapireq.APIApplication{}, newApplicationError(err, ErrClientAuthz)
	}
	respBody, err := ioutil.ReadAll(apiAppHTTP.Body)
	if err != nil {
		return authzapireq.APIApplication{}, newApplicationError(err, ErrAuthzInvalidResponse)
	}
	switch {
	// Adopt application: 400 might mean an Application with the given identifier already exists
	case apiAppHTTP.StatusCode == 400:
		return signalAdoptApp(string(respBody))
	case apiAppHTTP.StatusCode > 201:
		return authzapireq.APIApplication{}, newApplicationError(authzapireq.StatusCodeErr(apiAppHTTP, respBody, appJSON), ErrAuthzAPITemp)
	}
	apiApp, err := authzapireq.APIApplicationHTTP(respBody)
	if err != nil {
		return authzapireq.APIApplication{}, newApplicationError(err, ErrAuthzInvalidResponse)
	}
	// Obtain names of Owner and Administrator group
	apiApp, err = resolveOwnerAndAdministratorIDs(r.AuthzApiCache, apiApp)
	switch {
	case errors.Is(err, authzapireq.ErrNotFound):
		return apiApp, newApplicationError(nil, ErrInvalidOwner)
	case err != nil:
		return apiApp, newApplicationError(err, ErrTemporary)
	}
	return apiApp, nil
}

// createOIDCinAuthz: creates the Registration for the Application at the Authzsvc API
func (r *ApplicationRegistrationReconciler) createOIDCinAuthz(app webservicesv1alpha1.ApplicationRegistration) (authzapireq.APIRegistration, reconcileError) {
	jsonBody, err := r._createJSONRegistration(app)
	if err != nil {
		return authzapireq.APIRegistration{}, newApplicationError(err, ErrTemporary)
	}
	apiRegHTTP, err := r.Authz.Post(authzapireq.Registration.Join(app.Status.ID).Join(r.Authz.OidcProviderID()), bytes.NewBuffer(jsonBody))
	if err != nil {
		return authzapireq.APIRegistration{}, newApplicationError(err, ErrClientAuthz)
	}
	respBody, err := ioutil.ReadAll(apiRegHTTP.Body)
	if err != nil {
		return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzInvalidResponse)
	}
	switch {
	case apiRegHTTP.StatusCode == 400:
		return authzapireq.APIRegistration{}, newApplicationError(authzapireq.StatusCodeErr(apiRegHTTP, respBody, jsonBody), ErrAuthzAPIPermanent)
	case apiRegHTTP.StatusCode > 201:
		return authzapireq.APIRegistration{}, newApplicationError(authzapireq.StatusCodeErr(apiRegHTTP, respBody, jsonBody), ErrAuthzAPITemp)
	}
	apiReg, err := authzapireq.APIRegistrationHTTP(respBody)
	if err != nil {
		return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzInvalidResponse)
	}
	return apiReg, nil
}

// fetchOIDCfromAuthz tries to fetch any associated Registration from the AuthzAPI and returns an empty Registration if it doesn't find it
func (r *ApplicationRegistrationReconciler) fetchOIDCfromAuthz(regID string) (authzapireq.APIRegistration, reconcileError) {
	if regID != "" {
		apiAppHTTP, err := r.Authz.Get(authzapireq.Registration.Join(regID), nil)
		if err != nil {
			return authzapireq.APIRegistration{}, newApplicationError(err, ErrClientAuthz)
		}
		respBody, err := ioutil.ReadAll(apiAppHTTP.Body)
		if err != nil {
			return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzInvalidResponse)
		}
		switch {
		// Unauthorized, we can treat this as if the application didn't exist
		case apiAppHTTP.StatusCode == 401:
			return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzAPIUnauthorized)
		// Not Found, this isnt considered an error, we just return an empty application
		case apiAppHTTP.StatusCode == 404:
			return authzapireq.APIRegistration{}, nil
		case apiAppHTTP.StatusCode > 201:
			return authzapireq.APIRegistration{}, newApplicationError(
				authzapireq.StatusCodeErr(apiAppHTTP, respBody, nil), ErrAuthzAPITemp)
		}
		regID, err := authzapireq.APIRegistrationHTTP(respBody)
		if err != nil {
			return authzapireq.APIRegistration{}, newApplicationError(err, ErrAuthzInvalidResponse)
		}
		return regID, nil
	}
	return authzapireq.APIRegistration{}, nil
}

// enforceSpec patches (HTTP PUT) the existing ApplicationRegistration definition in the Authzsvc API with the info from the spec
func (r *ApplicationRegistrationReconciler) enforceSpec(appSpec webservicesv1alpha1.ApplicationRegistrationSpec, appID string) reconcileError {
	appJSON, sortedErr := r._createJSONBasedOnSpec(appSpec)
	if sortedErr != nil {
		return sortedErr
	}
	apiAppHTTP, err := r.Authz.Put(authzapireq.Application.Join(appID), bytes.NewBuffer(appJSON))
	if err != nil {
		return newApplicationError(err, ErrTemporary)
	}
	respBody, err := ioutil.ReadAll(apiAppHTTP.Body)
	if err != nil {
		return newApplicationError(err, ErrAuthzInvalidResponse)
	}
	switch {
	case apiAppHTTP.StatusCode == 404:
		return newApplicationError(authzapireq.StatusCodeErr(apiAppHTTP, respBody, appJSON), ErrAssociatedAppNotFound)
	case apiAppHTTP.StatusCode == 405:
		// TODO #39 check StatusCode (405 not allowed)
		return newApplicationError(authzapireq.StatusCodeErr(apiAppHTTP, respBody, appJSON), ErrAuthzAPIPermanent)
	case apiAppHTTP.StatusCode > 201:
		return newApplicationError(authzapireq.StatusCodeErr(apiAppHTTP, respBody, appJSON), ErrAuthzAPITemp)
	}
	return nil
}

func (r *ApplicationRegistrationReconciler) enforceOIDCRedirectURIs(app *webservicesv1alpha1.ApplicationRegistration) reconcileError {
	jsonBody, err := r._createJSONRegistration(*app)
	if err != nil {
		return newApplicationError(err, ErrTemporary) //TODO Better error type
	}
	apiRegHTTP, err := r.Authz.Put(authzapireq.Registration.Join(app.Status.RegistrationID), bytes.NewBuffer(jsonBody))
	if err != nil {
		return newApplicationError(err, ErrClientAuthz)
	}
	respBody, err := ioutil.ReadAll(apiRegHTTP.Body)
	if err != nil {
		return newApplicationError(err, ErrAuthzInvalidResponse)
	}
	switch {
	case apiRegHTTP.StatusCode == 400:
		return newApplicationError(authzapireq.StatusCodeErr(apiRegHTTP, respBody, jsonBody), ErrAuthzAPIPermanent)
	case apiRegHTTP.StatusCode > 201:
		return newApplicationError(authzapireq.StatusCodeErr(apiRegHTTP, respBody, jsonBody), ErrAuthzAPITemp)
	}
	_, err = authzapireq.APIRegistrationHTTP(respBody)
	if err != nil {
		return newApplicationError(err, ErrAuthzInvalidResponse)
	}
	return nil
}

// appspecMatchesAuthz checks if {authzApp == appSpec}. It does the inverse of _createJSONBasedOnSpec.
func appspecMatchesAuthz(appSpec webservicesv1alpha1.ApplicationRegistrationSpec, apiApp authzapireq.APIApplication) bool {
	return apiApp.AppID == appSpec.ApplicationIdentifier &&
		apiApp.DisplayName == appSpec.DisplayName &&
		apiApp.HomePage == appSpec.HomePage &&
		apiApp.Description == appSpec.Description
}

func (r *ApplicationRegistrationReconciler) fetchOIDCRedirectURIs(namespace string) ([]string, reconcileError) {
	oidcRedirectURIs := &webservicesv1alpha1.OidcReturnURIList{}
	if err := r.List(context.TODO(), oidcRedirectURIs, &client.ListOptions{Namespace: namespace}); err != nil {
		return nil, newApplicationError(err, ErrClientK8s)
	}
	redirectURIs := make([]string, len(oidcRedirectURIs.Items))
	for i, uri := range oidcRedirectURIs.Items {
		redirectURIs[i] = uri.Spec.RedirectURI
	}
	return redirectURIs, nil
}

func (r *ApplicationRegistrationReconciler) _createJSONBasedOnSpec(appSpec webservicesv1alpha1.ApplicationRegistrationSpec) ([]byte, reconcileError) {
	// resolve the owner UPN to an identity ID
	ownerID, err := r.AuthzApiCache.LookupUserId(appSpec.InitialOwner.Username)
	if err != nil {
		if errors.Is(err, authzapireq.ErrNotFound) {
			return nil, newApplicationError(nil, ErrInvalidOwner)
		}
		return nil, newApplicationError(err, ErrTemporary)
	}

	// if an initialAdminGroup is given, resolve it to a group ID
	var adminGroupID string
	if appSpec.InitialAdminGroup != "" {
		adminGroupID, err = r.AuthzApiCache.LookupGroupId(appSpec.InitialAdminGroup)
		if err != nil {
			if errors.Is(err, authzapireq.ErrNotFound) {
				return nil, newApplicationError(nil, ErrGroupDoesntExist)
			}
			return nil, newApplicationError(err, ErrTemporary)
		}
	}

	appJSON, err := json.Marshal(authzapireq.APIApplication{
		AppID:            appSpec.ApplicationIdentifier,
		DisplayName:      appSpec.DisplayName,
		HomePage:         appSpec.HomePage,
		OwnerID:          ownerID,
		Description:      appSpec.Description,
		AdministratorsID: &adminGroupID,
		ManagerID:        r.Authz.ManagerID(),
		Category:         string(appSpec.InitialResourceCategory),
	})
	if err != nil {
		return nil, newApplicationError(err, ErrTemporary)
	}
	return appJSON, nil
}

func (r *ApplicationRegistrationReconciler) _createJSONRegistration(app webservicesv1alpha1.ApplicationRegistration) ([]byte, error) {
	redirectURIs, err := r.fetchOIDCRedirectURIs(app.Namespace)
	if err != nil {
		return nil, err
	}
	jsonData := map[string]interface{}{
		"consentRequired":     false,
		"implicitFlowEnabled": false,
		"redirectUris":        redirectURIs,
	}
	return json.Marshal(jsonData)
}

func (r *ApplicationRegistrationReconciler) _createJSONAppRole(role webservicesv1alpha1.AppRole, appID string) ([]byte, error) {
	loaID, err := r.Authz.GetLoA(strconv.Itoa(role.MinLoA))
	if err != nil {
		return nil, err // Sort AuthzAPI errors?
	}
	jsonData := map[string]interface{}{
		"applicationId":   appID,
		"applyToAllUsers": role.ApplyToAllUsers,
		"description":     role.Description,
		"displayName":     role.DisplayName,
		"minimumLoaId":    loaID,
		"name":            role.Name,
		"required":        role.Required,
	}
	return json.Marshal(jsonData)
}

func validateSpec(appSpec webservicesv1alpha1.ApplicationRegistrationSpec) error {
	_, err := govalidator.ValidateStruct(appSpec)
	if err != nil {
		return err
	}
	_, err = govalidator.ValidateStruct(appSpec.InitialOwner)
	return err
}

// deleteApplicationFromAuthz deletes the application from the AuthzAPI and handles the error conditions
func (r *ApplicationRegistrationReconciler) deleteApplicationFromAuthz(ownedAppID string) reconcileError {
	httpResponse, err := r.Authz.Delete(authzapireq.Application.Join(ownedAppID), nil)
	if err != nil {
		return newApplicationError(err, ErrAuthzAPITemp)
	}
	respBody, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return newApplicationError(err, ErrAuthzInvalidResponse)
	}
	switch {
	case httpResponse.StatusCode > 201:
		return newApplicationError(authzapireq.StatusCodeErr(httpResponse, respBody, nil), ErrAuthzAPITemp)
	}
	return nil
}

// ensureSpecHasAppIdAndName initializes spec.applicationIdentifier and spec.displayName from convention,
// then returns if it needs to be updated.
func ensureSpecHasAppIdAndName(log logr.Logger, app *webservicesv1alpha1.ApplicationRegistration) (update bool) {
	if app.Spec.ApplicationIdentifier == "" || app.Spec.DisplayName == "" {
		log.Info("Initializing AppReg Spec")
		app.Spec.ApplicationIdentifier = app.ApplicationIdentifierConvention(os.Getenv("CLUSTER_NAME"))
		app.Spec.DisplayName = app.DisplayNameConvention(os.Getenv("CLUSTER_NAME"))
		update = true
	}
	return
}

// ensureStatusInit ensures that the status have been initialized, returns true if it is required an update
func ensureStatusInit(app *webservicesv1alpha1.ApplicationRegistration) (update bool) {
	if app.Status.ProvisioningStatus == "" {
		app.Status.ProvisioningStatus = webservicesv1alpha1.ProvisioningStatusCreating
		return true
	}
	return false
}

// THIS MUST BE THE ONLY PLACE SETTING PROVISIONINGSTATUS! (other than initialization)
// Sets the ProvisioningStatus and error messages (when applicable) based on error conditions:
// - if no error, then ProvisioningStatus is `Created` and we remove any error message
// - if retryable (temporary/transient) error, we don't change the status (TODO: possibly show the error in a dedicated Condition)
// - if error is ErrAssociatedAppNotFound then ProvisioningStatus is "DeletedFromAPI" (see reconcileError.ProvisioningError())
// - for other non-retryable (permanent) errors, set ProvisioningStatus to "ProvisioningError" and show errors messages in status
func (r *ApplicationRegistrationReconciler) setProvisioningStatus(log logr.Logger, appStatus *webservicesv1alpha1.ApplicationRegistrationStatus, statusErr reconcileError) (update bool) {
	if statusErr == nil {
		if appStatus.ProvisioningStatus != webservicesv1alpha1.ProvisioningStatusCreated {
			appStatus.ProvisioningStatus = webservicesv1alpha1.ProvisioningStatusCreated
			appStatus.ErrorReason = ""
			appStatus.ErrorMessage = ""
			return true
		}
		return false
	}
	if statusErr.IsRetryable() {
		// TODO add Condition.OutOfSync
		//appStatus.ProvisioningStatus = webservicesv1alpha1.ProvisioningStatusCreating
		// NOTE these are only for debugging, otherwise temporary errors shouldn't fill this in
		//appStatus.ErrorReason = statusErr.Unwrap().Error()
		//appStatus.ErrorMessage = statusErr.Error()
		//return true
		return false
	}
	log.Info("Updating ProvisioningStatus with permanent error: " + statusErr.Error())
	appStatus.ProvisioningStatus = statusErr.ProvisioningError()
	appStatus.ErrorReason = statusErr.Unwrap().Error()
	appStatus.ErrorMessage = statusErr.Error()
	return true
}

func namespacedName(app *webservicesv1alpha1.ApplicationRegistration) types.NamespacedName {
	return types.NamespacedName{
		Name:      app.ObjectMeta.Name,
		Namespace: app.ObjectMeta.Namespace,
	}
}
