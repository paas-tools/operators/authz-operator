package controller

import (
	"errors"
	"fmt"

	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
)

// ErrorConditions
var (
	// Generic temporary error
	ErrTemporary                = errors.New("TemporaryError")
	ErrApplicationAlreadyExists = errors.New("ApplicationAlreadyExists")
	ErrInvalidSpec              = errors.New("InvalidSpec")
	ErrClientK8s                = errors.New("k8sAPIClientError")
	ErrClientAuthz              = errors.New("AuthzAPIClientError")
	ErrAuthzAPITemp             = errors.New("AuthzAPIError")
	ErrAuthzAPIPermanent        = errors.New("AuthzAPIPermanentError")
	ErrAuthzInvalidResponse     = errors.New("AuthzAPIInvalidResponse")
	ErrAuthzAPIUnauthorized     = errors.New("AuthzAPIUnauthorized")
	ErrApplicationConflict      = errors.New("ApplicationConflict")
	ErrAssociatedAppNotFound    = errors.New("AssociatedApplicationNotFound")
	ErrInvalidOwner             = errors.New("InvalidOwner")
	ErrUnsupportedChangeInAuthz = errors.New("UnsupportedChangeInAuthz")
	ErrGroupDoesntExist         = errors.New("GroupDoesntExistError")
)

type reconcileError interface {
	error
	Wrap(msg string) reconcileError
	Unwrap() error
	IsRetryable() bool
	ProvisioningError() webservicesv1alpha1.ProvisioningStatusType
}

// applicationError wraps an error condition and gives it more context from where it occurred
type applicationError struct {
	innerException error
	errorCondition error
}

func newApplicationError(inner, condition error) reconcileError {
	return &applicationError{
		innerException: inner,
		errorCondition: condition,
	}
}

func (e *applicationError) Wrap(msg string) reconcileError {
	return newApplicationError(fmt.Errorf("%s: %w", msg, e.innerException), e.errorCondition)
}

func (e *applicationError) Unwrap() error { return e.errorCondition }
func (e *applicationError) Error() string {
	if e.innerException == nil {
		return e.errorCondition.Error()
	}
	return e.innerException.Error()
}

// IsRetryable returns if the error condition is temporary or permanent
func (e *applicationError) IsRetryable() bool {
	// NOTE: List all permanent errors
	switch e.errorCondition {
	case ErrApplicationAlreadyExists:
		return false
	case ErrInvalidSpec:
		return false
	case ErrAuthzAPIPermanent:
		return false
	case ErrAssociatedAppNotFound:
		return false
	case ErrApplicationConflict:
		return false
	case ErrInvalidOwner:
		return false
	case ErrUnsupportedChangeInAuthz:
		return false
	default:
		return true
	}
}

func (e *applicationError) ProvisioningError() webservicesv1alpha1.ProvisioningStatusType {
	switch e.errorCondition {
	case ErrAssociatedAppNotFound:
		return webservicesv1alpha1.ProvisioningStatusDeletedFromAPI
	default:
		return webservicesv1alpha1.ProvisioningStatusProvisioningError
	}
}
