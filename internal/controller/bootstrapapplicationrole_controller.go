/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"strconv"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/apicache"
	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapireq"
)

// BootstrapApplicationRoleReconciler reconciles a BootstrapApplicationRole object
type BootstrapApplicationRoleReconciler struct {
	client.Client
	Log           logr.Logger
	Scheme        *runtime.Scheme
	Authz         authzapireq.AuthzClient
	AuthzApiCache apicache.AuthzCache
}

// +kubebuilder:rbac:groups=webservices.cern.ch,resources=bootstrapapplicationroles,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=bootstrapapplicationroles/status,verbs=get;update;patch

func (r *BootstrapApplicationRoleReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("bootstrapapplicationrole", req.NamespacedName)

	// Fetch the BootstratpApplicationRole
	applicationRole := webservicesv1alpha1.BootstrapApplicationRole{}
	if err := r.Get(ctx, req.NamespacedName, &applicationRole); err != nil {
		if apierrors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	// If the role has been created already, nothing to do since we never touch an existing role
	if meta.IsStatusConditionTrue(applicationRole.Status.Conditions, webservicesv1alpha1.ConditionTypeRoleCreation) {
		log.V(6).Info("Application Role has been bootstrapped successfully, nothing else to do")
		return ctrl.Result{}, nil
	}

	applicationRegistrationList := &webservicesv1alpha1.ApplicationRegistrationList{}
	if err := r.List(ctx, applicationRegistrationList, &client.ListOptions{Namespace: req.NamespacedName.Namespace}); err != nil {
		log.Error(err, "Not able to retrieve ApplicationRegistration List")
		return ctrl.Result{}, err
	}
	// If no ApplicationRegistration present, do nothing
	if len(applicationRegistrationList.Items) == 0 {
		log.V(6).Info("No ApplicationRegistration found in namespace, retrying ...")
		return ctrl.Result{Requeue: true}, nil
	}
	// it only makes sense that we have exactly one ApplicationRegistration in the namespace.
	// Behavior is undefined is there are more than one.
	// Just take the first one.
	appreg := applicationRegistrationList.Items[0]

	// Check if ApplicationRegistration is ready (Status has an ID)
	if appreg.Status.ProvisioningStatus != webservicesv1alpha1.ProvisioningStatusCreated {
		log.V(6).Info("ApplicationRegistration has not been created, retrying ...")
		return ctrl.Result{Requeue: true}, nil
	}

	// Checking if AppRole exists on API
	roleExists, err := r.Authz.GetRole(appreg.Status.ID, applicationRole.Spec.Name)
	if err != nil {
		return ctrl.Result{}, err
	}
	// Check if Role exists, there will be nothing else to do, therfore no further reconciliation
	if roleExists {
		return r.updateBootstrapApplicationRoleStatus(log, ctx, applicationRole, webservicesv1alpha1.ConditionRoleAlreadyExists, metav1.ConditionTrue, webservicesv1alpha1.StatusMessageAlreadyExists, true)
	}
	// Start Status if empty
	if len(applicationRole.Status.Conditions) == 0 {
		return r.updateBootstrapApplicationRoleStatus(log, ctx, applicationRole, webservicesv1alpha1.ConditionRoleCreating, metav1.ConditionFalse, webservicesv1alpha1.StatusMessageWaitingNextReconciliation, true)
	}

	// Check if Groups exist
	groupIDs, missingGroups, err := r.getGroupsFromAPI(applicationRole.Spec.LinkedGroups)
	if err != nil {
		return r.updateBootstrapApplicationRoleStatusForGroup(log, ctx, applicationRole, webservicesv1alpha1.ConditionWaitingForLinkedGroups, missingGroups, metav1.ConditionFalse, webservicesv1alpha1.StatusMessageMissingGroups, true)
	}
	if len(missingGroups) > 0 {
		// Use specific method that is similar to updateBootstrapApplicationRoleStatus but allows to set `missingGroups` in the message
		return r.updateBootstrapApplicationRoleStatusForGroup(log, ctx, applicationRole, webservicesv1alpha1.ConditionWaitingForLinkedGroups, missingGroups, metav1.ConditionFalse, webservicesv1alpha1.StatusMessageMissingGroups, true)
	}
	// We create the ApplicationRole now that the requirements have been met, and Update the value on the Status
	newRole, err := r._createAuthzAppRole(applicationRole.Spec, appreg.Status.ID)
	if err != nil {
		return r.updateBootstrapApplicationRoleStatus(log, ctx, applicationRole, webservicesv1alpha1.ConditionRoleCreationError, metav1.ConditionFalse, webservicesv1alpha1.StatusMessageWaitingNextReconciliation, true)
	}
	applicationRole.Status.RoleID, err = r.Authz.CreateApplicationRole(newRole, appreg.Status.ID)
	if err != nil {
		return r.updateBootstrapApplicationRoleStatus(log, ctx, applicationRole, webservicesv1alpha1.ConditionRoleCreationError, metav1.ConditionFalse, webservicesv1alpha1.StatusMessageWaitingNextReconciliation, true)
	}
	failedGroups, err := r.linkGroupsToRole(appreg, applicationRole, groupIDs)
	if err != nil {
		// We don't requeue on failed Group linkage, ApplicationRole is considered bootstrapped
		return r.updateBootstrapApplicationRoleStatusForGroup(log, ctx, applicationRole, webservicesv1alpha1.ConditionGroupLinkError, failedGroups, metav1.ConditionTrue, webservicesv1alpha1.StatusMessageGroupLinkError, false)
	}
	return r.updateBootstrapApplicationRoleStatus(log, ctx, applicationRole, webservicesv1alpha1.ConditionRoleBootstrappedSuccessfully, metav1.ConditionTrue, webservicesv1alpha1.StatusMessageCreatedSuccesfully, false)
}

func (r *BootstrapApplicationRoleReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webservicesv1alpha1.BootstrapApplicationRole{}).
		Complete(r)
}

func (r *BootstrapApplicationRoleReconciler) updateBootstrapApplicationRoleStatusForGroup(log logr.Logger, ctx context.Context, appRole webservicesv1alpha1.BootstrapApplicationRole, condition string, missingGroups []string, status metav1.ConditionStatus, message string, requeue bool) (ctrl.Result, error) {
	for _, group := range missingGroups {
		message += group + "; "
	}
	meta.SetStatusCondition(&appRole.Status.Conditions, metav1.Condition{
		Type:    webservicesv1alpha1.ConditionTypeRoleCreation,
		Status:  status,
		Reason:  condition,
		Message: message,
	})
	if err := r.Status().Update(ctx, &appRole); err != nil {
		log.Error(err, "Failed to update ApplicationRole Status")
		return ctrl.Result{}, err
	}
	return ctrl.Result{Requeue: requeue}, nil
}
func (r *BootstrapApplicationRoleReconciler) updateBootstrapApplicationRoleStatus(log logr.Logger, ctx context.Context, appRole webservicesv1alpha1.BootstrapApplicationRole, condition string, status metav1.ConditionStatus, message string, requeue bool) (ctrl.Result, error) {
	meta.SetStatusCondition(&appRole.Status.Conditions, metav1.Condition{
		Type:    webservicesv1alpha1.ConditionTypeRoleCreation,
		Status:  status,
		Reason:  condition,
		Message: message,
	})
	if err := r.Status().Update(ctx, &appRole); err != nil {
		log.Error(err, "Failed to update ApplicationRole Status")
		return ctrl.Result{}, err
	}
	return ctrl.Result{Requeue: requeue}, nil
}

// getGroupsFromAPI validates if ALL Groups exist or not
func (r *BootstrapApplicationRoleReconciler) getGroupsFromAPI(linkedGroups []string) ([]string, []string, error) {
	var missingGroups []string
	var groupIDs []string
	for _, group := range linkedGroups {
		group, err := r.AuthzApiCache.GetGroup(group)
		if err == authzapireq.ErrNotFound {
			missingGroups = append(missingGroups, group.ID)
			continue
		}
		if err != nil {
			return nil, missingGroups, err
		}

		groupIDs = append(groupIDs, group.ID)
	}
	return groupIDs, missingGroups, nil
}

// linkGroupsToRole links all the groups in Spec.LinkedGroups with the ApplicationRole
func (r *BootstrapApplicationRoleReconciler) linkGroupsToRole(appReg webservicesv1alpha1.ApplicationRegistration, appRole webservicesv1alpha1.BootstrapApplicationRole, groupIDs []string) ([]string, error) {
	failedToLinkGroups := groupIDs
	appID := appReg.Status.ID
	for _, group := range groupIDs {
		err := r.Authz.LinkGroupToAppRole(group, appRole.Status.RoleID, appID)
		if err != nil {
			return failedToLinkGroups, err
		}
		failedToLinkGroups = failedToLinkGroups[1:]
	}
	return failedToLinkGroups, nil
}

func (r *BootstrapApplicationRoleReconciler) _createAuthzAppRole(role webservicesv1alpha1.BootstrapApplicationRoleSpec, appID string) (authzapireq.APIRole, error) {
	loaID, err := r.Authz.GetLoA(strconv.Itoa(role.MinLevelOfAssurance))
	if err != nil {
		return authzapireq.APIRole{}, err // Sort AuthzAPI errors?
	}
	newRole := authzapireq.APIRole{
		ApplicationID:   appID,
		ApplyToAllUsers: role.ApplyToAllUsers,
		Description:     role.Description,
		DisplayName:     role.DisplayName,
		MinimumLoaID:    loaID,
		Name:            role.Name,
		Required:        role.RoleRequired,
	}
	return newRole, nil
}
