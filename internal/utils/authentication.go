package utils

import (
	"context"
	"fmt"
	"net/url"
	"os"

	"gitlab.cern.ch/paas-tools/operators/authz-operator/internal/authzapiv1"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

const (
	// TokenPath contains sub-path to retrieve access token
	tokenPath = "/api-access/token"
	//Audience contains the audience on which the operator communicates with
	audience = "authorization-service-api"
)

// Create a new instance of authzapiv1.APIClient which uses OAuth2 client credentials for authentication
func CreateAuthzApiClient(authzApiUrl, clientID, clientSecret, issuerURL string) *authzapiv1.APIClient {
	// set up oauth2 client
	clientCredentialsConfig := clientcredentials.Config{
		AuthStyle:    oauth2.AuthStyleInParams, // for CERN Keycloak, client_id and client_secret must be send as POST parameters (not in HTTP headers)
		ClientID:     clientID,
		ClientSecret: clientSecret,
		TokenURL:     issuerURL + tokenPath,
		EndpointParams: url.Values{
			"audience": []string{audience},
		},
	}
	httpClient := clientCredentialsConfig.Client(context.Background())

	// set up AuthZ API client
	authzApiConfig := authzapiv1.NewConfiguration()
	authzApiConfig.Servers[0].URL = authzApiUrl
	authzApiConfig.HTTPClient = httpClient
	authzApiClient := authzapiv1.NewAPIClient(authzApiConfig)

	return authzApiClient
}

// CreateAuthzApiClientFromEnv is a convience wrapper around CreateAuthzApiClient which loads the necessary
// config parameters from the following environment variables:
// - AUTHZAPI_URL
// - KC_ISSUER_URL
// - KC_CLIENT_ID
// - KC_CLIENT_SECRET
func CreateAuthzApiClientFromEnv() (*authzapiv1.APIClient, error) {
	authzApiUrl := os.Getenv("AUTHZAPI_URL")
	if authzApiUrl == "" {
		return nil, fmt.Errorf("Environment variable AUTHZAPI_URL must be set")
	}
	issuerURL := os.Getenv("KC_ISSUER_URL")
	if issuerURL == "" {
		return nil, fmt.Errorf("Environment variable KC_ISSUER_URL must be set")
	}
	clientID := os.Getenv("KC_CLIENT_ID")
	if clientID == "" {
		return nil, fmt.Errorf("Environment variable KC_CLIENT_ID must be set")
	}
	clientSecret := os.Getenv("KC_CLIENT_SECRET")
	if clientSecret == "" {
		return nil, fmt.Errorf("Environment variable KC_CLIENT_SECRET must be set")
	}

	return CreateAuthzApiClient(authzApiUrl, clientID, clientSecret, issuerURL), nil
}
