package authzapireq

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"time"

	"golang.org/x/oauth2"
)

const (
	tokenExchangeGrantType = "urn:ietf:params:oauth:grant-type:token-exchange"
	// access_token | refresh_token
	requestedTokenType = "urn:ietf:params:oauth:token-type:access_token"
	// subjectTokenType seems useless
	//subjectTokenType       = "urn:ietf:params:oauth:token-type:access_token"
	appJSON = "application/json"
	//TokenURL contains sub-path to retrieve access token
	TokenURL = "api-access/token"
	//Audience contains the audience on which the operator communicates with
	Audience = "authorization-service-api"
)

// Exported API error conditions
var (
	// When HTTP 404 isn't enough
	ErrNotFound                  = errors.New("NotFound")
	ErrUnauthorized              = errors.New("Unauthorized")
	ErrExistsInAuthzNotInCluster = errors.New("Application exists in API but not in Cluster")
)

// +kubebuilder:rbac:groups=webservices.cern.ch,resources=applications,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=webservices.cern.ch,resources=applications/status,verbs=get;update;patch

// AccessTokenArgs is the only way we found to make arguments to accessToken both keywords and optional,
// at the expense of an extra type...
type AccessTokenArgs struct {
	Audience      string
	ExistingToken oauth2.Token
}

// AuthzAPI represents API endpoints in the Authzsvc API
type AuthzAPI string

const (
	apiV = "api/v1.0/"
	// Application [GET,POST]
	Application           AuthzAPI = apiV + "Application"
	MyApplication         AuthzAPI = apiV + "Application/my"
	Group                 AuthzAPI = apiV + "Group"
	RegistrationProviders AuthzAPI = apiV + "Registration/providers"
	Identity              AuthzAPI = apiV + "Identity"
	Registration          AuthzAPI = apiV + "Registration"
	LevelofAssurance      AuthzAPI = apiV + "LevelOfAssurance"
)

// StatusCodeErr documents an API error with the HTTP request and response
func StatusCodeErr(resp *http.Response, respBody []uint8, requestData []byte) error {
	apiMsg, _ := HTTPMsg(respBody)
	if requestData != nil {
		return fmt.Errorf("%s %s \ndata: %s\nResponse %d: %s", resp.Request.Method, resp.Request.URL.RequestURI(), requestData, resp.StatusCode, apiMsg)
	}
	return fmt.Errorf("%s %s\nResponse %d: %s", resp.Request.Method, resp.Request.URL.RequestURI(), resp.StatusCode, apiMsg)
}

func join(p url.URL, api AuthzAPI) string {
	p.Path = path.Join(p.Path, string(api))
	unescapedPath, err := url.PathUnescape(p.String())
	if err != nil {
		unescapedPath = ""
	}
	return unescapedPath
}

func urlJoin(receivedURL string, extra string) string {
	u, err := url.Parse(receivedURL)
	if err != nil {
		return ""
	}
	u.Path = path.Join(u.Path, extra)
	return u.String()
}

// Cat simply appends the given string
func (api AuthzAPI) Cat(param string) AuthzAPI {
	return AuthzAPI(string(api) + param)
}

// Join does a path.Join with the given string
func (api AuthzAPI) Join(p string) AuthzAPI {
	return AuthzAPI(path.Join(string(api), p))
}

// Limit adds Limit query to AuthzAPI endpoint
func (api AuthzAPI) Limit(l string) AuthzAPI {
	urlObj, err := url.Parse(string(api))
	if err != nil {
		return ""
	}
	qs := urlObj.Query()
	qs.Set("limit", l)
	urlObj.RawQuery = qs.Encode()
	return AuthzAPI(urlObj.String())
}

// AuthzAPI response types

// APIApplication contains all the relevant fields to parse JSON responses from the Authzsvc API that should be compared against and ApplicationRegistration
type APIApplication struct {
	AppID            string  `json:"applicationIdentifier"`
	DisplayName      string  `json:"displayName"`
	Description      string  `json:"description"`
	OwnerID          string  `json:"ownerId"`
	ManagerID        string  `json:"managerId"`
	IdentityID       string  `json:"identityId"`
	AdministratorsID *string `json:"administratorsId"`
	HomePage         string  `json:"homePage"`
	Category         string  `json:"resourceCategory"`
	Blocked          bool    `json:"blocked"`
	ID               string  `json:"id,omitempty"`
	CreateTime       string  `json:"creationTime,omitempty"`
	ModTime          string  `json:"modificationTime,omitempty"`
	// State introduced in authz-api v5.0
	State string `json:"state,omitempty"`
	// OIDC fields
	ClientID       string `json:"clientId,omitempty"`
	ClientSecret   string `json:"secret,omitempty"`
	RegistrationID string `json:"registrationId,omitempty"`
	// Internal fields
	// OwnerUPN is the Owner username, it is named OwnerUPN to match the naming on the Authzsvc API
	OwnerUPN string `json:"-"`
	// DisplayName of the Administrators group
	AdministratorsDisplayName string `json:"-"`
}

// APIRegistration parses a JSON response from the Authzsvc API containing an OIDC Data
type APIRegistration struct {
	RegistrationID string
	// Attributes we recognise
	ClientID             string   `json:"clientId"`
	ClientSecret         string   `json:"secret"`
	RedirectURIs         []string `json:"redirectUris"`
	ImplicitFlowEnabled  bool     `json:"implicitFlowEnabled"`
	ConsentRequired      bool     `json:"consentRequired"`
	Enabled              bool     `json:"enabled"`
	DefaultClientScopes  []string `json:"defaultClientScopes"`
	OptionalClientScopes []string `json:"optionalClientScopes"`
	// Additional attributes
	WebOrigins                []string `json:"webOrigins"`
	ClientAuthenticatorType   string   `json:"clientAuthenticatorType"`
	DirectAccessGrantsEnabled bool     `json:"directAccessGrantsEnabled"`
	PublicClient              bool     `json:"publicClient"`
	ServiceAccountsEnabled    bool     `json:"serviceAccountsEnabled"`
	StandardFlowEnabled       bool     `json:"standardFlowEnabled"`
	SurrogateAuthRequired     bool     `json:"surrogateAuthRequired"`
	FullScopeAllowed          bool     `json:"fullScopeAllowed"`
	FrontchannelLogout        bool     `json:"frontchannelLogout"`
}

type APIGroup struct {
	// UUID of the group
	ID string `json:"id"`
	// human-readable identifier of the group ("name")
	GroupIdentifier string `json:"groupIdentifier"`

	// the following fields are available but are currently not needed:

	// AdministratorsID       string      `json:"administratorsId"`
	// ApprovalRequired       bool        `json:"approvalRequired"`
	// AutoReassign           bool        `json:"autoReassign"`
	// Blocked                bool        `json:"blocked"`
	// CreationTime time.Time `json:"creationTime"`
	// Description            string      `json:"description"`
	// DisplayName            string      `json:"displayName"`
	// Dynamic                bool        `json:"dynamic"`
	// DynamicGroupType       string      `json:"dynamicGroupType"`
	// IsComputingGroup       bool        `json:"isComputingGroup"`
	// MemberIdentityIds          []string      `json:"memberIdentityIds"`
	// OwnerID                string      `json:"ownerId"`
	// PendingAction          bool        `json:"pendingAction"`
	// PrivacyType            string      `json:"privacyType"`
	// Public                 bool        `json:"public"`
	// Reassignable           bool        `json:"reassignable"`
	// RemoveNonActiveMembers bool        `json:"removeNonActiveMembers"`
	// ResourceCategory       string      `json:"resourceCategory"`
	// SecurityIssues         bool        `json:"securityIssues"`
	// SelfSubscriptionType   string      `json:"selfSubscriptionType"`
	// Source                 string      `json:"source"`
	// SyncType               string      `json:"syncType"`
}

type APIRole struct {
	Name            string `json:"name"`
	DisplayName     string `json:"displayName"`
	Description     string `json:"description"`
	ApplicationID   string `json:"applicationId"`
	ApplyToAllUsers bool   `json:"applyToAllUsers"`
	MinimumLoaID    string `json:"minimumLoaId"`
	Required        bool   `json:"required"`
	RoleId          string `json:"id"`
}

type APIPage struct {
	Total  int         `json:"total"`
	Offset int         `json:"offset"`
	Limit  int         `json:"limit"`
	Next   *string     `json:"next"`
	Links  APIPageLink `json:"links"`
}
type APIPageLink struct {
	Current string `json:"current"`
	Next    string `json:"next"`
	Last    string `json:"last"`
}

type APIIdentity struct {
	// ExternalEmail              interface{} `json:"externalEmail"`
	PrimaryAccountEmail string `json:"primaryAccountEmail"`
	Type                string `json:"type"`
	Upn                 string `json:"upn"`
	DisplayName         string `json:"displayName"`
	PersonID            string `json:"personId"`
	SupervisorID        string `json:"supervisorId"`
	DirectResponsibleID string `json:"directResponsibleId"`
	// Source                     string      `json:"source"`
	// Unconfirmed                bool        `json:"unconfirmed"`
	// UnconfirmedEmail           interface{} `json:"unconfirmedEmail"`
	PrimaryAccountID string `json:"primaryAccountId"`
	UID              int    `json:"uid"`
	Gid              int    `json:"gid"`
	ResourceCategory string `json:"resourceCategory"`
	Reassignable     bool   `json:"reassignable"`
	// AutoReassign               bool        `json:"autoReassign"`
	// PendingAction              bool        `json:"pendingAction"`
	// Blocked                    bool        `json:"blocked"`
	// SecurityIssues             bool        `json:"securityIssues"`
	// BlockingReason             string      `json:"blockingReason"`
	// BlockingTime               interface{} `json:"blockingTime"`
	// BlockingDeadline           interface{} `json:"blockingDeadline"`
	// ExpirationDeadline         interface{} `json:"expirationDeadline"`
	ID           string    `json:"id"`
	CreationTime time.Time `json:"creationTime"`
	// Room                       string      `json:"room"`
	// Floor                      string      `json:"floor"`
	// Building                   string      `json:"building"`
	// EndClass                   time.Time   `json:"endClass"`
	// LastName                   string      `json:"lastName"`
	// BirthDate                  time.Time   `json:"birthDate"`
	CernClass string `json:"cernClass"`
	CernGroup string `json:"cernGroup"`
	// FirstName                  string      `json:"firstName"`
	// ActiveUser                 bool        `json:"activeUser"`
	// StartClass                 time.Time   `json:"startClass"`
	CernSection    string `json:"cernSection"`
	Description    string `json:"description"`
	CernPersonID   string `json:"cernPersonId"`
	InstituteName  string `json:"instituteName"`
	CernDepartment string `json:"cernDepartment"`
	// EdhAuthPwdExpiry           time.Time   `json:"edhAuthPwdExpiry"`
	// EduPersonUniqueID          string      `json:"eduPersonUniqueID"`
	// InstituteAbbreviation      string      `json:"instituteAbbreviation"`
	// PreferredCernLanguage      string      `json:"preferredCernLanguage"`
	// ComputingRulesAccepted     time.Time   `json:"computingRulesAccepted"`
	// ComputingRulesValidUntil   time.Time   `json:"computingRulesValidUntil"`
	// ComputingRulesAcceptedFlag bool        `json:"computingRulesAcceptedFlag"`
}

// APIApplicationHTTP creates an APIApplication from the JSON contained in an API GET/POST Application request.
// If there is no parseable Application inside, returns an empty object and no error.
func APIApplicationHTTP(respBody []uint8) (APIApplication, error) {
	// The response might be wrapped in a list.
	// Initially we assume it's not, but if we discover there's a list, we'll use this func instead
	unmarshalList := func() (APIApplication, error) {
		appList, err := APIApplicationListHTTP(respBody)
		switch {
		case err != nil:
			return APIApplication{}, err
		case len(appList) == 0:
			return APIApplication{}, nil
		case len(appList) > 1:
			return APIApplication{}, errors.New("Multiple Applications in HTTP response")
		}
		return appList[0], nil
	}
	apiApplication := struct {
		Msg            string         `json:"message"`
		APIApplication APIApplication `json:"data"`
	}{}
	err := json.Unmarshal(respBody, &apiApplication) // Unmarshal single
	if err != nil {
		// Check if we tried to unmarshal a list
		if unmarshalErr, ok := err.(*json.UnmarshalTypeError); ok {
			if unmarshalErr.Field == "data" && unmarshalErr.Value == "array" {
				return unmarshalList()
			}
		}
		return APIApplication{}, err
	} else if apiApplication.Msg != "" {
		return APIApplication{}, errors.New(apiApplication.Msg)
	}
	return apiApplication.APIApplication, nil
}

// APIApplicationHTTP creates an APIApplication List from the JSON contained in an API GET/POST Application request.
// If there is no parseable Application inside, returns an empty object and no error.
func APIApplicationListHTTP(respBody []uint8) ([]APIApplication, error) {
	// The response might be wrapped in a list.
	// Initially we assume it's not, but if we discover there's a list, we'll use this func instead
	apiApplicationList := struct {
		Msg            string           `json:"message"`
		APIApplication []APIApplication `json:"data"`
	}{}
	err := json.Unmarshal(respBody, &apiApplicationList)
	switch {
	case err != nil:
		return []APIApplication{}, err
	case apiApplicationList.Msg != "":
		return []APIApplication{}, errors.New(apiApplicationList.Msg)
	case len(apiApplicationList.APIApplication) == 0:
		return []APIApplication{}, nil
	}
	return apiApplicationList.APIApplication, nil

}

// APIRegistrationHTTP creates an APIRegistration from the JSON contained in an API GET/POST Registration request
func APIRegistrationHTTP(respBody []uint8) (APIRegistration, error) {
	type APIRegistrationResp struct {
		RegistrationID   string          `json:"registrationId"`
		RegistrationData APIRegistration `json:"registration"`
	}

	// The response might be wrapped in a list.
	// Initially we assume it's not, but if we discover there's a list, we'll use this func instead
	unmarshalList := func() (APIRegistration, error) {
		respJSON := struct {
			Msg  string                `json:"message"`
			Data []APIRegistrationResp `json:"data"`
		}{}
		err := json.Unmarshal(respBody, &respJSON)
		switch {
		case err != nil:
			return APIRegistration{}, err
		case respJSON.Msg != "":
			return APIRegistration{}, errors.New(respJSON.Msg)
		case len(respJSON.Data) == 0:
			return APIRegistration{}, nil
		case len(respJSON.Data) > 1:
			return APIRegistration{}, errors.New("Multiple Registrations in HTTP response")
		}
		respJSON.Data[0].RegistrationData.RegistrationID = respJSON.Data[0].RegistrationID
		return respJSON.Data[0].RegistrationData, nil
	}
	respJSON := struct {
		Msg  string              `json:"message"`
		Data APIRegistrationResp `json:"data"`
	}{}
	err := json.Unmarshal(respBody, &respJSON) // Unmarshal single
	if err != nil {
		// Check if we tried to unmarshal a list
		if unmarshalErr, ok := err.(*json.UnmarshalTypeError); ok {
			if unmarshalErr.Field == "data" && unmarshalErr.Value == "array" {
				return unmarshalList()
			}
		}
		return APIRegistration{}, err
	} else if respJSON.Msg != "" {
		return APIRegistration{}, errors.New(respJSON.Msg)
	}
	respJSON.Data.RegistrationData.RegistrationID = respJSON.Data.RegistrationID
	return respJSON.Data.RegistrationData, nil
}

// HTTPMsg returns the `message` field of an HTTP response
func HTTPMsg(respBody []uint8) (string, error) {
	var msg struct {
		Msg string `json:"message"`
	}
	err := json.Unmarshal(respBody, &msg)
	if err != nil {
		return "", err
	}
	return msg.Msg, nil
}
