package authzapireq

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"github.com/go-logr/logr"
)

// AuthzClient provides methods to access the AuthzAPI
type AuthzClient interface {
	// Post authorized req to Authzsvc API
	Post(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error)
	// Get authorized req from Authzsvc API
	Get(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error)
	// Put authorized req from Authzsvc API
	Put(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error)
	// Delete authorized req from Authzsvc API
	Delete(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error)
	// GetMyApplications will retrieve all the applications in the API (Note: this call is expensive as it has to go through all the pages)
	GetMyApplications() ([]APIApplication, error)
	// GetRole validates if Role already exists on an ApplicationRegistration in the API
	GetRole(id string, role string) (bool, error)
	// GetApplicationByAppID queries the API for the Application with the given applicationIdentifier
	GetApplicationByAppID(appID string) (APIApplication, error)
	// GetLoA queries the Authzsvc API for the ID of the Level of Assurance with the given level
	// this method is also available through the AuthZ apicache
	GetLoA(level string) (string, error)
	// CreateApplicationRole creates a new Role for a Specific Application
	CreateApplicationRole(role APIRole, applicationID string) (string, error)
	// LinkGroupToAppRole links an existing Group to an existing Role in an existing Application
	LinkGroupToAppRole(groupID string, roleID string, appID string) error
	// GetIdentity returns information about a user account - this method is also available through the AuthZ apicache
	GetIdentity(id string) (APIIdentity, error)
	// GetGroup returns information about a group - this method is also available through the AuthZ apicache
	GetGroup(id string) (APIGroup, error)
	// ManagerID returns the API ID of this AuthzAPI client's Application registration
	ManagerID() string
	// OidcProviderID returns the OIDC Provider ID on the Authzsvc API
	OidcProviderID() string
	IssuerURL() string
	RefreshClientSecret(regID string) error
}

// NewAuthzClient creates a client for the Authzsvc API configured with environment variables
func NewAuthzClient(log logr.Logger) (AuthzClient, error) {
	issURL, err := url.Parse(os.Getenv("KC_ISSUER_URL"))
	if err != nil {
		return &AuthzClientHTTP{}, err
	}
	authURL, err := url.Parse(os.Getenv("AUTHZAPI_URL"))
	if err != nil {
		return &AuthzClientHTTP{}, err
	}
	cid := os.Getenv("KC_CLIENT_ID")
	csecret := os.Getenv("KC_CLIENT_SECRET")
	err = checkEmpty(issURL.String(), "KC_ISSUER_URL", checkEmpty(authURL.String(), "AUTHZAPI_URL",
		checkEmpty(cid, "KC_CLIENT_ID", checkEmpty(csecret, "KC_CLIENT_SECRET", nil))))
	return &AuthzClientHTTP{
		Logger:              log,
		clientID:            cid,
		clientSecret:        csecret,
		issuerURL:           *issURL,
		authzURL:            *authURL,
		applicationsPerPage: os.Getenv("AUTHZ_APPLICATIONS_PER_PAGE"),
	}, err
}

func checkEmpty(s, name string, chainedErr error) (err error) {
	err = chainedErr
	if s == "" {
		err = fmt.Errorf("%s empty or undefined\n%v", name, chainedErr)
	}
	return
}
