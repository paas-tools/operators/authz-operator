package authzapireq

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/go-logr/logr"
)

// AuthzClientHTTP is an http client authorized to talk with the authzsvc API
type AuthzClientHTTP struct {
	http.Client
	logr.Logger
	tokenCache
	clientID     string
	clientSecret string
	issuerURL    url.URL
	authzURL     url.URL
	// managerID is the Application ID of this client in the Authzsvc API
	managerID string
	// oidcProviderID is the requested ID for Creating OIDC credentials in application
	oidcProviderID string
	// applicationsPerPage defines how many Applications are retrieved per page when fetching multple applications through pagination
	applicationsPerPage string
}

func (c *AuthzClientHTTP) IssuerURL() string { return c.issuerURL.String() }

// ManagerID returns the API ID of this AuthzAPI client's Application registration
func (c *AuthzClientHTTP) ManagerID() string {
	if c.managerID == "" {
		managerIDApp, err := c.getApp(Application.Cat("?filter=applicationIdentifier:" + c.clientID))
		if err != nil || managerIDApp.IdentityID == "" {
			c.Logger.Error(err, "Failed to get manager ID")
			return ""
		}
		c.managerID = managerIDApp.IdentityID
	}
	return c.managerID
}

// OidcProviderID returns the API ID of this AuthzAPI client's Application registration
func (c *AuthzClientHTTP) OidcProviderID() string {
	if c.oidcProviderID == "" {
		oidcProviderID, err := c.GetRegistrationProvider("openid_connect")
		if err != nil || oidcProviderID == "" {
			c.Logger.Error(err, "Failed to get OIDC Provider ID")
			return ""
		}
		c.oidcProviderID = oidcProviderID
	}
	return c.oidcProviderID
}

// Post authorized req to Authzsvc API
// Errors: what happens if the accessToken is empty? It's the caller's problem.
func (c *AuthzClientHTTP) Post(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error) {
	req, err := http.NewRequest("POST", join(c.authzURL, cmd), body)
	if err != nil {
		return nil, err
	}
	c.addHeaders(req)
	c.Logger.V(6).Info("POSTing to Authz API", "url", req.URL)
	return c.Do(req)
}

// Get authorized req from Authzsvc API
func (c *AuthzClientHTTP) Get(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error) {
	req, err := http.NewRequest("GET", join(c.authzURL, cmd), body)
	if err != nil {
		return nil, err
	}
	c.addHeaders(req)
	c.Logger.V(6).Info("GETting from Authz API", "url", req.URL)
	return c.Do(req)
}

// Put authorized req from Authzsvc API
func (c *AuthzClientHTTP) Put(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error) {
	req, err := http.NewRequest("PUT", join(c.authzURL, cmd), body)
	if err != nil {
		return nil, err
	}
	c.addHeaders(req)
	c.Logger.V(6).Info("PUTting to Authz API", "url", req.URL)
	return c.Do(req)
}

// Delete authorized req from Authzsvc API
func (c *AuthzClientHTTP) Delete(cmd AuthzAPI, body io.Reader) (resp *http.Response, err error) {
	req, err := http.NewRequest("DELETE", join(c.authzURL, cmd), body)
	if err != nil {
		return nil, err
	}
	c.addHeaders(req)
	c.Logger.V(6).Info("DELETEing from Authz API", "url", req.URL)
	return c.Do(req)
}

// GetIdentity returns information about a user account
func (c *AuthzClientHTTP) GetIdentity(id string) (APIIdentity, error) {
	var user APIIdentity
	if id == "" {
		return user, fmt.Errorf("GetIdentity: no identifier specified")
	}
	httpResponse, err := c.Get(Identity.Join("/"+id), nil)
	if err != nil {
		return user, err
	}
	respBody, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return user, err
	}

	switch {
	case httpResponse.StatusCode == 401:
		return user, ErrUnauthorized
	case httpResponse.StatusCode == 404:
		return user, ErrNotFound
	case httpResponse.StatusCode > 201:
		return user, StatusCodeErr(httpResponse, respBody, nil)
	}

	resp := struct {
		Message string      `json:"message"`
		Data    APIIdentity `json:"data"`
	}{}
	err = json.Unmarshal(respBody, &resp)

	return resp.Data, nil
}

// GetGroup returns information about a group
func (c *AuthzClientHTTP) GetGroup(id string) (APIGroup, error) {
	var group APIGroup
	if id == "" {
		return group, fmt.Errorf("GetGroup: no identifier specified")
	}
	httpResponse, err := c.Get(Group.Join("/"+id), nil)
	if err != nil {
		return group, err
	}
	respBody, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return group, err
	}

	switch {
	case httpResponse.StatusCode == 401:
		return group, ErrUnauthorized
	case httpResponse.StatusCode == 404:
		return group, ErrNotFound
	case httpResponse.StatusCode > 201:
		return group, StatusCodeErr(httpResponse, respBody, nil)
	}

	resp := struct {
		Message string   `json:"message"`
		Data    APIGroup `json:"data"`
	}{}
	err = json.Unmarshal(respBody, &resp)

	return resp.Data, nil
}

// GetApplicationByAppID queries the API for the Application with the given applicationIdentifier
func (c *AuthzClientHTTP) GetApplicationByAppID(appID string) (APIApplication, error) {
	apiApp, err := c.getApp(Application.Cat("?filter=applicationIdentifier:" + appID))
	if err != nil {
		return APIApplication{}, err
	}
	return apiApp, err
}

// GetRole validates if Role already exists on an ApplicationRegistration in the API
func (c *AuthzClientHTTP) GetRole(id string, role string) (bool, error) {
	apiHTTP, err := c.Get(Application.Join(id).Join("roles"), nil)
	if err != nil {
		return false, err
	}

	respBody, err := ioutil.ReadAll(apiHTTP.Body)
	if err != nil {
		return false, err
	}

	message := struct {
		Msg  string    `json:"message"`
		Data []APIRole `json:"data"`
	}{}
	err = json.Unmarshal(respBody, &message)
	if err != nil {
		return false, err
	}
	// Check if there's an ApplicationRole DisplayName matching the one requested to be created
	for _, reg := range message.Data {
		if reg.Name == role {
			// We could add the ID in the Status but there is no reason to since we won't be taking any actions at this point
			return true, nil
		}
	}
	return false, nil
}

// GetMyApplications will retrieve all the applications in the API (Note: this call is expensive as it has to go through all the pages)
func (c *AuthzClientHTTP) GetMyApplications() ([]APIApplication, error) {
	var appList []APIApplication
	nextPage := MyApplication
	// Allow custom number of Applications per Page if env set
	if c.applicationsPerPage != "" {
		nextPage = MyApplication.Limit(c.applicationsPerPage)
		if nextPage == "" {
			return nil, fmt.Errorf("Failed to add query to page when retrievign 'AUTHZ_APPLICATIONS_PER_PAGE' variable.")
		}
	}
	for nextPage != "" {
		// Default Applications per page is 1000.
		resp, err := c.Get(AuthzAPI(nextPage), nil)
		if err != nil {
			return nil, err
		}
		// We have "consumed" the page, and therefore we set it to nil so we don't go through it again
		nextPage = ""
		switch {
		case resp.StatusCode == 401:
			return nil, ErrUnauthorized
		case resp.StatusCode == 404:
			return nil, ErrNotFound
		case resp.StatusCode >= 500:
			return nil, fmt.Errorf("Internal Server Error on API while retrieving AppList: %v", StatusCodeErr(resp, nil, nil))
		case resp.StatusCode >= 400:
			return nil, fmt.Errorf("Client error while retrieving AppList: %v", StatusCodeErr(resp, nil, nil))
		}
		apps, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		// make sure to instantiate a fresh struct for unmarshalling each page
		apiAppList := struct {
			Msg             string           `json:"message"`
			PaginationLinks APIPage          `json:"pagination"`
			APIApp          []APIApplication `json:"data"`
		}{}
		err = json.Unmarshal(apps, &apiAppList)
		switch {
		case err != nil:
			return nil, err
		case apiAppList.Msg != "":
			return nil, errors.New(apiAppList.Msg)
		case apiAppList.PaginationLinks.Next != nil:
			nextPage = AuthzAPI(*apiAppList.PaginationLinks.Next)
		}
		appList = append(appList, apiAppList.APIApp...)
	}
	return appList, nil
}

// GetRegistrationProvider queries the API for the Registration provider with the given identifier
func (c *AuthzClientHTTP) GetRegistrationProvider(providerID string) (string, error) {
	return c.getID(RegistrationProviders.Cat("?filter=authenticationProviderIdentifier:" + providerID + "&field=id"))
}

// GetLoA queries the API for the Level of Assurance with the given level
func (c *AuthzClientHTTP) GetLoA(level string) (string, error) {
	return c.getID(LevelofAssurance.Cat("?filter=value:" + level + "&field=id"))
}

// CreateApplicationRole creates a new Role for a Specific Application
func (c *AuthzClientHTTP) CreateApplicationRole(role APIRole, applicationID string) (string, error) {
	type jsonID struct {
		Id          string `json:"id"`
		DisplayName string `json:"displayName"`
	}
	appRole := struct {
		Roles jsonID `json:"data"`
	}{}
	jsonBody, err := json.Marshal(role)
	if err != nil {
		return "", err
	}
	apiHTTP, err := c.Post(Application.Join(applicationID).Join("roles"), bytes.NewBuffer(jsonBody))
	if err != nil {
		return "", err
	}
	respBody, err := ioutil.ReadAll(apiHTTP.Body)
	if err != nil {
		return "", err
	}
	switch {
	case apiHTTP.StatusCode == 401:
		return "", ErrUnauthorized
	case apiHTTP.StatusCode > 201:
		return "", StatusCodeErr(apiHTTP, respBody, nil)
	}
	err = json.Unmarshal(respBody, &appRole)
	if err != nil {
		return "", err
	}
	return appRole.Roles.Id, nil
}

// LinkGroupToAppRole links an existing Group to an existing Role in an existing Application
func (c *AuthzClientHTTP) LinkGroupToAppRole(groupID string, roleID string, appID string) error {
	apiHTTP, err := c.Post(Application.Join(appID).Join("roles").Join(roleID).Join("groups").Join(groupID), nil)
	if err != nil {
		return err
	}
	respBody, err := ioutil.ReadAll(apiHTTP.Body)
	if err != nil {
		return err
	}
	switch {
	case apiHTTP.StatusCode == 401:
		return ErrUnauthorized
	case apiHTTP.StatusCode > 201:
		return StatusCodeErr(apiHTTP, respBody, nil)
	}
	return nil
}

func (c *AuthzClientHTTP) getApp(req AuthzAPI) (APIApplication, error) {
	resp, err := c.Get(req, nil)
	if err != nil {
		return APIApplication{}, err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return APIApplication{}, err
	}
	apiApp, err := APIApplicationHTTP(respBody)
	switch {
	case resp.StatusCode == 401:
		return APIApplication{}, ErrUnauthorized
	case resp.StatusCode > 201:
		return APIApplication{}, StatusCodeErr(resp, respBody, nil)
	case err != nil:
		return APIApplication{}, err
	case apiApp.ID == "":
		return APIApplication{}, ErrNotFound
	}
	return apiApp, err
}

func (c *AuthzClientHTTP) getID(req AuthzAPI) (string, error) {
	app, err := c.getApp(req)
	return app.ID, err
}

func (c *AuthzClientHTTP) addHeaders(req *http.Request) {
	req.Header.Set("Content-Type", appJSON)
	req.Header.Set("accept", "*/*")
	req.Header.Set("Authorization", "Bearer "+c.accessToken())
}

// accessToken needs to handle the errors and come back with a string
func (c *AuthzClientHTTP) accessToken() string {
	// initialize cache if empty
	if c.token() == "" {
		err := c.tokenCache.init(c)
		if err != nil {
			c.Logger.Error(err, "Failed to get Access Token")
			return ""
		}
	}
	// check if cached token has expired and refresh
	err := c.tokenCache.refresh(c)
	if err != nil {
		c.Logger.Error(err, "Failed to get Access Token")
		return ""
	}

	return c.token()
}

func (c *AuthzClientHTTP) token() string {
	return c.tokenCache.Token.AccessToken
}

func (c *AuthzClientHTTP) RefreshClientSecret(regID string) error {
	resp, err := c.Post(Registration.Join(regID).Join(c.OidcProviderID()).Join("invoke/client-secret"), nil)
	if err != nil {
		return err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	switch {
	case err != nil:
		return err
	case resp.StatusCode > 201:
		return StatusCodeErr(resp, respBody, nil)
	}
	return nil
}
