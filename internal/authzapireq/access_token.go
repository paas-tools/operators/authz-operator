package authzapireq

import (
	"encoding/json"
	"net/url"
	"os"
	"strings"

	"github.com/coreos/go-oidc"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

// AccessToken returns a signed token claiming the client's identity and potentially a specific audience,
// with a token exchange in that case. If a valid token exists, it reuses it.
//
// TODO(fborgesa): we shouldn't return the base64 encoded string, but a parsed JSON. Check with
// [JWT](https://pkg.go.dev/gopkg.in/square/go-jose.v2/jwt) package
func getAccessToken(audience string, clientID, clientSecret string, provider *oidc.Provider, verifier *oidc.IDTokenVerifier) (oauth2.Token, error) {
	// Initialize
	var err error
	getToken := func(c clientcredentials.Config) (oauth2.Token, error) {
		client := c.Client(oauth2.NoContext)
		resp, err := client.PostForm(c.TokenURL, valuesFromConfig(c))
		if err != nil || resp.StatusCode > 201 {
			return oauth2.Token{}, err
		}
		defer resp.Body.Close()
		var token oauth2.Token
		json.NewDecoder(resp.Body).Decode(&token)
		_, err = verifier.Verify(oauth2.NoContext, token.AccessToken)
		if err != nil {
			return oauth2.Token{}, err
		}
		return token, nil
	}

	var audiencedToken oauth2.Token
	endpointParams := url.Values{"grant_type": {"client_credentials"}}
	if audience != "" {
		endpointParams["audience"] = []string{audience}
	}
	audiencedToken, err = getToken(clientcredentials.Config{
		ClientID:       clientID,
		ClientSecret:   clientSecret,
		TokenURL:       urlJoin(os.Getenv("KC_ISSUER_URL"), TokenURL),
		EndpointParams: endpointParams,
	})
	if err != nil {
		return oauth2.Token{}, err
	}
	return audiencedToken, nil
}

func valuesFromConfig(c clientcredentials.Config) url.Values {
	v := url.Values{}
	if c.EndpointParams != nil {
		v = c.EndpointParams
	}
	if c.ClientID != "" {
		v.Set("client_id", c.ClientID)
	}
	if c.ClientSecret != "" {
		v.Set("client_secret", c.ClientSecret)
	}
	return v
}

type tokenCache struct {
	Token    oauth2.Token
	provider *oidc.Provider
	verifier *oidc.IDTokenVerifier
}

func (tc *tokenCache) init(c *AuthzClientHTTP) (err error) {
	tc.provider, err = oidc.NewProvider(oauth2.NoContext, c.issuerURL.String())
	if err != nil {
		return err
	}
	tc.verifier = tc.provider.Verifier(&oidc.Config{ClientID: c.clientID, SkipClientIDCheck: true})
	tc.Token, err = getAccessToken("authorization-service-api",
		c.clientID, c.clientSecret, tc.provider, tc.verifier)
	return err
}

func (tc *tokenCache) refresh(c *AuthzClientHTTP) error {
	_, err := tc.verifier.Verify(oauth2.NoContext, tc.Token.AccessToken)
	if err != nil {
		if strings.Contains(err.Error(), "oidc: token is expired") {
			c.Logger.Info(err.Error() + " and will be refreshed")
			err = nil
		}
		token, err := getAccessToken("authorization-service-api",
			c.clientID, c.clientSecret, tc.provider, tc.verifier)
		if err != nil {
			return err
		}
		tc.Token = token
	}
	return err
}
