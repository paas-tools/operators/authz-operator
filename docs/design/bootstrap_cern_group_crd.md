
# Why?

We need to be able to **only** create new CERN Groups through the AuthzAPI, this will be defined with `BootstrapCERNGroup` CRD and a controller to reconcile it.
Given it is `Bootstrap`, the goal is to create a group in the app portal, on which the operator will never touch after since it's expected that the site owner will manage the Group on the Application Portal.

Only 1 ApplicationRegistration is expected to exist per namespace,
and is selected implicitly for the all these CustomResources to refer to.
More ApplicationRegistrations in the project is *undefined behavior*.


## CERNGroup CRD  

```yaml
kind: BootstrapCERNGroups
metadata:
  # The resource's name is also the Group's identifier.
  # Note: this is only possible because we assume a single Authz Application per namespace, therefore avoiding name collisions (Same as ApplicationRole)
  name: {$WEBSITE_NAME}-admin-group
spec:
  # All the fields bellow are the ones required to create a new Group, we can remove some to have them defaulted with a value (Ex: `selfSubscritiptionType: "Closed"` )
  # GroupIdentifier and displayName are two string names to give when creating a Group, we may use metadata.name for both these fields, otherwise we should give these as extra fields on the spec
  # TODO: Confirm that these are sufficient, creating as an user gets `401` , "message": "You are not allowed to change this group due to synchronization constraints."
  # owner can be retrieved from ApplicationRegistration ?
  owner: TODO
  public: "True"
  description: "This is the admin group of $WEBSITE"
  # administratorId to be checked if required and if we want to implement in case of optional
  administratorId: 
  approvalRequired: "true"
  selfSubscriptionType: "Closed" # Three types available , [ Closed, Open, CernUsers ]
  privacyType: "Admins" # Three types available , [ Open, Members, Admins ]
  syncType: "Slave" # There are 4 types, [ Slave, SlaveWithPlaceholders, Master, SyncError ], slave is default, TODO: check what each type means
status:
  # Authz internal object ID of the associated Role
  groupID: fcsda12412-70ad-41fe-91b7-erg241csao91
  # All available status conditions shown together with errors, deviating from the example
  status:
    conditions:
    - type: GroupCreated
      status: true
      reason: {GroupBootstrappedSuccessfully,GroupCreating, GroupAlreadyExists ,ErrorCreating , GroupCreated}
      Message: ""
      LastTransitionTime: time
```

## CERNGroup Reconcile pseudocode

```golang
func reconcile(){
    // The watch would give us the CERNGroup to reconcile
    var cernGroup
    // Get ApplicationList of the same namespace and validate that there is only one
    applicationList := getApplicationList(applicationRole.namespace)
    if len(applicationList) == 0{
      // do nothing if there is no ApplicationRegistration.
      log("No ApplicationRegistration found in namespace, nothing to do")
    }
    // it only makes sense that we have exactly one ApplicationRegistration in the namespace.
    // Behavior is undefined is there are more than one.u
    // Just take the first one.
    appreg := applicationList.Items[0]
    // No need to check deletion timestamp since we won't have a finalizer and no extra work needed when applicationRole deleted
    if cernGroup.Status == ""{
      cernGroup.Status.Condition["GroupCreated"].Status = false
      cernGroup.Status.Condition["GroupCreated"].Reason = "GroupCreating"
    }
    // Check if group has been created
    if !cernGroup.Status.Condition["GroupCreated"].Status {
      // First we check if group exists
      exists, err := getGroup(cernGroup, appreg); err != nil{
        // Error will be from the API call itself, so we just return error and try again
        return err
      }
      // If the Group exists, there will be nothing else to do, therefore no further reconciliation
      if exists{
        cernGroup.Status.Condition["GroupCreated"].Status = true
        cernGroup.Status.Condition["GroupCreated"].Reason = "GroupAlreadyExists"
        // Stop reconcile and do nothing else
        return nil
      }
      // createGroup will create the Group in API, and in case it fails, will return error
      err := createGroup(cernGroup); err != nil{::
          // Based on error, this will likely be triggered by errors in the AuthzAPI call, we restart reconcile in this case to try again
          cernGroup.Status.Condition["GroupCreated"].Status= false
          cernGroup.Status.Condition["GroupCreated"].Reason = "GroupCreationError"
          return err
      }
      cernGroup.Status.Condition["GroupCreated"].Status = true
      cernGroup.Status.Condition["GroupCreated"].Reason = "GroupBootstrappedSuccessfully"
    }
    // If has been created, nothing to do
}
```
