# BootstrapApplicationRole design proposal

## Motivation

Currently we need to create Application roles in Authz, as well any linkage between the roles and groups.
See the following: 
- have the ability to create inital roles on ApplicationRegistration creation, this would replace ApplicationRegistration's `initialRoles` field.
- need to have roles created at website creation on [Drupal](https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/219)

The goal of `BootstrapApplicationRole` is to automate the creation of new roles, as well any required linkage with CERN Groups.
Given it is `Bootstrap`, the goal is to set up an initial role and link it with groups in the app portal, on which the operator will never touch after, since the owner will manage it through the Application Portal.

### Specification

`BootstrapApplicationRoles`, like Applications, are identified by a unique name (For BootstrapApplicationRoles is `name` in the AuthzAPI, represented as `Role Identifier` in the Application Portal).
A `BootstrapApplicationRole` may contain a list of CERN Groups, defined as `LinkedGroups` in the `spec`, to be linked with, each group can be identified as well with a single value (`groupIdentifier`).

The `BootstrapApplicationRole` behavior will follow the following points.

- It will create the `ApplicationRole` in the AuthzAPI once and **never** retrieve information from the AuthzAPI to the CR.
- If changed/deleted in the AuthzAPI, any changes made in the AuthzAPI will be ignored.
- If the CR is deleted, nothing will be done.

If a `BootstrapApplicationRole` with the given `name` identifier exists in AuthzAPI, the `BootstrapApplicationRole` will do nothing.
If it doesn't exist, it will be created.

The `BootstrapApplicationRole` will only be created after all the requested `LinkedGroups` are found to exist: we won't modify anything once the role is created.

Only 1 ApplicationRegistration is expected to exist per namespace,
and is selected implicitly for the all these CustomResources to refer to.
More ApplicationRegistrations in the project is *undefined behavior*.


### CRDs

This sample demonstrates the CRD design and how it would be configured for an application admininstator role:

```yaml
kind: BootstrapApplicationRole
metadata:
  # The resource's name is also the Role's identifier.
  # Note: this is only possible because we assume a single Authz Application per namespace, therefore avoiding name collisions
  name: administrator
spec:
  linkedGroups:
    - Group1
    - Group2 
  ## All the Spec fiels are based on the requirements from the API
  # The equivalent of Role Name in the Application portal
  displayName: "Application administrator"
  # Brief description, required for Role creation
  description: "Bearers of this role have administrator privileges in the application"  #These fields are the ones asked from the ApplicationPortal
  # Required to know if the role is required to access my application
  roleRequired: false
  # (From Application Portal): If checked, users must authenticate with Multifactor Authentication to be granted this role.
  multifactorRequired: false
  # (From Application Portal): if checked, this role will applied to all authenticated users, regardless of them belonging to any group or not. Use this option to define a role that is based only on the value of the Minimum Level of Assurance and/or usage of Multifactor authentication.
  applyToAllUsers: false
  # Level of assurance defines the accepted authentication providers, ranging from CERN identities (highest) to social accounts (public, therefore lowest)
  # Default: 4
  minLevelOfAssurance: 4  # CERN/EduGAIN
status:
  # Authz internal object ID of the associated Role
  roleID: f4eb7b1a-70ad-41fe-91b7-021bdd341eef

  conditions:
  # Would be based on "github.com/operator-framework/operator-lib/status", struct:
  #  Condition struct {
  #      Type               ConditionType          `json:"type"`
  #      Status             corev1.ConditionStatus `json:"status"`
  #      Reason             ConditionReason        `json:"reason,omitempty"`
  #      Message            string                 `json:"message,omitempty"`
  #      LastTransitionTime metav1.Time            `json:"lastTransitionTime,omitempty"` } 
  # The spec is only used to create a new role.
  # status: false if AuthzAPI returns unexpected error
  - type: RoleCreated
    status: true
    reason: {RoleBootstrappedSuccessfully, RoleCreating, RoleAlreadyExists,RoleCreationError,GroupLinkError, WaitingForLinkedGroups}
    Message: ""
    LastTransitionTime: time
```

## ApplicationRole Reconcile pseudocode

```golang
func reconcile(){
  // The watch would give us the BootstrapApplicationRole to reconcile
  var applicationRole 
  // Get ApplicationList of the same namespace and validate that there is only one
  applicationList := getApplicationList(applicationRole.namespace)
  if len(applicationList) == 0{
     // do nothing if there is no ApplicationRegistration.
		log("No ApplicationRegistration found in namespace, nothing to do")
  }
  // it only makes sense that we have exactly one ApplicationRegistration in the namespace.
	// Behavior is undefined is there are more than one.
	// Just take the first one.
	appreg := applicationList.Items[0]

  // No need to check deletion timestamp since we won't have a finalizer and no extra work needed when applicationRole deleted

  if applicationRole.Status == "" {
      applicationRole.Status.Condition["RoleCreated"].Status = false
      applicationRole.Status.Condition["RoleCreated"].Reason = "RoleCreating"
  }

  // Check if Role has been created, only if not we do something
  if !applicationRole.Status.Condition["RoleCreated"].Status {
    // First we check if role exists
    // (Option 2 is try to create and check if identifier already exists, however error code is a bit generic (400) to parse, maybe with message "The identifier is already used by an existing account or application." although prone to failures)
    exists, err := getRole(applicationRole, appreg); err != nil{
      // Error will be from the API call itself, so we just return error and try again
      return err
    }
    // If the Role exists, there will be nothing else to do, therefore no further reconciliation
    if exists{
      applicationRole.Status.Condition["RoleCreated"].Status = true
      applicationRole.Status.Condition["RoleCreated"].Reason = "RoleAlreadyExists"
      // Stop reconcile and do nothing else
      return nil
    }
    // Check if Groups exist
    groupIDs, exist, err := getGroupsFromAPI(applicationRole.Spec.LinkedGroups)
    if !exist{
      applicationRole.Status.Condition["RoleCreated"].Status = false
      applicationRole.Status.Condition["RoleCreated"].Reason = "WaitingForLinkedGroups"
      applicationRole.Status.Condition["RoleCreated"].Message = "ListOfMissingGroups" // Message will be filled with all the missing Groups
      // This will requeue ***with exponential back-off*** the CR to check if the groups already exist
      return err
    }
    appID, err := createApplicationRoleInAuthzAPI(applicationRole, appreg); err != nil{
        // Based on error, restart reconcile
        applicationRole.Status.Condition["RoleCreated"].Status = false
        applicationRole.Status.Condition["RoleCreated"].Reason = "RoleCreationError"
        return err
    }
    err := LinkGroupsToRole(appID, groupIDs)
    if err !=nil{
        // Based on error, restart reconcile
        applicationRole.Status.Condition["RoleCreated"].Status = false
        applicationRole.Status.Condition["RoleCreated"].Reason = "GroupLinkError"
        return err
    }
    applicationRole.Status.Condition["RoleCreated"].Status = true
    applicationRole.Status.Condition["RoleCreated"].Reason = "RoleBootstrappedSuccessfully"
  }
  // If already in "RoleCreated", nothing to do
}
```
