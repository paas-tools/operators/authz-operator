## IMPORTANT: This document is incomplete as we have yet no use case for this CRD therefore some parts of the behavior/spec are to be defined

# ManagedApplicationRole design proposal


## Motivation

Currently we need to create Application roles in Authz, as well any linkage between the roles and groups.
See the following: 
- have the ability to fully define in-cluster the desired state in Authz, so OKD is the source of truth for the role definitions (with a specific webframeworks UI for role management).
  While we initially decided to not replicate the role-management functionality of the Application Portal, we are reconsidering following [an incident where ApplicationRegistrations were deleted](https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/bdef0cf1857e668ce5cc4478cea8cc149559d33d/docs/troubleshooting/recover-deleted-appregs.md)
- need to have roles created at website creation on [Drupal](https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/219)

The goal of `ManagedApplicationRole` is to fully manage new roles, as well any required linkage with CERN Groups.

### Specification

`ManagedApplicationRoles`, like Applications, are identified by a single value (For ApplicationRoles is `name` in the AuthzAPI).
A `ManagedApplicationRole` may contain a list of CERN Groups, defined as  `LinkedGroups` in the `spec`, to be linked with, each group can be identified as well with a single value (`groupIdentifier`).

The `ManagedApplicationRole` behavior will be based on the following:
- The `ManagedApplicationRole` will be fully managed by the CR's spec.
- There is a **need** for ownership information to be stored in the AuthzAPI to enable recreation/migration between clusters.
- If deleted or changed in the AuthzAPI, the resource will be reacreated as described in the `Spec`.
- If the CR is deleted, the Role will be deleted from the AuthzAPI (this is not extended to CERN Groups).
- If while creating, already exists in the API, validate that we are owners/administrators in order to enforce `Spec`, otherwise go to error state?

If a ManagedApplicationRole with the given `name` exists in AuthzAPI, it will be *recreated* and values will be overwritten.
If it doesn't exist, it will be created.

The `ManagedApplicationRole` will only be created after guaranteing that all the `LinkedGroups` exist.

Only 1 ApplicationRegistration is expected to exist per namespace,
and is selected implicitly for the all these CustomResources to refer to.
More ApplicationRegistrations in the project is *undefined behavior*.


### CRDs

This sample demonstrates the CRD design and how it would be configured for an application admininstator role:

```yaml
kind: ManagedApplicationRole
metadata:
  # The resource's name is also the Role's identifier.
  # Note: this is only possible because we assume a single Authz Application per namespace, therefore avoiding name collisions
  name: administrator
spec:
  linkedGroups:
    - Group1
    - Group2 
  ## All the Spec fiels are based on the requirements from the API
  # The equivalent of Role Name in the Application portal
  displayName: "Application administrator"
  # Brief description, required for Role creation
  description: "Bearers of this role have administrator privileges in the application"  #These fields are the ones asked from the ApplicationPortal
  # Required to know if the role is required to access my application
  roleRequired: false
  # (From Application Portal): If checked, users must authenticate with Multifactor Authentication to be granted this role.
  multifactorRequired: false
  # (From Application Portal): if checked, this role will applied to all authenticated users, regardless of them belonging to any group or not. Use this option to define a role that is based only on the value of the Minimum Level of Assurance and/or usage of Multifactor authentication.
  applyToAllUsers: false
  # Level of assurance defines the accepted authentication providers, ranging from CERN identities (highest) to social accounts (public, therefore lowest)
  # Default: 4
  minLevelOfAssurance: 4  # CERN/EduGAIN
status:
  conditions:
  # Would be based on "github.com/operator-framework/operator-lib/status", struct:
  #  Condition struct {
  #      Type               ConditionType          `json:"type"`
  #      Status             corev1.ConditionStatus `json:"status"`
  #      Reason             ConditionReason        `json:"reason,omitempty"`
  #      Message            string                 `json:"message,omitempty"`
  #      LastTransitionTime metav1.Time            `json:"lastTransitionTime,omitempty"` } 
  # The spec is only used to create a new role.
  # status: false if AuthzAPI returns unexpected error
  - type: SyncedRole
    status: true
    reason: {RoleCreating,RoleAlreadyExists,RoleErrorUpdating, RoleCreationError}
    Message: ""
    LastTransitionTime: time
  - type : LinkedGroupsFound
    #if groups not found, set to false
    status: true
    # if false, maybe list the groups that are missing here?
    reason: {}
```

## ApplicationRole Reconcile pseudocode

```golang
func reconcile(){
  // The watch would give us the ManagedApplicationRole to reconcile
  var applicationRole 
  // Get ApplicationList of the same namespace and validate that there is only one
  applicationList := getApplicationList(applicationRole.namespace)
  if len(applicationList) == 0{
     // do nothing if there is no ApplicationRegistration.
		log("No ApplicationRegistration found in namespace, nothing to do")
  }
  // it only makes sense that we have exactly one ApplicationRegistration in the namespace.
	// Behavior is undefined is there are more than one.
	// Just take the first one.
	appreg := applicationList.Items[0]

  // First check if there is a deletion timestamp
  if applicationRole.GetDeletionTimestamp() != nil{
    if !applicationRole.Spec.CreateOnly{
      err := deleteApplicationRoleInAuthzAPI(applicationRole)
      if err != nil{
        // Error deleting, we re-do reconciliation
        return err
      } 
    }
    removeApplicationRoleFinalizer(applicationRole) 
  }

  if applicationRole.Status == ""{
    applicationRole.Status["SyncedRole"].Status = false
    applicationRole.Status["SyncedRole"].Reason = "RoleCreating"
  }

  // This IF would be wrapped inside a function
  if applicationRole.Status["SyncedRole"].Reason == "RoleCreating" || applicationRole.Status["SyncedRole"].Reason == "RoleCreationError" {
    // First fetch Role and confirm we are owners/admins in case it exists, otherwise we will create and be owners/admins
    existsAndNotAdmin, err := getApplicationRole(applicationRole, appreg); err != nil{
      // Based on error, restart reconcile
      applicationRole.Status["SyncedRole"].Status = false
      applicationRole.Status["SyncedRole"].Reason = "RoleCreationError"
      return err
    }
    if existsAndNotAdmin{
      // this is a permanent error, so no reconciliation will be made again
      applicationRole.Status["SyncedRole"].Status = false
      applicationRole.Status["SyncedRole"].Reason = "RoleCreationError"
      return nil
    } 
    groupIDs, exist, err := getGroupsFromAPI(applicationRole.Spec.LinkedGroups)
    if !exist{
      applicationRole.Status.Condition.Type["LinkedGroupsFound"].Status = false
      // We will not requeue and proceed with Role creation, the LinkedGroups will be retried on `enforceSpec`
    }else{
      applicationRole.Status.Condition.Type["LinkedGroupsFound"].Status = true
    }
    // create Role will check if the Role is in API:
    // If not in API, just create, otherwise will override *IF* we are owners/administrators
    appID, err := createRole(applicationRole, appreg); err != nil{
      // Based on error, restart reconcile
      applicationRole.Status.Condition.Reason = "RoleCreationError"
      return err
    }
    if applicationRole.Status.Condition["LinkedGroupsFound"].Status{
      err := LinkGroupsToRole(appID, groupIDs)
      if err !=nil{
        // Based on error, restart reconcile
        applicationRole.Status.Condition.Reason = "RoleCreationError"
      }
    }
    applicationRole.Status.Condition["SyncedRole"].Status = true
    applicationRole.Status.Condition.Reason = "RoleCreated"
  }

  err != enforceSpecOnAuthzAPI(applicationRole.Spec)
  if err != nil{
    applicationRole.Status.Condition["SyncedRole"].Reason = "RoleErrorUpdating"
    applicationRole.Status.Condition["SyncedRole"].Status = false
    return err
  } 
  applicationRole.Status.Condition["SyncedRole"].Status = true
  applicationRole.Status.Condition["SyncedRole"].Reason = "RoleCreated"
}
```
