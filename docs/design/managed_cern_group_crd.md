## IMPORTANT: This document is incomplete as we have yet no use case for this CRD therefore some parts of the behavior/spec are to be defined

# Why?

We need to be able to create and **manage** new CERN Groups through the AuthzAPI, this will be defined with `ManagedCERNGroup` CRD and a controller to reconcile it.


The `ManagedCernGroup`, like in `ManagedApplicationRole`, will have the behavior based on the following:
- The `ManagedCernGroup` will be fully managed by the CR's spec.
- There is a **need** for ownership information to be stored in the AuthzAPI to enable recreation/migration between clusters.
- If deleted or changed in the AuthzAPI, the resource will be reacreated as described in the `Spec`.
- If the CR is deleted, the Group will be deleted from the AuthzAPI (?) //TODO: Confirm this action once completed
- If while creating, already exists in the API, validate that we are owners/administrators in order to enforce `Spec`, otherwise go to error state?


Only 1 ApplicationRegistration is expected to exist per namespace,
and is selected implicitly for the all these CustomResources to refer to.
More ApplicationRegistrations in the project is *undefined behavior*.


## CERNGroup CRD  

```yaml
kind: ManagedCernGroup
metadata:
  # The resource's name is also the Group's identifier.
  # Note: this is only possible because we assume a single Authz Application per namespace, therefore avoiding name collisions (Same as ApplicationRole)
  name: {$WEBSITE_NAME}-admin-group
spec:
# All the fields bellow are the ones required to create a new Group, we can remove some to have them defaulted with a value (Ex: `selfSubscritiptionType: "Closed"` )
  # TODO: Confirm that these are sufficient, creating as an user gets `401` 
  # owner can be retrieved from ApplicationRegistration ?
  owner: TODO
  public: "True"
  description: "This is the admin group of $WEBSITE"
  # administratorId to be checked if required and if we want to implement in case of optional
  administratorId: 
  approvalRequired: "true"
  selfSubscriptionType: "Closed" # Three types available , [ Closed, Open, CernUsers ]
  privacyType: "Admins" # Three types available , [ Open, Members, Admins ]
  syncType: "Slave" # There are 4 types, [ Slave, SlaveWithPlaceholders, Master, SyncError ], slave is default, TODO: check what each type means"
status:
  # Authz internal object ID of the associated Role
  groupID: fcsda12412-70ad-41fe-91b7-erg241csao91
  # All available status conditions shown together with errors, deviating from the example
  status:
    conditions:
    - type: CreatedGroup
      status: true
      reason: {Creating, ErrorCreating}
    - type: SyncedGroup
      status: true
      reason: {ErrorUpdating}
      Message: ""
      LastTransitionTime: time
```

## CERNGroup Reconcile pseudocode

```golang
func reconcile(){
    // The watch would give us the CernGroup to reconcile
    var cernGroup
    // First check if there is a deletion timestamp
    if cernGroup.GetDeletionTimestamp() != nil{
      err := deleteGroupLink(cernGroup)
      if err != nil{
        // Error happened during AuthzAPI call, let's retry
        return err
      }
      removeFinalizer(cernGroup)
    }
    if !cernGroup.Status.Condition["CreatedGroup"].Status {
      // createGroup will check if the Group is in API
      // If not in API: create it
      // If in API, confirm that is owned by the same CR, then force spec
      err := createGroup(cernGroup); err != nil{
          // Based on error, restart reconcile
          cernGroup.Status.Condition["CreatedGroup"].status = false
          cernGroup.Status.Condition["CreatedGroup"].reason = "GroupCreationError"
          return err
      }
      cernGroup.Status.Condition["CreatedGroup"].reason = ""
      cernGroup.Status.Condition["CreatedGroup"].status = true
    }
    err != enforceSpecOnAuthzAPI(cernGroup.Spec)
    if err != nil{
      cernGroup.Status.Condition["SyncedGroup"].status= false
      cernGroup.Status.Condition["SyncedGroup"].reason= "ErrorUpdating"
      return err
    }
    cernGroup.Status.Condition["SyncedGroup"].status= true
    cernGroup.Status.Condition["SyncedGroup"].reason= ""
}
```
