Operator for SSO registration:


    
```graphviz
graph graphname { 
        Keycloak [style="rounded,filled"]
        Authorization_Service_API [style="rounded,filled"]

        Authorization_Service_API -- SSO_Operator[color="orange"];
        SSO_Operator -- Keycloak [color="blue"];
        Authorization_Service_API -- Swagger[color="orange"];
        Authorization_Service_API -- Application_Portal[color="orange"];
        Swagger -- Keycloak[color="blue"];
        Application_Portal -- Keycloak[color="blue"]; 
	}
```

The grey elements are services.
The white elements are clients.
The lines represent communcation channels.


The workflow of **Swagger** and the **Application Portal** are similar to the **SSO Registration Operator** when it comes to communicating with the **Authorization Service API**.
They first communicate with **Keycloak**, where they retrieve a token that is then used on requests with the **Authorization Service API** .
