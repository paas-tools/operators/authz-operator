API notes
---

# Web UI breakdown

## Add an Application

* Note: OPTIONS -> "preflighted" requests (check if it's safe to send the actual req)
Also [discover verbs for resource](http://zacstewart.com/2012/04/14/http-options-method.html)

init
- get apiToken
- get Request/my (get all requests created by me)
- get Application/my (get all apps owned/admin by me)
- get `Request/to_approve`

groups
- get Group?filter=...
- get Group/{groupId}

submit
- post Application

## Edit app

- get Application/{appId}
- get Application/{appId}/roles
- get Registration/{appId}
- get Identity/{identityId}
- get Group/{appId}

SSOReg
- get Registration/Providers -> OIDC
- post Registration/{appId}/{authId} -> includes regist secret
- get Registration/{regId}/secret ?

RoleReg
- bunch of GETs
- POST Application/{appId}/roles

GroupXRole
- get Application/{appId}/roles/{roleId}
- get Application/{appId}/roles/{roleId}/groups
- post Application/{appId}/roles/{roleId}/groups/{groupId}

## OIDC Registration

token exchange permissions
- get api/v1.0/Registration/{providerId}/search?filter=registrationName%3Astartswith%3Aauthoriza&sort=registrationName
- PUT api/v1.0/Registration/{regId}/token-exchange-request/{allowedRegId}
- get api/v1.0/Registration/{regId}/token-exchange-permission/allowed
- get api/v1.0/Registration/{regId}/token-exchange-permission/granted
