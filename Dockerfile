# Build the manager binary
FROM golang:1.20 as builder

WORKDIR /workspace
# NB: modified from sdk-generated Dockerfile (since we vendor modules): copy the full gitlab project contents
COPY . .

# Build
# NB: modified from sdk-generated Dockerfile (since we vendor modules): added -mod=vendor
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -mod=vendor -o operator cmd/operator/operator.go && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -mod=vendor -o softdelete cmd/softdeletion/lifecycle-delete.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/operator /bin/
COPY --from=builder /workspace/softdelete /bin/
USER nonroot:nonroot

CMD ["/bin/operator"]
